package com.gox.taximodule.ui.fragment.invoice

import android.annotation.SuppressLint
import android.app.Activity.RESULT_CANCELED
import android.content.Intent
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.gox.basemodule.base.BaseFragment
import com.gox.basemodule.common.cardlist.ActivityCardList
import com.gox.basemodule.data.Constant
import com.gox.basemodule.model.VerifyPayPalSuccess
import com.gox.basemodule.utils.ViewUtils
import com.gox.taximodule.R
import com.gox.taximodule.data.dto.request.ReqPaymentUpdateModel
import com.gox.taximodule.data.dto.request.ReqTips
import com.gox.taximodule.data.dto.response.ResCheckRequest
import com.gox.taximodule.databinding.InvoiceFragmentBinding
import com.gox.taximodule.ui.activity.main.TaxiMainViewModel
import com.gox.taximodule.ui.fragment.rating.RatingFragment
import com.gox.taximodule.ui.fragment.tips.TipsFragment
import permissions.dispatcher.NeedsPermission
import permissions.dispatcher.RuntimePermissions
import java.util.jar.Manifest

@RuntimePermissions
class InvoiceFragment : BaseFragment<InvoiceFragmentBinding>(), InvoiceNavigator {

    private lateinit var mInvoiceFragmentBinding: InvoiceFragmentBinding
    private lateinit var mInvoiceViewModel: InvoiceViewModel
    private lateinit var mTaxiMainViewModel: TaxiMainViewModel
    private var mRequestId: Int? = null
    private var mCardId: String? = null
    private var mTipsAmount: String? = null
    private var paymentMode : String ? = ""

    companion object {
        fun newInstance() = InvoiceFragment()
    }

    override fun getLayoutId(): Int = R.layout.invoice_fragment

    override fun initView(mRootView: View?, mViewDataBinding: ViewDataBinding?) {
        mInvoiceFragmentBinding = mViewDataBinding as InvoiceFragmentBinding
        mInvoiceViewModel = ViewModelProviders.of(this).get(InvoiceViewModel::class.java)
        mTaxiMainViewModel = ViewModelProviders.of(activity!!).get(TaxiMainViewModel::class.java)

        mInvoiceViewModel.navigator = this
        mInvoiceFragmentBinding.viewModel = mInvoiceViewModel

        mTaxiMainViewModel.getCheckRequestResponse().observe(this, Observer {
            if (it.statusCode == "200") {
                if (it.responseData?.data!!.isNotEmpty()) {
                    mRequestId = it.responseData.data[0]!!.id
                    statusWhenDropped(it)
                }
            }
        })
        mInvoiceViewModel.getUpdatePaymentResponse().observe(this, Observer {
            if (it != null) {
                if (it.statusCode.equals("200")) {
                    ViewUtils.showToast(activity!!, it.message, true)

                }
            }
        })
            mInvoiceViewModel.verifyPaypalResposne.observe(this, Observer<VerifyPayPalSuccess>{
            if(it.statusCode.equals("200")){
                ViewUtils.showNormalToast(activity!!, it.message!!)
                val mRatingFragment: RatingFragment = RatingFragment.newInstance()
                mRatingFragment.show(baseActivity.supportFragmentManager, mRatingFragment.tag)
            }
        })

        mInvoiceFragmentBinding.tvAddTips.setOnClickListener {
            val mTipsFragment = TipsFragment.newInstance()
            mTipsFragment.isCancelable = true
            mTipsFragment.show(baseActivity.supportFragmentManager, mTipsFragment.tag)
        }
        mInvoiceFragmentBinding.tvTaxiChangePayment.setOnClickListener {

            val intent = Intent(activity!!, ActivityCardList::class.java)
            intent.putExtra("activity_result_flag", "1")
            intent.putExtra("payment_type", mTaxiMainViewModel.paymentType.get())
            startActivityForResult(intent, Constant.PAYMENT_TYPE_REQUEST_CODE)
        }


        mInvoiceViewModel.getInvoiceResponse().observe(this, Observer {
            if(paymentMode.equals("cash",true)) {
                if (it.statusCode == "200") {
                    mTaxiMainViewModel.ratingdone.value = false
                    ViewUtils.showNormalToast(activity!!, it.message!!)
                    val mRatingFragment: RatingFragment = RatingFragment.newInstance()
                    mRatingFragment.show(baseActivity.supportFragmentManager, mRatingFragment.tag)
                }
            }else
            {
                if (!it.responseData!!.isNullOrEmpty()) {
                    val intent = Intent(activity, ActivityCardList::class.java)
//                    intent.putExtra("payPalurl",it.responseData!!.url)
                    intent.putExtra("payment_type", mTaxiMainViewModel.paymentType.get())
                    startActivityForResult(intent, Constant.RequestCode.PAYAPL_RESULT)
                }
            }
        })
        mInvoiceViewModel.getErrorResponse().observe(this, Observer {
            if (it != null) {
                ViewUtils.showNormalToast(activity!!, it.toString())
            }
        })

        mTaxiMainViewModel.ratingdone.observe(this, Observer {
            if(it != null && it == true)
                activity!!.finish()
        })
    }

    @SuppressLint("SetTextI18n", "ResourceType")
    private fun statusWhenDropped(it: ResCheckRequest?) {
        val result = it?.responseData?.data?.get(0)!!
        mInvoiceFragmentBinding.tvTaxiSourceLocation.text = result.s_address
        mInvoiceFragmentBinding.tvTaxiDestinationLocation.text = result.d_address
        mInvoiceFragmentBinding.tvBookingID.text = result.booking_id
        mInvoiceFragmentBinding.tvDistanceTravelled.text = "%.2f".format(result.total_distance) + " " + result.unit.toString()
//        if (!result.travel_time.isNullOrEmpty() && result.travel_time.toInt() > 0) {
//            mInvoiceFragmentBinding.tvTimeTaken.visibility = VISIBLE
//            mInvoiceFragmentBinding.tvLabelTimeTaken.visibility = VISIBLE
//            mInvoiceFragmentBinding.tvTimeTaken.text = result.travel_time.toString() + getString(R.string.mins)
//        } else {
//            mInvoiceFragmentBinding.tvTimeTaken.visibility = GONE
//            mInvoiceFragmentBinding.tvLabelTimeTaken.visibility = GONE
//        }
        if (result.payment?.discount!! > 0) {
            mInvoiceFragmentBinding.tvDisCountFare.text = result.currency.toString() + " " + result.payment.discount.toString()
            mInvoiceFragmentBinding.tvLabelDisCount.visibility = VISIBLE
            mInvoiceFragmentBinding.tvDisCountFare.visibility = VISIBLE
        } else {
            mInvoiceFragmentBinding.tvLabelDisCount.visibility = GONE
            mInvoiceFragmentBinding.tvDisCountFare.visibility = GONE
        }
        if (result.payment.waiting_amount!! > 0) {
            mInvoiceFragmentBinding.tvLabelWaitingTimeFare.text = result.currency.toString() + " " + result.payment.waiting_amount.toString()
            mInvoiceFragmentBinding.tvLabelWaitingTimeFare.visibility = VISIBLE
            mInvoiceFragmentBinding.tvLabelWaitingTime.visibility = VISIBLE
        } else {
            mInvoiceFragmentBinding.tvLabelWaitingTimeFare.visibility = GONE
            mInvoiceFragmentBinding.tvLabelWaitingTime.visibility = GONE
        }
        if (result.payment.toll_charge!! > 0) {
            mInvoiceFragmentBinding.tvLabelTollFare.text = result.currency.toString() + " " + result.payment.toll_charge.toString()
            mInvoiceFragmentBinding.tvLabelTollFare.visibility = VISIBLE
            mInvoiceFragmentBinding.tvLabelToll.visibility = VISIBLE
        } else {
            mInvoiceFragmentBinding.tvLabelTollFare.visibility = GONE
            mInvoiceFragmentBinding.tvLabelToll.visibility = GONE
        }

        if(result.calculator.equals("MIN")){
            mInvoiceFragmentBinding.tvLabelTimeTaken.text=result.payment?.time_fare_text
            mInvoiceFragmentBinding.tvTimeTaken.text = result.currency.toString() + " "+ result.payment.minute.toString()
            mInvoiceFragmentBinding.tvLabelDistanceFare.visibility = GONE
            mInvoiceFragmentBinding.tvDistanceFare.visibility = GONE
            if(result.payment.minute == 0.0)
                mInvoiceFragmentBinding.timeFarelay.visibility = View.GONE
            else
                mInvoiceFragmentBinding.timeFarelay.visibility = View.VISIBLE
        } else if(result.calculator.equals("HOUR")){
            mInvoiceFragmentBinding.tvLabelDistanceFare.visibility = GONE
            mInvoiceFragmentBinding.tvDistanceFare.visibility = GONE
            mInvoiceFragmentBinding.tvLabelTimeTaken.text=result.payment?.time_fare_text
            mInvoiceFragmentBinding.tvTimeTaken.text = result.currency.toString() + " "+ result.payment.hour.toString()
            if(result.payment.hour == 0.0)
                mInvoiceFragmentBinding.timeFarelay.visibility = View.GONE
            else
                mInvoiceFragmentBinding.timeFarelay.visibility = View.VISIBLE
        } else if(result.calculator.equals("DISTANCE")){
            mInvoiceFragmentBinding.tvLabelDistanceFare.text= result.payment?.distance_fare_text
            mInvoiceFragmentBinding.tvDistanceFare.text = result.currency.toString() + " " + result.payment?.distance.toString()
            mInvoiceFragmentBinding.tvLabelTimeTaken.visibility = GONE
            mInvoiceFragmentBinding.tvTimeTaken.visibility = GONE
        } else if(result.calculator.equals("DISTANCEMIN")){
            mInvoiceFragmentBinding.tvLabelTimeTaken.text=result.payment?.time_fare_text
            mInvoiceFragmentBinding.tvLabelDistanceFare.text= result.payment?.distance_fare_text
            mInvoiceFragmentBinding.tvTimeTaken.text = result.currency.toString() + " "+ result.payment.minute.toString()
            mInvoiceFragmentBinding.tvDistanceFare.text = result.currency.toString() + " " + result.payment?.distance.toString()
            if(result.payment.minute == 0.0)
                mInvoiceFragmentBinding.timeFarelay.visibility = View.GONE
            else
                mInvoiceFragmentBinding.timeFarelay.visibility = View.VISIBLE
        } else if(result.calculator.equals("DISTANCEHOUR")){
            mInvoiceFragmentBinding.tvLabelTimeTaken.text=result.payment?.time_fare_text
            mInvoiceFragmentBinding.tvLabelDistanceFare.text= result.payment?.distance_fare_text
            mInvoiceFragmentBinding.tvTimeTaken.text = result.currency.toString() + " "+ result.payment.hour.toString()
            mInvoiceFragmentBinding.tvDistanceFare.text = result.currency.toString() + " " + result.payment?.distance.toString()
            if(result.payment.hour == 0.0)
                mInvoiceFragmentBinding.timeFarelay.visibility = View.GONE
            else
                mInvoiceFragmentBinding.timeFarelay.visibility = View.VISIBLE
        }

        if(result.payment?.distance == 0.0)
            mInvoiceFragmentBinding.distanceFarelay.visibility = View.GONE
        else
            mInvoiceFragmentBinding.distanceFarelay.visibility = View.VISIBLE
        mInvoiceFragmentBinding.tvLabelBaseFare.text = result.payment?.base_fare_text
        mInvoiceFragmentBinding.tvLabelWaitingTime.text = result.payment?.waiting_fare_text
        mInvoiceFragmentBinding.tvLabelDisCount.text = result.payment?.discount_fare_text
//        mInvoiceFragmentBinding.tvLabelTimeTaken.text=result.payment?.time_fare_text
//        mInvoiceFragmentBinding.tvLabelDistanceFare.text= result.payment?.distance_fare_text
        mInvoiceFragmentBinding.tvBaseFare.text = result.currency.toString() + " " + result.payment?.fixed
        if (result.payment.tax!! > 0) {
            mInvoiceFragmentBinding.tvTax.text = result.currency.toString() + " " + result.payment?.tax.toString()
            mInvoiceFragmentBinding.tvLabelTax.visibility = VISIBLE
            mInvoiceFragmentBinding.tvTax.visibility = VISIBLE
        } else {
            mInvoiceFragmentBinding.tvLabelTax.visibility = GONE
            mInvoiceFragmentBinding.tvTax.visibility = GONE
        }

        mInvoiceFragmentBinding.totalPayableAmount.text = result.currency.toString() + " " + result.payment?.total.toString()
        mInvoiceFragmentBinding.tvLabelPeakChargeFare.text = result.currency.toString() + " " + result.payment?.peak_amount.toString()
        mInvoiceFragmentBinding.tvTotalAmount.text = result.currency.toString() + " " + result.payment?.payable?.toInt()
        if (result.payment.toll_charge!! > 0) {
            mInvoiceFragmentBinding.tvLabelTollFare.text = result.currency.toString() + " " + result.payment.toll_charge.toString()
            mInvoiceFragmentBinding.tvLabelTollFare.visibility = VISIBLE
            mInvoiceFragmentBinding.tvLabelToll.visibility = VISIBLE
        } else {
            mInvoiceFragmentBinding.tvLabelTollFare.visibility = GONE
            mInvoiceFragmentBinding.tvLabelToll.visibility = GONE
        }
        if (result.payment.total_fare!! > 0) {
            mInvoiceFragmentBinding.tvTotalFare.text = result.currency.toString() + " " + result.payment.total_fare.toString()
            mInvoiceFragmentBinding.tvLabelTotalFare.visibility = VISIBLE
            mInvoiceFragmentBinding.tvTotalFare.visibility = VISIBLE
        } else {
            mInvoiceFragmentBinding.tvLabelTotalFare.visibility = GONE
            mInvoiceFragmentBinding.tvTotalFare.visibility = GONE
        }
        if (result.payment.sub_total!! > 0) {
            mInvoiceFragmentBinding.tvSubTotal.text = result.currency.toString() + " " + result.payment.sub_total.toString()
            mInvoiceFragmentBinding.tvLabelSubTotal.visibility = VISIBLE
            mInvoiceFragmentBinding.tvSubTotal.visibility = VISIBLE
        } else {
            mInvoiceFragmentBinding.tvLabelSubTotal.visibility = GONE
            mInvoiceFragmentBinding.tvSubTotal.visibility = GONE
        }

        mInvoiceFragmentBinding.PayableAmount.text = result.currency.toString() + " " + result.payment?.payable?.toString()
        mTaxiMainViewModel.paymentType.set(result.payment_mode)
        paymentMode = result.payment_mode
        if (result.payment_mode.equals("CASH")) {
            mInvoiceFragmentBinding.tvLabelTips.visibility = GONE
            mInvoiceFragmentBinding.tvAddTips.visibility = GONE
            mInvoiceFragmentBinding.btConfirm.visibility = GONE
            mInvoiceFragmentBinding.tvPaymentType.text = result.payment_mode
            mInvoiceFragmentBinding.ivPaymentMode.setImageDrawable(resources.getDrawable(R.drawable.ic_money))
            mInvoiceFragmentBinding.tvTaxiChangePayment.visibility = VISIBLE
        } else {
            mInvoiceFragmentBinding.tvLabelTips.visibility = VISIBLE
            mInvoiceFragmentBinding.tvAddTips.visibility = VISIBLE
            mInvoiceFragmentBinding.tvPaymentType.text = result.payment_mode
            mInvoiceFragmentBinding.btConfirm.text = requireActivity().getString(R.string.paynow)
            mInvoiceFragmentBinding.btConfirm.visibility = VISIBLE
            mInvoiceFragmentBinding.ivPaymentMode.setImageDrawable(resources.getDrawable(R.drawable.ic_credit_card))
            mInvoiceFragmentBinding.tvTaxiChangePayment.visibility = VISIBLE
            mTaxiMainViewModel.mAddTips.observe(this, Observer {
                if (it != null) {
                    mInvoiceFragmentBinding.tvAddTips.visibility = GONE
                    mInvoiceFragmentBinding.tvTipsAmount.visibility = VISIBLE
                    mTipsAmount = it.toString()
                    mInvoiceFragmentBinding.tvTipsAmount.text = result.currency.toString() + " " + it.toString()
                }
            })
        }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (resultCode != RESULT_CANCELED) {
            when (requestCode) {
                Constant.PAYMENT_TYPE_REQUEST_CODE -> {
                    val payment_type:String = data?.extras?.get("payment_type").toString()
                    mCardId = data?.extras?.get("card_id").toString()
                    if (payment_type!!.equals(Constant.PaymentMode.CASH,true)) {
                        mInvoiceFragmentBinding.ivPaymentMode.setImageDrawable(resources.getDrawable(R.drawable.ic_money))
                        mInvoiceFragmentBinding.tvPaymentType.text = payment_type.toString()
                        mTaxiMainViewModel.paymentType.set(Constant.PaymentMode.CASH.toUpperCase())
                    } else {
                        mInvoiceFragmentBinding.ivPaymentMode.setImageDrawable(resources.getDrawable(R.drawable.ic_credit_card))
                        mInvoiceFragmentBinding.tvPaymentType.text = Constant.PaymentMode.CARD.toUpperCase()
                        mTaxiMainViewModel.paymentType.set(Constant.PaymentMode.CARD.toUpperCase())
                        mTaxiMainViewModel.card_id.set(mCardId)
                        val reqPaymentUpdateModel = ReqPaymentUpdateModel()
                        reqPaymentUpdateModel.requestId = mRequestId
                        reqPaymentUpdateModel.paymentMode = Constant.PaymentMode.CARD.toUpperCase()
                        mInvoiceViewModel.updatePayment(reqPaymentUpdateModel)
                    }
                }
                Constant.RequestCode.PAYAPL_RESULT -> {
                    if(data!=null && data.hasExtra("paypal")){
                        val payPalPayment=data.getSerializableExtra("paypal") as HashMap<String,String>
                        if(!payPalPayment.isEmpty()) {
                            if (payPalPayment.get("status").equals("1")) {
                                mInvoiceViewModel.verifyPayment(payPalPayment)
                            } else {
                                mInvoiceViewModel.failure(payPalPayment)
                            }
                        }
                    }
                }
            }
        }
    }

    @NeedsPermission(android.Manifest.permission.ACCESS_FINE_LOCATION)
    fun test(){

    }

    override fun showRating() {
        val reqTips = ReqTips()
        reqTips.requestId = mRequestId
        reqTips.cardId = mCardId
        reqTips.tipsAmount = mTipsAmount
        mInvoiceViewModel.setCardPayment(reqTips)
    }
}
