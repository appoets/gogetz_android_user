package com.gox.taximodule.data.dto.response

data class RatingResponse(
    val error: List<Any>?,
    val message: String?,
    val responseData: List<Any>?,
    val statusCode: String?,
    val title: String?
)