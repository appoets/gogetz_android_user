package com.gox.basemodule.common.payment.model

import com.google.gson.annotations.SerializedName

data class ResCommonSuccessModel(
        @SerializedName("statusCode")
        var statusCode: String? = null,
        @SerializedName("title")
        var title: String? = null,
        @SerializedName("message")
        var message: String? = null,
        @SerializedName("responseData")
        var responseData: String? = null
){
    data class ResponseData(
            val message: String? = "",
            val url: String? = ""
    )
}