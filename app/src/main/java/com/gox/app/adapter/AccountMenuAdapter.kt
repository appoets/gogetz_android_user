package com.gox.app.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import com.gox.app.BR
import com.gox.app.R
import com.gox.app.data.repositary.remote.model.AccountMenuModel
import com.gox.app.ui.myaccount_fragment.MyAccountFragment
import com.gox.app.ui.myaccount_fragment.MyAccountFragmentViewModel

class  AccountMenuAdapter(private  val accountFragment: MyAccountFragment,private  val accountViewModel:MyAccountFragmentViewModel):RecyclerView.Adapter<AccountMenuAdapter.AccountViewModel>(){

     class  AccountViewModel(private  val binding:ViewDataBinding):RecyclerView.ViewHolder(binding.root){
        fun bind(accountFragment: MyAccountFragment,accountViewModel: MyAccountFragmentViewModel,position:Int,accountMenuModel: AccountMenuModel){
            binding.setVariable(BR.position,position)
            binding.setVariable(BR.accountViewModel,accountViewModel)
            binding.setVariable(BR.accountFragment,accountFragment)
            binding.setVariable(BR.accountMenuModel,accountMenuModel)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AccountViewModel {
        return  AccountViewModel(DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.layout_account_menu_item,parent,false))
    }

    override fun getItemCount(): Int {
        return  accountViewModel.getAccountMenus().size
    }

    override fun onBindViewHolder(holder: AccountViewModel, position: Int) {
        holder.bind(accountFragment,accountViewModel,position,accountViewModel.getAccountMenu(position))
    }
}