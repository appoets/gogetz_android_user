package com.gox.app.data.repositary.remote.model

import java.io.Serializable

data class CityListResponse(
    val error: List<Any>,
    val message: String,
    val responseData: List<City>,
    val statusCode: String,
    val title: String
):Serializable

