package com.gox.app.ui.myaccount_fragment

import android.content.DialogInterface
import android.content.Intent
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.Observer
import com.gox.basemodule.BaseApplication
import com.gox.basemodule.base.BaseFragment
import com.gox.basemodule.data.PreferenceHelper
import com.gox.basemodule.data.clearAll
import com.gox.basemodule.common.payment.managepayment.ManagePaymentActivity
import com.gox.basemodule.utils.ViewCallBack
import com.gox.basemodule.utils.ViewUtils
import com.gox.basemodule.common.cardlist.ActivityCardList
import com.gox.app.R
import com.gox.app.adapter.AccountMenuAdapter
import com.gox.app.data.repositary.remote.model.AccountMenuModel
import com.gox.app.databinding.FragmentMyaccountBinding
import com.gox.app.ui.invitereferals.InviteReferalsActivity
import com.gox.app.ui.language_setting.LanguageActivity
import com.gox.basemodule.common.manage_address.ManageAddressActivity
import com.gox.app.ui.onboard.OnBoardActivity
import com.gox.app.ui.privacypolicy.PrivacyPolicyActivity
import com.gox.app.ui.profile.ProfileActivity
import com.gox.app.ui.signup.SignupActivity
import com.gox.app.ui.support.SupportActivity
import com.gox.basemodule.data.PreferenceKey
import com.gox.basemodule.data.getValue
import kotlinx.android.synthetic.main.fragment_myaccount.*

class MyAccountFragment : BaseFragment<FragmentMyaccountBinding>(), MyAccountFragmentNavigator {
    lateinit var mViewDataBinding: FragmentMyaccountBinding
    private var accountTitleArrayList: Array<String>? = arrayOf()
    private var accountIconArraylist: Array<Int>? = arrayOf()
    lateinit var accountAdapter: AccountMenuAdapter
    val preferenceHelper = PreferenceHelper(BaseApplication.baseApplication)
    val myAccountFragmentViewModel = MyAccountFragmentViewModel()
    override fun getLayoutId(): Int = R.layout.fragment_myaccount

    override fun initView(mRootView: View?, mViewDataBinding: ViewDataBinding?) {
        this.mViewDataBinding = mViewDataBinding as FragmentMyaccountBinding
        myAccountFragmentViewModel.navigator = this
        mViewDataBinding.accountFragment = this
        mViewDataBinding.myaccountfragmentviewmodel = myAccountFragmentViewModel
        (baseActivity).setSupportActionBar(mViewDataBinding.profileToolbar)
        setHasOptionsMenu(true)

        myAccountFragmentViewModel.successResponse.observe(this, Observer {
            if (it != null) {
                if (it.statusCode == "200") {
                    ViewUtils.showToast(activity!!, it.message, true)
                    preferenceHelper.clearAll()
                    openNewActivity(activity, OnBoardActivity::class.java, false)
                    baseActivity.finishAffinity()
                }
            }
        })

        toolbar_logo.setOnClickListener {
            logout()
        }

        getMenuItems()
    }

    fun getMenuItems() {
        val accessKey = preferenceHelper.getValue(PreferenceKey.ACCESS_TOKEN, "") as String
        if (accessKey.isNullOrEmpty()) {
            getGustMenuItems()
            toolbar_logo.visibility=View.GONE
        } else {
            getUserMenuItems()
            toolbar_logo.visibility=View.VISIBLE

        }

    }

    fun getUserMenuItems() {
        val accountTitleArrayList = resources.getStringArray(R.array.menuitems)
        val accountIconArraylist = resources.obtainTypedArray(R.array.menuicons)
        val accountMenus = List(accountTitleArrayList!!.size) {
            AccountMenuModel(accountTitleArrayList[it], accountIconArraylist.getResourceId(it, -1))
        }
        accountIconArraylist.recycle()
        myAccountFragmentViewModel.setAccountMenus(accountMenus)
        accountAdapter = AccountMenuAdapter(this, myAccountFragmentViewModel)
        accountAdapter.notifyDataSetChanged()
        mViewDataBinding.rvSettingMenu.adapter = accountAdapter
    }

    fun getGustMenuItems() {
        val accountTitleArrayList = resources.getStringArray(R.array.gustmenu_title)
        val accountIconArraylist = resources.obtainTypedArray(R.array.gustmenu_icon)
        val accountMenus = List(accountTitleArrayList!!.size) {
            AccountMenuModel(accountTitleArrayList[it], accountIconArraylist.getResourceId(it, -1))
        }
        accountIconArraylist.recycle()
        myAccountFragmentViewModel.setAccountMenus(accountMenus)
        accountAdapter = AccountMenuAdapter(this, myAccountFragmentViewModel)
        accountAdapter.notifyDataSetChanged()
        mViewDataBinding.rvSettingMenu.adapter = accountAdapter
    }


    private fun logout() {
        ViewUtils.showAlert(baseActivity, R.string.xjek_logout_alert, object : ViewCallBack.Alert {
            override fun onPositiveButtonClick(dialog: DialogInterface) {
                myAccountFragmentViewModel.Logout()
                dialog.dismiss()
            }

            override fun onNegativeButtonClick(dialog: DialogInterface) {
                dialog.dismiss()
            }
        })
    }


    fun onMenuItemClick(position: Int, accountMenuModel: AccountMenuModel) {

        if (!preferenceHelper.getValue(PreferenceKey.ACCESS_TOKEN, "")?.equals("")!!) {

            when (position) {

                0 -> {
                    openNewActivity(activity, ProfileActivity::class.java, false)
                }


                1 -> {
                    val intent = Intent(activity, ManageAddressActivity::class.java)
                    intent.putExtra("changeAddressFlag", "0")
                    startActivity(intent)
                }

                2 -> {
                    val intent = Intent(activity, ActivityCardList::class.java)
                    intent.putExtra("activity_result_flag", "0")
                    startActivity(intent)
                }

                3 -> {
                    val intent = Intent(activity, ManagePaymentActivity::class.java)
                    intent.putExtra("activity_result_flag", "0")
                    startActivity(intent)
                }

                4 -> {
                    openNewActivity(activity, PrivacyPolicyActivity::class.java, false)

                }

                5 -> {
                    openNewActivity(activity, SupportActivity::class.java, false)

                }

                6 -> {
                    openNewActivity(activity, LanguageActivity::class.java, false)

                }

                7 -> {
                    openNewActivity(activity, InviteReferalsActivity::class.java, false)
                }

            }
        } else {
            when (position) {
                0 -> {
                    openNewActivity(activity, LanguageActivity::class.java, false)
                }

                1 -> {
                    openNewActivity(activity, PrivacyPolicyActivity::class.java, false)
                }

                2 -> {
                    openNewActivity(activity, SignupActivity::class.java, false)
                }
            }
        }

    }
}
