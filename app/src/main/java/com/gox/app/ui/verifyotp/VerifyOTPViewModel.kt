package com.gox.app.ui.verifyotp

import androidx.lifecycle.MutableLiveData
import com.gox.app.data.repositary.AppRepository
import com.gox.app.data.repositary.remote.model.SendOTPResponse
import com.gox.app.data.repositary.remote.model.VerifyOTPResponse
import com.gox.basemodule.BuildConfig
import com.gox.basemodule.base.BaseViewModel
import com.gox.basemodule.repositary.ApiListener
import okhttp3.MediaType
import okhttp3.RequestBody

class VerifyOTPViewModel:BaseViewModel<VerifyOTPNavigator>(){
    var loadingProgress = MutableLiveData<Boolean>()
    var verifyOTPResponse = MutableLiveData<VerifyOTPResponse>()
    var errorResponse = MutableLiveData<String>()
    var country_code = MutableLiveData<String>()
    var phonenumber = MutableLiveData<String>()
    var sendOTPResponse = MutableLiveData<SendOTPResponse>()
    private val appRepository = AppRepository.instance()


    fun actionVerifyOTP(){
        navigator.verifyOTP()
    }


    fun verifyOTPApiCall(hashMap: HashMap<String, RequestBody>){
        getCompositeDisposable().add(appRepository.verifyOTP(this,hashMap))
    }


    fun resendOTP() {
        loadingProgress.value = true
        val hashMap: HashMap<String, RequestBody> = HashMap()
        hashMap.put("country_code", RequestBody.create(MediaType.parse("text/plain"), country_code.value!!.replace("+", "")))
        hashMap.put("mobile", RequestBody.create(MediaType.parse("text/plain"), phonenumber.value!!.toString()))
        hashMap.put("salt_key", RequestBody.create(MediaType.parse("text/plain"), BuildConfig.SALT_KEY))
        getCompositeDisposable().add(appRepository.sendOTP(object : ApiListener {
            override fun onSuccess(successData: Any) {
                loadingProgress.value = false
                sendOTPResponse.postValue(successData as SendOTPResponse)
            }

            override fun onError(error: Throwable) {
                loadingProgress.value = false
                errorResponse.postValue(getErrorMessage(error))
            }
        }, hashMap))
    }

}