package com.bee.deliverymodule.boxTypes

import androidx.lifecycle.MutableLiveData
import com.gox.basemodule.BaseApplication
import com.gox.basemodule.base.BaseViewModel
import com.gox.basemodule.data.PreferenceHelper
import com.gox.basemodule.data.PreferenceKey
import com.gox.basemodule.data.getValue
import com.gox.basemodule.repositary.ApiListener
import com.bee.deliverymodule.views.datamodel.BoxTypesDataModel
import com.bee.deliverymodule.views.datamodel.VechileTypeModel
import com.bee.deliverymodule.views.networkConfig.DeliverRespository

class BoxTypeViewModel : BaseViewModel<BoxTypeNavigator>() {
    var mlBoxType = MutableLiveData<BoxTypesDataModel>()
    var errorResponse = MutableLiveData<String>()
    var selectedBoxtype=MutableLiveData<String>()
    val preferenceHelper = PreferenceHelper(BaseApplication.baseApplication)
    val appRepository = DeliverRespository.instance()
    val loadingPrgress=MutableLiveData<Boolean>()

    fun getBoxtyps(){
        loadingPrgress.value=true
        getCompositeDisposable().addAll(appRepository.getBoxType(this, preferenceHelper.getValue(PreferenceKey.ACCESS_TOKEN, "").toString(), object : ApiListener {
            override fun onSuccess(successData: Any) {
                loadingPrgress.postValue(false)
                 mlBoxType.postValue(successData as BoxTypesDataModel)
            }

            override fun onError(error: Throwable) {
                loadingPrgress.postValue(false)
            }

        }))
    }

}