package com.bee.deliverymodule.boxTypes

import android.view.View
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.gox.basemodule.base.BaseFragment
import com.bee.delivermodule.R
import com.bee.delivermodule.databinding.FragmentBoxTypesBinding
import com.bee.deliverymodule.adapter.BoxTypeAdapter
import com.bee.deliverymodule.views.createOrder.CreateOrderMainFragment
import com.bee.deliverymodule.views.deliveryHome.DeliveryMainViewModel

class BoxTypeFragment : BaseFragment<FragmentBoxTypesBinding>() {
    private lateinit var fragmentBoxTypesBinding: FragmentBoxTypesBinding
    private lateinit var boxTypeViewModel: BoxTypeViewModel
    private lateinit var deliveryMainViewModel: DeliveryMainViewModel
    private lateinit var linearLayoutManager: LinearLayoutManager
    private lateinit var boxAdapter: BoxTypeAdapter

    override fun getLayoutId(): Int {
        return R.layout.fragment_box_types
    }

    override fun initView(mRootView: View?, mViewDataBinding: ViewDataBinding?) {
        fragmentBoxTypesBinding = mViewDataBinding as FragmentBoxTypesBinding
        boxTypeViewModel = ViewModelProviders.of(this).get(BoxTypeViewModel::class.java)
        deliveryMainViewModel = ViewModelProviders.of(activity!!).get(DeliveryMainViewModel::class.java)
        if(deliveryMainViewModel.isSingleDelivery.value!!)
            deliveryMainViewModel.mainTitle.postValue("Single Delivery")
        else
            deliveryMainViewModel.mainTitle.postValue("Multiple Delivery")
                //GetObserveValues
        getObserveValues()

        //InitalzieRecylerView
        initRecyler()

        //getBoxType
        boxTypeViewModel.getBoxtyps()
    }

    fun getObserveValues() {
        boxTypeViewModel.loadingPrgress.observe(this, Observer {
            baseLiveDataLoading.value = it
        })


        boxTypeViewModel.mlBoxType.observe(this, Observer {
            if (it != null && it.statusCode.equals("200")) {
                boxAdapter = BoxTypeAdapter(boxTypeViewModel, activity!!, it.responseData!!)
                fragmentBoxTypesBinding.rvBoxTypes.adapter = boxAdapter
            }
        })

        boxTypeViewModel.selectedBoxtype.observe(this, Observer {
            if (!it.isNullOrEmpty()) {
                deliveryMainViewModel.selectedBoxType.postValue(it)
                deliveryMainViewModel.changeFragment(CreateOrderMainFragment())
            }
        })
    }

    fun initRecyler() {
        linearLayoutManager = LinearLayoutManager(activity!!)
        fragmentBoxTypesBinding.rvBoxTypes.layoutManager = linearLayoutManager
    }

}