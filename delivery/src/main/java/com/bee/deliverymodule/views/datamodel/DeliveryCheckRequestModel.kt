package com.bee.deliverymodule.views.datamodel

data class DeliveryCheckRequestModel(
    var error: List<Any?>? = listOf(),
    var message: String? = "",
    var responseData: ResponseData? = ResponseData(),
    var statusCode: String? = "",
    var title: String? = ""
) {
    data class ResponseData(
        var data: List<Data?>? = listOf(),
        var emergency: List<Emergency?>? = listOf(),
        var response_time: String? = "",
        var sos: String? = ""
    ) {
        data class Data(
            var admin_id: Any? = Any(),
            var admin_service: String? = "",
            var assigned_at: String? = "",
            var assigned_time: String? = "",
            var booking_id: String? = "",
            var calculator: String? = "",
            var cancel_reason: Any? = Any(),
            var cancelled_by: Any? = Any(),
            var chat: Any? = Any(),
            var city_id: Any? = Any(),
            var country_id: Int? = 0,
            var created_at: String? = "",
            var created_time: String? = "",
            var currency: String? = "",
            var d_address: String? = "",
            var d_latitude: Double? = 0.0,
            var d_longitude: Double? = 0.0,
            var deliveries: List<Delivery> = listOf(),
            var delivery_mode: Any? = Any(),
            var delivery_type_id: Int? = 0,
            var delivery_vehicle_id: Int? = 0,
            var destination_log: String? = "",
            var distance: Double? = 0.0,
            var finished_at: String? = "",
            var finished_time: String? = "",
            var geofence_id: Int? = 0,
            var id: Int? = 0,
            var is_drop_location: Int? = 0,
            var is_scheduled: String? = "",
            var location_points: String? = "",
            var otp: String? = "",
            var paid: Int? = 0,
            var payable_amount: Double? = 0.0,
            var payment_mode: String? = "",
            var payment_by: String? = "",
            var peak: Int? = 0,
            var peak_hour_id: Any? = Any(),
            var promocode_id: Int? = 0,
            var provider: Provider? = Provider(),
            var provider_id: Int? = 0,
            var provider_rated: Int? = 0,
            var provider_service_id: Int? = 0,
            var provider_vehicle_id: Any? = Any(),
            var rating: Any? = Any(),
            var reasons: List<Any?>? = listOf(),
            var request_type: String? = "",
            var return_date: Any? = Any(),
            var route_key: String? = "",
            var s_address: String? = "",
            var s_latitude: Double? = 0.0,
            var s_longitude: Double? = 0.0,
            var schedule_at: Any? = Any(),
            var schedule_time: String? = "",
            var service: Service? = Service(),
            var service_type: ServiceType? = ServiceType(),
            var started_at: String? = "",
            var started_time: String? = "",
            var payment:Payment ,
            var status: String? = "",
            var surge: Int? = 0,
            var timezone: String? = "",
            var total_amount: Double? = 0.0,
            var track_distance: Int? = 0,
            var track_latitude: Double? = 0.0,
            var track_longitude: Double? = 0.0,
            var travel_time: Any? = Any(),
            var unit: String? = "",
            var use_wallet: Int? = 0,
            var user: User? = User(),
            var user_id: Int? = 0,
            var user_rated: Double? = 0.0,
            var weight: Double? = 0.0
        ) {
            data class Payment(
                    val card: Int = 0,
                    val cash: Int = 0,
                    val commision: Double = 0.0,
                    val commision_percent: Int = 0,
                    val delivery_id: Int = 0,
                    val discount: Double = 0.0,
                    val discount_percent: Double = 0.0,
                    val distance: Double = 0.0,
                    val fixed: Double = 0.0,
                    val fleet: Int = 0,
                    val fleet_id: Any = Any(),
                    val fleet_percent: Int = 0,
                    val id: Int = 0,
                    val is_partial: Any = Any(),
                    val payable: Double = 0.0,
                    val payment_id: Any = Any(),
                    val payment_mode: String = "",
                    val peak_amount: Int = 0,
                    val peak_comm_amount: Int = 0,
                    val promocode_id: String? = "",
                    val provider_id: Int = 0,
                    val provider_pay: Double = 0.0,
                    val round_of: Double = 0.0,
                    val tax: Double = 0.0,
                    val tax_percent: Double = 0.0,
                    val tips: Int = 0,
                    val total: Double = 0.0,
                    val total_waiting_time: Int = 0,
                    val user_id: Int = 0,
                    val wallet: Int = 0,
                    val weight: Int = 0
            )
            data class Delivery(
                var admin_id: Any? = Any(),
                var admin_service: String? = "",
                var assigned_at: String? = "",
                var currency: Any? = Any(),
                var d_address: String? = "",
                var d_latitude: Double? = 0.0,
                var d_longitude: Double? = 0.0,
                var delivery_request_id: Int? = 0,
                var destination_log: String? = "",
                var distance: Double? = 0.0,
                var finished_at: String? = "",
                var geofence_id: Any? = Any(),
                var id: Int? = 0,
                var instruction: String? = "",
                var is_fragile: Int? = 0,
                var location_points: String? = "",
                var mobile: String? = "",
                var name: String? = "",
                var otp: String? = "",
                var package_type: PackageType? = PackageType(),
                var package_type_id: Int? = 0,
                var paid: Int? = 0,
                var payment: Payment? = Payment(),
                var payment_mode: String? ="",
                var picture: Any? = Any(),
                var provider_id: Int? = 0,
                var provider_rated: Int? = 0,
                var route_key: Any? = Any(),
                var s_address: String? = "",
                var s_latitude: Int? = 0,
                var s_longitude: Int? = 0,
                var schedule_at: Any? = Any(),
                var started_at: String? = "",
                var status: String? = "",
                var surge: Double? = 0.0,
                var timezone: Any? = Any(),
                var track_distance: Int? = 0,
                var track_latitude: Int? = 0,
                var track_longitude: Int? = 0,
                var travel_time: String? = "",
                var unit: String? = "",
                var user_id: Any? = Any(),
                var weight:Double? = 0.0
            ) {
                data class PackageType(
                    var id: Int? = 0,
                    var package_name: String? = "",
                    var status: Int? = 0
                )

                data class Payment(
                    var card: Int? = 0,
                    var cash: Int? = 0,
                    var commision: Double? = 0.0,
                    var commision_percent: Int? = 0,
                    var delivery_id: Int? = 0,
                    var discount: Double? = 0.0,
                    var discount_percent: Int? = 0,
                    var distance: Double? = 0.0,
                    var fixed: Double? = 0.0,
                    var fleet: Double? = 0.0,
                    var fleet_id: Any? = Any(),
                    var fleet_percent:Double? = 0.0,
                    var id: Int? = 0,
                    var is_partial: Any? = Any(),
                    var payable: Double? = 0.0,
                    var payment_id: Any? = Any(),
                    var payment_mode: String? = "",
                    var peak_amount:Double? = 0.0,
                    var peak_comm_amount: Int? = 0,
                    var promocode_id: Any? = Any(),
                    var provider_id: Int? = 0,
                    var provider_pay: Double? = 0.0,
                    var round_of: Double? = 0.0,
                    var tax: Double? = 0.0,
                    var tax_percent:Double? = 0.0,
                    var tips:Double? = 0.0,
                    var total: Double? = 0.0,
                    var total_waiting_time: Int? = 0,
                    var user_id: Int? = 0,
                    var wallet: Int? = 0,
                    var weight: Double? = 0.0
                )
            }

            data class Provider(
                var activation_status: Int? = 0,
                var admin_id: Any? = Any(),
                var city_id: Int? = 0,
                var country_code: String? = "",
                var country_id: Int? = 0,
                var currency: String? = "",
                var currency_symbol: String? = "",
                var current_location: Any? = Any(),
                var current_ride_vehicle: Int? = 0,
                var current_store: Any? = Any(),
                var device_id: Any? = Any(),
                var device_token: String? = "",
                var device_type: String? = "",
                var email: String? = "",
                var first_name: String? = "",
                var gender: String? = "",
                var id: Int? = 0,
                var is_assigned: Int? = 0,
                var is_bankdetail: Int? = 0,
                var is_document: Int? = 0,
                var is_online: Int? = 0,
                var is_service: Int? = 0,
                var language: String? = "",
                var last_name: String? = "",
                var latitude: Double? = 0.0,
                var login_by: String? = "",
                var longitude: Double? = 0.0,
                var mobile: String? = "",
                var otp: Any? = Any(),
                var payment_gateway_id: Any? = Any(),
                var payment_mode: String? = "",
                var picture: String? = "",
                var qrcode_url: String? = "",
                var rating: Double? = 0.0,
                var referal_count: Int? = 0,
                var referral_unique_id: String? = "",
                var social_unique_id: Any? = Any(),
                var state_id: Int? = 0,
                var status: String? = "",
                var stripe_cust_id: Any? = Any(),
                var wallet_balance: Double? = 0.0,
                var zone_id: Any? = Any()
            )

            data class Service(
                var capacity: Int? = 0,
                var delivery_types_id: Int? = 0,
                var id: Int? = 0,
                var status: Int? = 0,
                var vehicle_image: String? = "",
                var vehicle_marker: String? = "",
                var vehicle_name: String? = "",
                var vehicle_type: String? = ""
            )

            data class ServiceType(
                var admin_service: String? = "",
                var base_fare: Double? = 0.0,
                var category_id: Int? = 0,
                var company_id: Int? = 0,
                var delivery_vehicle_id: Int? = 0,
                var id: Int? = 0,
                var per_miles:Double? = 0.0,
                var per_mins: Double? = 0.0,
                var provider_id: Int? = 0,
                var provider_vehicle_id: Int? = 0,
                var ride_delivery_id: Any? = Any(),
                var service_id: Any? = Any(),
                var status: String? = "",
                var sub_category_id: Any? = Any(),
                var vehicle: Vehicle? = Vehicle()
            ) {
                data class Vehicle(
                    var child_seat: Int? = 0,
                    var company_id: Int? = 0,
                    var id: Int? = 0,
                    var picture: String? = "",
                    var picture1: String? = "",
                    var provider_id: Int? = 0,
                    var vechile_image: Any? = Any(),
                    var vehicle_color: String? = "",
                    var vehicle_make: String? = "",
                    var vehicle_model: String? = "",
                    var vehicle_no: String? = "",
                    var vehicle_service_id: Int? = 0,
                    var vehicle_year: Int? = 0,
                    var wheel_chair: Int? = 0
                )
            }

            data class User(
                var city_id: Any? = Any(),
                var company_id: Int? = 0,
                var country_code: String? = "",
                var country_id: Int? = 0,
                var created_at: String? = "",
                var currency_symbol: String? = "",
                var email: String? = "",
                var first_name: String? = "",
                var gender: String? = "",
                var id: Int? = 0,
                var language: String? = "",
                var last_name: String? = "",
                var latitude: Any? = Any(),
                var login_by: String? = "",
                var longitude: Any? = Any(),
                var mobile: String? = "",
                var payment_mode: String? = "",
                var picture: Any? = Any(),
                var rating: Double? = 0.0,
                var referral_unique_id: String? = "",
                var state_id: Int? = 0,
                var status: Int? = 0,
                var user_type: String? = "",
                var wallet_balance:Double? = 0.0
            )
        }

        data class Emergency(
            var number: String? = ""
        )
    }
}