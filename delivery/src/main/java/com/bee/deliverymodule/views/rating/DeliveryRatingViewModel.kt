package com.bee.deliverymodule.views.rating

import androidx.lifecycle.MutableLiveData
import com.gox.basemodule.BaseApplication
import com.gox.basemodule.base.BaseViewModel
import com.gox.basemodule.data.Constant
import com.gox.basemodule.data.PreferenceHelper
import com.gox.basemodule.data.PreferenceKey
import com.gox.basemodule.data.getValue
import com.gox.basemodule.repositary.ApiListener
import com.bee.deliverymodule.views.datamodel.RatingSuccessModel
import com.bee.deliverymodule.views.networkConfig.DeliverRespository

class  DeliveryRatingViewModel:BaseViewModel<DeliveryRatingNavigator>(){
    var errorResponse= MutableLiveData<String>()
    var ratingSuccess=MutableLiveData<RatingSuccessModel>()
    val preferenceHelper = PreferenceHelper(BaseApplication.baseApplication)
    val appRepository = DeliverRespository.instance()
    val reqID=MutableLiveData<Int>()
    val strComments=MutableLiveData<String>("")
    val rating=MutableLiveData<Int>(1)
    var loaderPrgress=MutableLiveData<Boolean>()

    fun submitRating(){
        loaderPrgress.value=true
        val hashMap: HashMap<String, Any> = HashMap()
        hashMap["id"] = reqID.value.toString()
        hashMap["rating"] = rating!!.value!!.toInt()
        hashMap["comment"] = strComments.value.toString()
        hashMap["admin_service"] ="DELIVERY"
        getCompositeDisposable().add(appRepository.setRating(this, Constant.M_TOKEN
                + preferenceHelper.getValue(PreferenceKey.ACCESS_TOKEN, "").toString(), hashMap,object:ApiListener{
            override fun onSuccess(successData: Any) {
                loaderPrgress.value=false
                ratingSuccess.postValue(successData as RatingSuccessModel)
            }

            override fun onError(error: Throwable) {
                loaderPrgress.value=false
            }
        }))

    }
}