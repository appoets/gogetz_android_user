package com.bee.deliverymodule.views.datamodel

data class BoxTypesDataModel(
    var error: List<Any?>? = listOf(),
    var message: String? = "",
    var responseData: List<ResponseData> = listOf(),
    var statusCode: String? = "",
    var title: String? = ""
) {
    data class ResponseData(
        var delivery_name: String? = "",
        var id: Int? = 0
    )
}