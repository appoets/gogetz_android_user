package com.bee.deliverymodule.views.datamodel

data class EstimatedFareDataModel(
    var error: List<Any?>? = listOf(),
    var message: String? = "",
    var responseData: ResponseData? = ResponseData(),
    var statusCode: String? = "",
    var title: String? = ""
) {
    data class ResponseData(
        var currency: String? = "",
        var fare: Fare? = Fare(),
        var promocodes: List<Any?>? = listOf(),
        var service: Service? = Service()
    ) {
        data class Fare(
            var base_price: Double? = 0.0,
            var distance: Double? = 0.0,
            var estimated_fare: Double? = 0.0,
            var peak: Double? = 0.0,
            var peak_percentage: Int? = 0,
            var service: String? = "",
            var service_type: Int? = 0,
            var tax_price: Double? = 0.0,
            var unit: String? = "",
            var wallet_balance: Double? = 0.0,
            var weight: String? = ""
        )

        data class Service(
            var capacity: Int? = 0,
            var delivery_types_id: Int? = 0,
            var id: Int? = 0,
            var status: Int? = 0,
            var vehicle_image: String? = "",
            var vehicle_marker: String? = "",
            var vehicle_name: String? = "",
            var vehicle_type: String? = ""
        )
    }
}