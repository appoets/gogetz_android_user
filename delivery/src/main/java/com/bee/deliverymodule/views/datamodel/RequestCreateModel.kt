package com.bee.deliverymodule.views.datamodel

data class RequestCreateModel(
    var error: List<Any?>? = listOf(),
    var message: String? = "",
    var responseData: ResponseData? = ResponseData(),
    var statusCode: String? = "",
    var title: String? = ""
) {
    data class ResponseData(
        var message: String? = "",
        var request: Int? = 0
    )
}