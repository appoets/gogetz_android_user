package com.bee.deliverymodule.views.vechiletype

import androidx.lifecycle.MutableLiveData
import com.gox.basemodule.BaseApplication
import com.gox.basemodule.base.BaseViewModel
import com.gox.basemodule.data.PreferenceHelper
import com.gox.basemodule.data.PreferenceKey
import com.gox.basemodule.data.getValue
import com.gox.basemodule.repositary.ApiListener
import com.bee.deliverymodule.views.datamodel.DeliveryCheckRequestModel
import com.bee.deliverymodule.views.datamodel.VechileTypeModel
import com.bee.deliverymodule.views.networkConfig.DeliverRespository

class AddVechileViewModel : BaseViewModel<AddVechileInterface>() {
    var loadingProgress = MutableLiveData<Boolean>()
    var vechileTypesModel = MutableLiveData<VechileTypeModel>()
    val appRepository = DeliverRespository.instance()
    var addressLiveData=MutableLiveData<String>()
    val preferenceHelper = PreferenceHelper(BaseApplication.baseApplication)
    var errorResponse = MutableLiveData<String>()
    var mLatitude=MutableLiveData<String>()
    var mLongitude=MutableLiveData<String>()
    var selectedPosition=MutableLiveData<String>()
    var selectedVechileType=MutableLiveData<String>()


    fun gotoItemDetailPage() {
        navigator.goToItemDetailPage()
    }

    fun getVechileType(type:String){
        loadingProgress.value = true
        getCompositeDisposable().addAll(appRepository.callVechileTypes(this, preferenceHelper.getValue(PreferenceKey.ACCESS_TOKEN, "").toString(),type, object : ApiListener {
            override fun onSuccess(successData: Any) {
                loadingProgress.postValue(false)
                vechileTypesModel.postValue(successData as VechileTypeModel)
            }

            override fun onError(error: Throwable) {
                loadingProgress.postValue(false)
            }

        }))

    }
}