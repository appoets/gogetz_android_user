package com.bee.deliverymodule.views.itemDetail

import android.content.Intent
import android.provider.ContactsContract
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.AdapterView
import android.widget.RadioGroup
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.gox.basemodule.adapter.PlaceListAdapter
import com.gox.basemodule.base.BaseActivity
import com.gox.basemodule.data.Constant
import com.gox.basemodule.data.PlacesModel
import com.gox.basemodule.utils.PlacesAutocompleteUtil
import com.gox.basemodule.utils.ViewUtils
import com.bee.delivermodule.R
import com.bee.delivermodule.databinding.ActivityPackageDetailBinding
import com.bee.deliverymodule.views.datamodel.ItemDetailModel
import com.bee.deliverymodule.views.datamodel.PackageTypeModel
import com.bee.deliverymodule.views.deliveryHome.DeliveryMainViewModel
import com.bee.deliverymodule.views.deliveryList.DeliveryListViewModel
import com.bumptech.glide.Glide
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.model.LatLng
import com.google.gson.Gson
import com.gox.basemodule.utils.createMultipartBody
import com.theartofdev.edmodo.cropper.CropImage
import kotlinx.android.synthetic.main.activity_package_detail.*
import java.io.File

class ItemDetailActivity : BaseActivity<ActivityPackageDetailBinding>(), ItemDetailNavigator, AdapterView.OnItemClickListener {
    private lateinit var activityPackageDetailBinding: ActivityPackageDetailBinding
    private lateinit var itemDetailViewModel: ItemDetailViewModel
    private var deliveryDetailList: ItemDetailModel = ItemDetailModel()
    private var prediction: ArrayList<PlacesModel> = ArrayList()
    private lateinit var placesAutocompleteUtil: PlacesAutocompleteUtil
    private var countryCode: String? = ""
    private lateinit var textWatcher: TextWatcher
    private  var packageData: ArrayList<PackageTypeModel.ResponseData> = ArrayList()



    override fun getLayoutId(): Int {
        return R.layout.activity_package_detail
    }

    override fun initView(mViewDataBinding: ViewDataBinding?) {
        activityPackageDetailBinding = mViewDataBinding as ActivityPackageDetailBinding
        itemDetailViewModel = ViewModelProviders.of(this).get(ItemDetailViewModel::class.java)
        itemDetailViewModel.navigator = this
        activityPackageDetailBinding.itemDetailViewModel = itemDetailViewModel
        placesAutocompleteUtil = PlacesAutocompleteUtil(this)
        activityPackageDetailBinding.itemDetailActivity = this
        textWatcher = PlaceTextLisnter()
        itemDetailViewModel.getPackageType()
        activityPackageDetailBinding.atvDeliverAddress.addTextChangedListener(textWatcher)
        //GetIntentValues
        getIntentValues()
        activityPackageDetailBinding.isFragileYes.setOnClickListener {
            itemDetailViewModel.mIsFragile.value = true
        }
        activityPackageDetailBinding.isFragileNo.setOnClickListener {
            itemDetailViewModel.mIsFragile.value = false
        }
        txt_category_selection.setOnClickListener {
//            spinnerpackageTypeCategory.expand()
        }
        itemDetailViewModel.packageTypeDetails.observe(this, Observer {
            if(it.statusCode.equals("200")){
            if (!it.responseData.isNullOrEmpty()) {
                packageData.clear()
                packageData.addAll(it.responseData)
                val packageName:ArrayList<String> = ArrayList()
                for(pack in packageData){
                    packageName.add(pack.package_name!!)
                }
//                spinnerpackageTypeCategory.setItems(packageName)
                if (itemDetailViewModel.packageTypeID.value != 0) {
                    val vehiclePosition = packageData!!.indexOfFirst { data -> data?.id == itemDetailViewModel.packageTypeID.value }
//                    spinnerpackageTypeCategory.selectedIndex = vehiclePosition
                    txt_category_selection.setText(packageData.get(vehiclePosition)!!.package_name)
                } else {
//                    spinnerpackageTypeCategory.selectedIndex = 0
                    txt_category_selection.setText(packageData!!.get(0)!!.package_name)
                    itemDetailViewModel.packageTypeID.value = packageData!!.get(0)!!.id
                }
            }
            }
        })

//        spinnerpackageTypeCategory.setOnItemSelectedListener { view, position, id, item ->
            run {
                txt_category_selection.setText(item.toString())
                itemDetailViewModel.packageTypeID.value = packageData[position].id
            }
        }


//        activityPackageDetailBinding.isFragile.setOnCheckedChangeListener(RadioGroup.OnCheckedChangeListener {
//            radioGroup, i ->
//            itemDetailViewModel.mIsFragile.value = radioGroup.checkedRadioButtonId.equals(R.id.isFragileYes)
//        })

    }

    fun getIntentValues() {
        countryCode = if (intent != null && intent.hasExtra("countryCode")) intent.getStringExtra("countryCode") else ""
        if(intent.hasExtra("deliveryDetailList")) {
            activityPackageDetailBinding.tvrepeattlast.visibility = View.GONE
//            deliveryDetailList = intent.getSerializableExtra("deliveryDetailList") as ItemDetailModel
        }else{
            activityPackageDetailBinding.tvrepeattlast.visibility = View.GONE
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE -> {
                if(data != null){
                    val result = CropImage.getActivityResult(data)
                    activityPackageDetailBinding.ivCamera.visibility = View.GONE
                    if(!result.uri.path.isNullOrEmpty()){
                        itemDetailViewModel.imagePath.postValue(result.uri.path)
                        Glide.with(this).load(File(result.uri.path))
                                .placeholder(R.drawable.default_image_placeholder)
                                .into(activityPackageDetailBinding.ivPackageImage)}
                }
            }
        }
    }


    override fun onReset() {
//        activityPackageDetailBinding.atvDeliverAddress.setText("")
        activityPackageDetailBinding.edtDeliveryInstruction.setText("")
        activityPackageDetailBinding.edtMobileNumber.setText("")
        activityPackageDetailBinding.edtPackageDetail.setText("")
        activityPackageDetailBinding.edtPackageType.setText("")
        activityPackageDetailBinding.edtReciverName.setText("")
        activityPackageDetailBinding.edtLength.setText("")
        activityPackageDetailBinding.edtBreadth.setText("")
        activityPackageDetailBinding.edtHeight.setText("")
        activityPackageDetailBinding.edtWeight.setText("")
        activityPackageDetailBinding.isFragileYes.setChecked(false)
        activityPackageDetailBinding.isFragileNo.setChecked(true)
        itemDetailViewModel.mLength.value = ""
        itemDetailViewModel.mBreadth.value = ""
        itemDetailViewModel.mHeight.value = ""
        itemDetailViewModel.mWeight.value = ""
        itemDetailViewModel.mlReciverName.value = ""
        itemDetailViewModel.mlDeliveryAddress.value = ""
        itemDetailViewModel.mlReciverPhone.value = ""
        itemDetailViewModel.packageTypeID.value = 1
        itemDetailViewModel.mlPackageType.value = ""
        itemDetailViewModel.mlPackDetail.value = ""
        itemDetailViewModel.mlDeliveryInstruction.value = ""
        itemDetailViewModel.mIsFragile.value = false
        itemDetailViewModel.getPackageType()
    }

    override fun onSubmit() {
        if (activityPackageDetailBinding.atvDeliverAddress.getText().isNullOrEmpty())
            com.gox.basemodule.utils.ViewUtils.showToast(this, resources.getString(R.string.empty_delivery_address), false)
        else if (activityPackageDetailBinding.edtDeliveryInstruction.getText().isNullOrEmpty())
            com.gox.basemodule.utils.ViewUtils.showToast(this, resources.getString(R.string.empty_instruction), false)
        else if (activityPackageDetailBinding.edtReciverName.getText().isNullOrEmpty())
            com.gox.basemodule.utils.ViewUtils.showToast(this, resources.getString(R.string.empty_reciver_name), false)
        else if (activityPackageDetailBinding.edtMobileNumber.getText().isNullOrEmpty())
            com.gox.basemodule.utils.ViewUtils.showToast(this, resources.getString(R.string.empty_phone_number), false)
        else if (activityPackageDetailBinding.edtPackageDetail.getText().isNullOrEmpty())
            com.gox.basemodule.utils.ViewUtils.showToast(this, resources.getString(R.string.empty_package_detail), false)
//        else if (activityPackageDetailBinding.edtLength.getText().isNullOrEmpty())
//            ViewUtils.showToast(this, resources.getString(R.string.empty_length), false)
//        else if (activityPackageDetailBinding.edtBreadth.getText().isNullOrEmpty())
//            ViewUtils.showToast(this, resources.getString(R.string.empty_breadth), false)
//        else if (activityPackageDetailBinding.edtHeight.getText().isNullOrEmpty())
//            ViewUtils.showToast(this, resources.getString(R.string.empty_height), false)
        else if (activityPackageDetailBinding.edtWeight.getText().isNullOrEmpty())
            ViewUtils.showToast(this, resources.getString(R.string.empty_weight), false)
        else if (itemDetailViewModel.imagePath.value.isNullOrEmpty())
            ViewUtils.showToast(this, resources.getString(R.string.empty_image), false)
        else {
            itemDetailViewModel.mlDeliveryAddress.value = activityPackageDetailBinding.atvDeliverAddress.getText().toString()
            val itemDetailModel = ItemDetailModel(itemDetailViewModel.mlReciverName.value!!,
                    itemDetailViewModel.mlReciverPhone.value!!,
                    itemDetailViewModel.mlDeliveryInstruction.value!!,
                    itemDetailViewModel.packageTypeID.value!!,
                    itemDetailViewModel.mlPackDetail.value!!,
                    itemDetailViewModel.mlDeliveryAddress.value!!,
                    itemDetailViewModel.mLatitude.value!!,
                    itemDetailViewModel.mLongitude.value!!,
                    itemDetailViewModel.mLength.value!!,
                    itemDetailViewModel.mBreadth.value!!,
                    itemDetailViewModel.mHeight.value!!,
                    itemDetailViewModel.mWeight.value!!,
                    if(itemDetailViewModel.mIsFragile.value!!){"1"}else{"0"},
                    itemDetailViewModel.imagePath.value!!)

            val intent = Intent()
            val strPkgDetail = Gson().toJson(itemDetailModel)
            intent.putExtra("pkgDetail", strPkgDetail)
            setResult(100, intent)
            finish()
        }

    }

    override fun onRepeat() {
//        val reatelast = deliveryDetailList
//        activityPackageDetailBinding.edtDeliveryInstruction.setText(reatelast.deliveryInstruction)
//        activityPackageDetailBinding.edtMobileNumber.setText(reatelast.reciverPhoneNumber)
//        activityPackageDetailBinding.edtPackageDetail.setText(reatelast.packageDetail)
//        activityPackageDetailBinding.edtPackageType.setText(reatelast.packageType)
//        activityPackageDetailBinding.edtReciverName.setText(reatelast.reciverName)
//        activityPackageDetailBinding.edtLength.setText(reatelast.length.toString())
//        activityPackageDetailBinding.edtBreadth.setText(reatelast.breadth.toString())
//        activityPackageDetailBinding.edtHeight.setText(reatelast.height.toString())
//        activityPackageDetailBinding.edtWeight.setText(reatelast.weight.toString())
//        spinnerpackageTypeCategory.selectedIndex = reatelast.packageType
//        txt_category_selection.setText(packageData!!.get(reatelast.packageType)!!.package_name)
//        itemDetailViewModel.packageTypeID.value = packageData!!.get(reatelast.packageType)!!.id
//        if(reatelast.is_fragile.equals("1")){
//            activityPackageDetailBinding.isFragileYes.setChecked(true)
//            activityPackageDetailBinding.isFragileNo.setChecked(false)
//        }else{
//            activityPackageDetailBinding.isFragileYes.setChecked(false)
//            activityPackageDetailBinding.isFragileNo.setChecked(true)
//        }
    }

    inner class PlaceTextLisnter : TextWatcher {
        override fun afterTextChanged(p0: Editable?) {
        }

        override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
        }

        override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
            placesAutocompleteUtil.findAutocompletePredictions(s.toString(), countryCode!!,
                    object : PlacesAutocompleteUtil.PlaceSuggestion {
                        override fun onPlaceReceived(mPlacesList: ArrayList<PlacesModel>?) {
                            prediction = mPlacesList!!
                            var adapter = PlaceListAdapter(this@ItemDetailActivity, prediction)
                            activityPackageDetailBinding.atvDeliverAddress.setAdapter(adapter)
                            adapter.notifyDataSetChanged()
                            activityPackageDetailBinding.atvDeliverAddress.setOnItemClickListener(this@ItemDetailActivity)
                        }
                    })

        }
    }

    override fun onItemClick(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        if (prediction.size > 0) try {
            val mPlace = prediction[position]
            val mLatLng = placesAutocompleteUtil.getPlaceName(mPlace.mPlaceId, object : PlacesAutocompleteUtil.onLoccationListner {
                override fun onLatLngRecived(placeLatLng: LatLng) {
                    if (placeLatLng.latitude != 0.0 && placeLatLng.longitude != 0.0) {
                        itemDetailViewModel.mlDeliveryAddress.value = prediction[position].mFullAddress
                        activityPackageDetailBinding.atvDeliverAddress.removeTextChangedListener(textWatcher)
                        activityPackageDetailBinding.atvDeliverAddress.setText(prediction[position].mFullAddress.toString())
                        itemDetailViewModel.mLatitude.value = placeLatLng.latitude
                        itemDetailViewModel.mLongitude.value = placeLatLng.longitude
                        itemDetailViewModel.mlDeliveryAddress.value = prediction[position].mFullAddress.toString()
                        hideKeyboard()
                    }
                }
            })
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun captureImage() {
        val intent = CropImage.activity()
                .getIntent(this)

        startActivityForResult(intent, CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE)
    }

}