package com.bee.deliverymodule.views.deliveryHome

import android.Manifest
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Address
import android.location.Geocoder
import android.location.Location
import android.location.LocationManager
import android.net.Uri
import android.provider.Settings
import android.text.TextUtils
import android.widget.FrameLayout
import androidx.core.app.ActivityCompat
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModelProviders
import com.bee.delivermodule.R
import com.bee.delivermodule.databinding.DeliveryActivityMainBinding
import com.bee.deliverymodule.boxTypes.BoxTypeFragment
import com.bee.deliverymodule.views.createOrder.CreateOrderMainFragment
import com.bee.deliverymodule.views.deliveryType.DeliveryTypeFragment
import com.bee.deliverymodule.views.deliverystatus.DeliveryStatusPageFragment
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.MapsInitializer
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.gox.basemodule.base.BaseActivity
import com.gox.basemodule.utils.*
import kotlinx.android.synthetic.main.header_delivery.view.*
import permissions.dispatcher.*
import java.io.IOException
import java.util.*

@RuntimePermissions
class DeliveryMainActivity : BaseActivity<DeliveryActivityMainBinding>(), DeliveryMainNavigator {

    private lateinit var mDeliveryActivityMainBinding: DeliveryActivityMainBinding
    private lateinit var mDeliveryMainViewModel: DeliveryMainViewModel
    private lateinit var fragmentMap: SupportMapFragment
    private lateinit var sheetBehavior: BottomSheetBehavior<FrameLayout>
    private var checkRequestTimer: Timer? = null
    private var mGoogleMap: GoogleMap? = null
    private var mLastKnownLocation: Location? = null
    var loadingProgress = MutableLiveData<Boolean>()
    var enabled: Boolean? = null
    private var currentFragment: String? = ""
    private var strCountryName: String? = ""
    private var strCountryCode: String? = ""
    private var isFragmentInBackstack: Boolean = false
    private var isDeliveryTypeShowing: Boolean = false
    private var isDeliveyStatuShowing: Boolean = false


    private lateinit var mPlacesAutocompleteUtil: PlacesAutocompleteUtil
    override fun getLayoutId(): Int = R.layout.delivery_activity_main

    override fun initView(mViewDataBinding: ViewDataBinding?) {
        mDeliveryActivityMainBinding = mViewDataBinding as DeliveryActivityMainBinding
        mDeliveryMainViewModel = ViewModelProviders.of(this).get(DeliveryMainViewModel::class.java)
        mDeliveryMainViewModel.navigator = this
        mDeliveryActivityMainBinding.deliveryMainModel = mDeliveryMainViewModel
        mDeliveryActivityMainBinding.executePendingBindings()
        mPlacesAutocompleteUtil = PlacesAutocompleteUtil(applicationContext)
        val locationManager = getSystemService(LOCATION_SERVICE) as LocationManager?
        enabled = locationManager?.isProviderEnabled(LocationManager.GPS_PROVIDER)
        checkRequestTimer = Timer()

        MapsInitializer.initialize(this@DeliveryMainActivity)

        //getIntentValues
        getIntentValues()

        updateLocationWithPermissionCheck()

        getObserverValues()
        mDeliveryMainViewModel.showLoading.postValue(true)
        checkRequestTimer!!.schedule(object : TimerTask() {
            override fun run() {
                mDeliveryMainViewModel.callCheckRequest()
            }
        }, 0, 5000)

        mDeliveryActivityMainBinding.deliveryToolbar.ivDeliverLeft.setOnClickListener { onBackPressed() }
    }

    override fun onDestroy() {
        super.onDestroy()
        checkRequestTimer?.cancel()
    }

    fun getObserverValues() {
//        mDeliveryMainViewModel.loadingProgress.observe(this, androidx.lifecycle.Observer {
//            baseLiveDataLoading.value = it
//        })
        mDeliveryMainViewModel.showLoading.observe(this, androidx.lifecycle.Observer {
            baseLiveDataLoading.postValue(it)
        })

        mDeliveryMainViewModel.errorResponse.observe(this, androidx.lifecycle.Observer {
            //ViewUtils.showToast(this,"Please check the network.",false)
            loadDeliveryTypeFragment()
           // onBackPressed()
        })
        mDeliveryMainViewModel.mainTitle.observe(this, androidx.lifecycle.Observer {
            mDeliveryActivityMainBinding.deliveryToolbar.tbrDeliverTitle.text = mDeliveryMainViewModel.mainTitle.value
        })

        mDeliveryMainViewModel.deliveryCheckRequestModel.observe(this, androidx.lifecycle.Observer {
            if (it != null) {
                if (it.responseData!!.data.isNullOrEmpty() && isDeliveryTypeShowing == false) {
                    loadDeliveryTypeFragment()
                } else if (it.responseData!!.data!!.size > 0 && isDeliveyStatuShowing == false) {
                    mDeliveryActivityMainBinding.deliveryToolbar.tbrDeliverTitle.text = ""
                    isDeliveyStatuShowing = true
                    mDeliveryMainViewModel.showLoading.postValue(false)
                    loadFragment(DeliveryStatusPageFragment())
                }
            }
        })
    }

    fun loadDeliveryTypeFragment(){
        if (isDeliveryTypeShowing == false) {
            isDeliveryTypeShowing = true
            mDeliveryMainViewModel.showLoading.postValue(false)
            mDeliveryActivityMainBinding.deliveryToolbar.tbrDeliverTitle.text = "Choose your delivery type"
            loadFragment(DeliveryTypeFragment())
        }
    }

    private fun getIntentValues() {
        mDeliveryMainViewModel.strCountryname.value = if (intent != null && intent.hasExtra("countryName")) intent.getStringExtra("countryName") else ""
        mDeliveryMainViewModel.strCountryCode.value = if (intent != null && intent.hasExtra("countryCode")) intent.getStringExtra("countryCode") else ""

        if (!mDeliveryMainViewModel.strCountryname.value.isNullOrEmpty()) {
            getLatLng(mDeliveryMainViewModel.strCountryname.value!!)
        }
    }

    @NeedsPermission(Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION)
    fun updateLocation() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return
        }
        LocationUtils.getLastKnownLocation(this, object : LocationCallBack.LastKnownLocation {
            override fun onSuccess(location: Location) {
                mLastKnownLocation = location
            }

            override fun onFailure(messsage: String?) {

            }
        })
    }

    private fun getLatLng(countryName: String) {
        val geoCoder = Geocoder(this)
        var addressList: List<Address>? = listOf()
        try {
            addressList = geoCoder.getFromLocationName(countryName, 1)
        } catch (e: IOException) {
            e.printStackTrace()
        }
        if (!addressList.isNullOrEmpty()) {
            val address = addressList!![0]
//            mDeliveryMainViewModel.strAddress.postValue(address.getAddressLine(0))
//            mDeliveryMainViewModel.pickupLatLong.value = LatLng(address.latitude, address.longitude)
//            mDeliveryMainViewModel.strCountryCode.value = LocationUtils.getCountryCode(this, LatLng(address.latitude, address.longitude))
        }
    }

    @OnShowRationale(Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION)
    fun showRationaleForlocation(request: PermissionRequest) {

    }

    @OnPermissionDenied(Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION)
    fun onLocationAccessDenied() {
        ViewUtils.showAlert(this, R.string.location_permission_denied,object : ViewCallBack.Alert{
            override fun onPositiveButtonClick(dialog: DialogInterface) {
                val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
                val uri: Uri = Uri.fromParts("package", packageName, null)
                startActivity(intent)
                dialog.dismiss()
            }
            override fun onNegativeButtonClick(dialog: DialogInterface) {

            }
        })
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        // NOTE: delegate the permission handling to generated function
        onRequestPermissionsResult(requestCode, grantResults)
    }

    override fun loadFragment(fragment: Fragment) {
        val ft = supportFragmentManager.beginTransaction()
        val backStackName = fragment::class.java.simpleName
        if (backStackName.equals(CreateOrderMainFragment::class.java))
            addBackStack(false, ft, backStackName, fragment)
        else
            addBackStack(true, ft, backStackName, fragment)


    }

    override fun tootlBarTitle(title: String) {
        mDeliveryActivityMainBinding.deliveryToolbar.tbrDeliverTitle.setText(title)
    }


    private fun addBackStack(
            isReplaceAll: Boolean,
            ft: FragmentTransaction,
            backStackName: String,
            callingFragment: Fragment
    ) {
        if (currentFragment.equals(backStackName)) return
        if (isReplaceAll) {
            if (replaceFragment(backStackName) == false) {
                //ClearTopFragment
                if (backStackName.equals(DeliveryStatusPageFragment::class.java.simpleName)) {
                    clearBackStack()
                }
                ft.replace(R.id.deliveryContainer, callingFragment, backStackName)
                ft.addToBackStack(callingFragment::class.java.simpleName)
                ft.commit()
                return
            }
        } else {
            ft.replace(R.id.deliveryContainer, callingFragment, backStackName)
            ft.addToBackStack(callingFragment::class.java.simpleName)
            ft.commit()
        }
    }

    private fun clearBackStack() {
        val fragmentCount = supportFragmentManager.backStackEntryCount
        if (fragmentCount > 1) {
            val clearBackStackName = supportFragmentManager.getBackStackEntryAt(0).name
            supportFragmentManager.popBackStackImmediate(clearBackStackName, FragmentManager.POP_BACK_STACK_INCLUSIVE)
        }
    }

    fun getTopFragment(): Fragment? {
        supportFragmentManager.run {
            return when (backStackEntryCount) {
                0 -> null
                else -> findFragmentByTag(getBackStackEntryAt(backStackEntryCount - 1).name)
            }
        }
    }


    fun onBack() {
        val fragCount = supportFragmentManager.backStackEntryCount
        if (fragCount == 0) {
            finish()
        } else {
            onBackPressed()
        }
    }

    override fun onBackPressed() {
        val fragCount = supportFragmentManager.backStackEntryCount
        if (fragCount == 1) {
            finish()
        } else {
            val frag = getTopFragment()
            if (frag != null) {
                val strSimple = frag!!.javaClass.simpleName
                if (frag!!.javaClass.simpleName.equals(CreateOrderMainFragment::class.java.simpleName)) {
                    val childFragmentManager = frag.childFragmentManager
                    if (childFragmentManager.backStackEntryCount > 1) {
                        childFragmentManager.popBackStackImmediate()
                    } else {
                        //  supportFragmentManager.popBackStackImmediate()
                        supportFragmentManager.popBackStackImmediate(BoxTypeFragment::class.java.simpleName, FragmentManager.POP_BACK_STACK_INCLUSIVE)
                        /*childFragmentManager.fragments.clear()
                    supportFragmentManager.beginTransaction().remove(frag).commit()
                    supportFragmentManager.popBackStack()*/
                        /*  val fragmentList = childFragmentManager.fragments
                    if (!fragmentList.isNullOrEmpty()) {
                        for (fragment in fragmentList) {
                            childFragmentManager.beginTransaction().remove(fragment).commit()
                        }
                    }
                    frag.childFragmentManager.*/
                        //supportFragmentManager.popBackStackImmediate(CreateOrderMainFragment::class.java.simpleName,FragmentManager.POP_BACK_STACK_INCLUSIVE)
                        /// supportFragmentManager.executePendingTransactions()
                        //    super.onBackPressed()
                    }
                } else {
                    if (frag.javaClass.simpleName.equals(DeliveryTypeFragment::class.java.simpleName)) {
                        mDeliveryMainViewModel.selectedVechileID.value = ""
                        finish()
                    } else {
                        mDeliveryMainViewModel.selectedVechileID.value = ""
                        super.onBackPressed()
                    }
                }
            } else {
                mDeliveryMainViewModel.selectedVechileID.value = ""
                super.onBackPressed()
            }
        }
    }

    private fun replaceFragment(backStackName: String): Boolean {
        val manager: FragmentManager = supportFragmentManager
        isFragmentInBackstack = false
        val count: Int = manager.getBackStackEntryCount()
        if (count > 0) {
            for (i in 0 until count) {
                if (manager.getBackStackEntryAt(i) != null &&
                        manager.getBackStackEntryAt(i).getName().isNullOrEmpty()
                        && !TextUtils.isEmpty(backStackName)
                ) {
                    if (manager.getBackStackEntryAt(i).getName().equals(backStackName)) {
                        manager.popBackStackImmediate(backStackName, 0)
                        isFragmentInBackstack = true
                        break
                    }
                }
            }
        }
        return isFragmentInBackstack

    }

}
