package com.bee.deliverymodule.views.estimatePackage

import android.app.Activity.RESULT_CANCELED
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.RadioGroup
import android.widget.Toast
import androidx.appcompat.content.res.AppCompatResources.getDrawable
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.bee.delivermodule.R
import com.bee.delivermodule.databinding.FragmentPackageEstimationBinding
import com.bee.deliverymodule.views.createOrder.CreateOrderNavigator
import com.bee.deliverymodule.views.datamodel.RequestSchduleModel
import com.bee.deliverymodule.views.deliveryHome.DeliveryMainActivity
import com.bee.deliverymodule.views.deliveryHome.DeliveryMainViewModel
import com.bee.deliverymodule.views.deliveryList.DeliveryListFragment
import com.bee.deliverymodule.views.deliverySchdule.DeliverySchduleDialogFragment
import com.bee.deliverymodule.views.deliverystatus.DeliveryStatusPageFragment
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.MapsInitializer
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.gox.basemodule.base.BaseFragment
import com.gox.basemodule.common.cardlist.ActivityCardList
import com.gox.basemodule.data.Constant
import com.gox.basemodule.utils.AppUtils
import com.gox.basemodule.utils.ViewCallBack
import com.gox.basemodule.utils.ViewUtils
import com.gox.basemodule.utils.createMultipartBody
import kotlinx.android.synthetic.main.fragment_package_estimation.*
import okhttp3.MediaType
import okhttp3.RequestBody
import org.json.JSONObject
import java.io.File

class EstimatePackageFragment : BaseFragment<FragmentPackageEstimationBinding>(), EstimatePackageNavigator, RadioGroup.OnCheckedChangeListener, OnMapReadyCallback {


    private lateinit var fragmentPackageEstimationBinding: FragmentPackageEstimationBinding
    private lateinit var viewModel: EstimagePackageViewModel
    private lateinit var deliveryMainViewModel: DeliveryMainViewModel
    private lateinit var deliveryMainActivity: DeliveryMainActivity
    private var mScheduleDateTime: RequestSchduleModel? = null
    private lateinit var fragmentHomeTransActionMap: SupportMapFragment
    private var googleApiClient: GoogleApiClient? = null
    private lateinit var mGoogleMap: GoogleMap
    //private val reqEstimateModel = ReqEstimateModel()
    private var mCardId: String? = null

    companion object {
        var orderNavigator: CreateOrderNavigator? = null
        fun getEstimagePageFragment(orderCreateOrderNavigator: CreateOrderNavigator): EstimatePackageFragment {
            orderNavigator = orderCreateOrderNavigator
            return EstimatePackageFragment()
        }
    }

    override fun getLayoutId(): Int {
        return R.layout.fragment_package_estimation
    }

    /*override fun onAttach(context: Context?) {
        super.onAttach(context)
        deliveryMainActivity = context as DeliveryMainActivity
    }*/

    override fun initView(mRootView: View?, mViewDataBinding: ViewDataBinding?) {
        deliveryMainActivity = context as DeliveryMainActivity
        fragmentPackageEstimationBinding = mViewDataBinding as FragmentPackageEstimationBinding
        viewModel = ViewModelProviders.of(this).get(EstimagePackageViewModel::class.java)
        deliveryMainViewModel = ViewModelProviders.of(activity!!).get(DeliveryMainViewModel::class.java)
        viewModel.navigator = this
        fragmentPackageEstimationBinding.estimateViewModel = viewModel
        fragmentPackageEstimationBinding.estimateFragment = this
        fragmentPackageEstimationBinding.deliveryRbPayment.setOnCheckedChangeListener(this)
        fragmentPackageEstimationBinding.responsibleRadiogroup.setOnCheckedChangeListener(this)

        orderNavigator!!.changeStepProgress(EstimatePackageFragment::class.java.simpleName)

        /* if (BaseApplication.getCustomPreference!!.getString("Selected_model", "Model") != null)
             deliveryMainActivity.tootlBarTitle(BaseApplication.getCustomPreference!!.getString("Selected_model", "Model").toString())*/


        //DeliveryTitle
//        if (!deliveryMainViewModel.selectedVechileName.value.isNullOrEmpty())
//            fragmentPackageEstimationBinding.deliveryTitle.setText(deliveryMainViewModel.selectedVechileName.value.toString())

        //GetObserverValues
        getObserValues()

        //GetEstimation
        getEstimation()
        btnChange.setOnClickListener {
            val intent = Intent(activity!!, ActivityCardList::class.java)
            intent.putExtra("activity_result_flag", "1")
            startActivityForResult(intent, Constant.PAYMENT_TYPE_REQUEST_CODE)
        }
    }


    fun getObserValues() {
        viewModel.loadingProgress.observe(this, Observer {
            baseLiveDataLoading.value = it
        })

        viewModel.errorResponse.observe(this, Observer {
            ViewUtils.showToast(activity!!, it, false)
        })

        deliveryMainViewModel.getScheduleDateTimeData().observe(this, Observer {
            mScheduleDateTime = it
            fragmentPackageEstimationBinding.tvLblDeliverySchedule.visibility = View.VISIBLE
            fragmentPackageEstimationBinding.tvSchedule.visibility = View.GONE
            fragmentPackageEstimationBinding.rlSchdule.visibility = View.VISIBLE
            viewModel.schduleDate.value = mScheduleDateTime?.scheduleDate!!
            viewModel.schduleTime.value = mScheduleDateTime?.scheduleTime!!
            fragmentPackageEstimationBinding.scheduleDateTime.text = ("Scheduled on" + " " + mScheduleDateTime?.scheduleTime!!)
        })

        viewModel.mlEstimagedFarDataModel.observe(this, Observer {
            if (it != null && it.statusCode.equals("200")) {
                if (it.responseData!!.fare!!.base_price != null)
                    fragmentPackageEstimationBinding.tvBaseFare.setText(AppUtils.getNumberFormat()!!.format(it.responseData!!.fare!!.base_price))
                if (it.responseData!!.fare!!.tax_price != null)
                    fragmentPackageEstimationBinding.tvTax.setText(AppUtils.getNumberFormat()!!.format(it.responseData!!.fare!!.tax_price))
                if (it.responseData!!.fare!!.estimated_fare != null)
                    fragmentPackageEstimationBinding.tvTotalFare.setText(AppUtils.getNumberFormat()!!.format(it.responseData!!.fare!!.estimated_fare))
                if (!it.responseData!!.fare!!.distance.toString().isEmpty()) {
                    fragmentPackageEstimationBinding.tvDistanceFare.setText(AppUtils.getNumberFormat()!!.format(it.responseData!!.fare!!.distance))
                    viewModel.distance.value = it.responseData!!.fare!!.distance.toString()
                }
                fragmentPackageEstimationBinding.tvDurationFare.setText(AppUtils.getNumberFormat()!!.format(0.0))
                fragmentPackageEstimationBinding.deliveryTitle.setText(it.responseData!!.service!!.vehicle_name)

            }
        })

        viewModel.mCreateDeliveryResponseModel.observe(this, Observer {
            if (it != null && it.statusCode.equals("200")) {
                if((it.message?:"").contains("Schedule",true)){
                    ViewUtils.showToast(activity!!,it.message,true)
                    activity?.finish()
                }else{
                val deliveryStatusFragment = DeliveryStatusPageFragment()
                val bundle = Bundle()
                bundle.putBoolean("isSearch", true)
                deliveryStatusFragment.arguments = bundle
                deliveryMainActivity.loadFragment(deliveryStatusFragment)
                }
            }
        })
    }

    fun getEstimation() {
        viewModel.loadingProgress.value = true
        val estimateMap = HashMap<String, Any>()
        for (itemDetail in deliveryMainViewModel.deliveryDetailList.value!!) {
            viewModel.deliveryLatitude.value!!.add(itemDetail.deliveryLatitude)
            viewModel.deliveryLongitude.value!!.add(itemDetail.deliveryLongitude)
            viewModel.reciverName.value!!.add(itemDetail.reciverName)
            viewModel.reciverInstruction.value!!.add(itemDetail.deliveryInstruction)
            viewModel.IsFragileList.value!!.add(itemDetail.is_fragile)
            viewModel.weightList.value!!.add(itemDetail.weight)
            viewModel.LengthList.value!!.add(itemDetail.length)
            viewModel.BreadthList.value!!.add(itemDetail.breadth)
            viewModel.HeightList.value!!.add(itemDetail.height)
            viewModel.packateTypesList.value!!.add(itemDetail.packageType)
            viewModel.reciverPhone.value!!.add(itemDetail.reciverPhoneNumber)
        }
        viewModel.weight.value = "10"
        viewModel.distance.value = "100"

        if (!deliveryMainViewModel.selectedVechileID.value.isNullOrEmpty())
            viewModel.serviceType.value = deliveryMainViewModel.selectedVechileID.value
        else
            viewModel.serviceType.value = "1"

        if (deliveryMainViewModel.selectedBoxType.value.isNullOrEmpty())
            viewModel.deliveryType.value = "1"
        else
            viewModel.deliveryType.value = deliveryMainViewModel.selectedBoxType.value
        if (tvPaymentDetails.text.toString().equals("CASH",true)) {
            viewModel.paymentMode.value = "CASH"
        } else {
            viewModel.paymentMode.value = Constant.PaymentMode.PAYSTACK
        }
        estimateMap.put("s_latitude", deliveryMainViewModel.pickupLatLong.value!!.latitude)
        estimateMap.put("s_longitude", deliveryMainViewModel.pickupLatLong.value!!.longitude)
        estimateMap.put("service_type", deliveryMainViewModel.selectedVechileID.value!!)
//        estimateMap.put("d_latitude[]", viewModel.deliveryLatitude.value!!.get(0))
//        estimateMap.put("d_longitude[]", viewModel.deliveryLongitude.value!!.get(0))
        estimateMap.put("distance", 10)
        estimateMap.put("delivery_type_id", deliveryMainViewModel.selectedBoxType.value!!)
        estimateMap.put("payment_mode", viewModel.paymentMode.value!!)
        estimateMap.put("payment_by", "SENDER")


//        estimateMap.put("receiver_name[]", viewModel.reciverName.value!!)
//        estimateMap.put("receiver_mobile[]", viewModel.reciverPhone.value!!)
//        estimateMap.put("receiver_instruction[]", viewModel.reciverInstruction.value!!)
//        estimateMap.put("weight[]", viewModel.weightList.value!!)
//        estimateMap.put("length[]", viewModel.LengthList.value!!)
//        estimateMap.put("breadth[]", viewModel.BreadthList.value!!)
//        estimateMap.put("height[]", viewModel.HeightList.value!!)
//        estimateMap.put("is_fragile[]", viewModel.isFregile.value!!)
        // val requestBody = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), JSONObject(estimateMap as Map<*, *>).toString())
        viewModel.callEstimateApi(estimateMap)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (resultCode != RESULT_CANCELED) {
            when (requestCode) {
                Constant.PAYMENT_TYPE_REQUEST_CODE -> {
                    val paymentType = data?.extras?.get("payment_type").toString()
                    val card_id = data?.extras?.get("card_id").toString()
                    if (paymentType.equals("cash", true)) {
                        ivPaymentType.setImageDrawable(activity!!.getDrawable(R.drawable.ic_xuber_money))
                        tvPaymentDetails.text = paymentType.toString().toUpperCase()

                    } else {
                        if(fragmentPackageEstimationBinding.rbreceiver.isChecked){
                            Toast.makeText(activity!!, "Paystack is not available of Receiver side payment", Toast.LENGTH_LONG).show()
                            ivPaymentType.setImageDrawable(activity!!.getDrawable(R.drawable.ic_xuber_money))
                            tvPaymentDetails.text = "CASH"
                            viewModel.paymentMode.value = "CASH"
                        } else {
                            ivPaymentType.setImageDrawable(activity!!.getDrawable(R.drawable.ic_xuber_credit_card))
                            tvPaymentDetails.text = Constant.PaymentMode.PAYSTACK
                            mCardId = card_id
                        }
                    }
                }
            }
        }
    }


    override fun submitDetail() {
        viewModel.loadingProgress.value = true
        val estimateMap = HashMap<String, Any>()
        viewModel.deliveryLatitude.value!!.clear()
        viewModel.deliveryLongitude.value!!.clear()
        viewModel.reciverName.value!!.clear()
        viewModel.reciverPhone.value!!.clear()
        viewModel.weightList.value!!.clear()
        viewModel.LengthList.value!!.clear()
        viewModel.BreadthList.value!!.clear()
        viewModel.HeightList.value!!.clear()
        viewModel.IsFragileList.value!!.clear()
        viewModel.deliveryAddress.value!!.clear()
        viewModel.packateTypesList.value!!.clear()
        viewModel.distanceList.value!!.clear()
        viewModel.imageList.value!!.clear()
        for (i in 0 until deliveryMainViewModel.deliveryDetailList.value!!.size) {
            val itemDetail = deliveryMainViewModel.deliveryDetailList.value!!.get(i)
            viewModel.deliveryLatitude.value!!.add(itemDetail.deliveryLatitude)
            viewModel.deliveryLongitude.value!!.add(itemDetail.deliveryLongitude)
            viewModel.reciverName.value!!.add(itemDetail.reciverName)
            viewModel.reciverPhone.value!!.add(itemDetail.reciverPhoneNumber)
            viewModel.weightList.value!!.add(itemDetail.weight)
            viewModel.LengthList.value!!.add(itemDetail.length)
            viewModel.BreadthList.value!!.add(itemDetail.breadth)
            viewModel.HeightList.value!!.add(itemDetail.height)
            viewModel.IsFragileList.value!!.add(itemDetail.is_fragile)
            viewModel.deliveryAddress.value!!.add(itemDetail.deliveryAddress)
            viewModel.packateTypesList.value!!.add(itemDetail.packageType)
            viewModel.distanceList.value!!.add(10)
            viewModel.imageList.value!!.add(createMultipartBody("picture[]", "image/*",
                            File(itemDetail.picture)))
//            viewModel.imageList.value!!.add(itemDetail.imagePath)
        }

        viewModel.distance.value = "100"
        if (!deliveryMainViewModel.selectedVechileID.value.isNullOrEmpty())
            viewModel.serviceType.value = deliveryMainViewModel.selectedVechileID.value
        else
            viewModel.serviceType.value = "1"

        if (deliveryMainViewModel.selectedBoxType.value.isNullOrEmpty())
            viewModel.deliveryType.value = "1"
        else
            viewModel.deliveryType.value = deliveryMainViewModel.selectedBoxType.value

        if (tvPaymentDetails.text.toString().equals("CASH",true)) {
            viewModel.paymentMode.value = "CASH"
        }else{
            viewModel.paymentMode.value = Constant.PaymentMode.PAYSTACK
        }
        estimateMap.put("s_latitude", deliveryMainViewModel.pickupLatLong.value!!.latitude)
        estimateMap.put("s_longitude", deliveryMainViewModel.pickupLatLong.value!!.longitude)
        estimateMap.put("service_type", deliveryMainViewModel.selectedVechileID.value!!.toInt())
        estimateMap.put("s_address", deliveryMainViewModel.strAddress.value!!)
        estimateMap.put("delivery_type_id", viewModel.deliveryType.value!!.toInt())
        estimateMap.put("payment_mode", viewModel.paymentMode.value!!)
        if( fragmentPackageEstimationBinding.rbsender.isChecked){
            estimateMap.put("payment_by", "SENDER")
        }else if(fragmentPackageEstimationBinding.rbreceiver.isChecked){
            estimateMap.put("payment_by", "RECEIVER")
        }
        if (!viewModel.schduleTime.value.isNullOrEmpty())
            estimateMap.put("schedule_time", viewModel.schduleTime.value!!)
        if(!viewModel.schduleDate.value.isNullOrEmpty())
            estimateMap.put("schedule_date", viewModel.schduleDate.value!!)

        viewModel.sendDeliveryRequest(estimateMap)
    }

    override fun onCheckedChanged(group: RadioGroup?, checkedId: Int) {
        when (checkedId) {
            R.id.rbcard -> {
                fragmentPackageEstimationBinding.rbreceiver.visibility = View.GONE
                fragmentPackageEstimationBinding.rbsender.visibility = View.VISIBLE
                fragmentPackageEstimationBinding.rbreceiver.setChecked(false)
                fragmentPackageEstimationBinding.rbsender.setChecked(true)
                viewModel.paymentMode.value = Constant.PaymentMode.PAYSTACK
            }

            R.id.rbcash -> {
                fragmentPackageEstimationBinding.rbsender.visibility = View.VISIBLE
                fragmentPackageEstimationBinding.rbreceiver.visibility = View.VISIBLE
                viewModel.paymentMode.value = "CASH"
            }

            R.id.rbsender -> {

            }

            R.id.rbreceiver -> {
                if(tvPaymentDetails.text == Constant.PaymentMode.PAYSTACK) {
                    Toast.makeText(activity!!, "Paystack is not available of Receiver side payment", Toast.LENGTH_LONG).show()
                    ivPaymentType.setImageDrawable(activity!!.getDrawable(R.drawable.ic_xuber_money))
                    tvPaymentDetails.text = "CASH"
                    viewModel.paymentMode.value = "CASH"
                }
            }

        }
    }


    override fun editSchdule() {
        openScheduleUI()
    }

    override fun deleteSchdule() {
        ViewUtils.showAlert(activity!!, R.string.taxi_cancel_schedule_ride, object : ViewCallBack.Alert {
            override fun onPositiveButtonClick(dialog: DialogInterface) {
                val mScheduleRide = RequestSchduleModel()
                mScheduleRide.scheduleDate = ""
                mScheduleRide.scheduleTime = ""
                deliveryMainViewModel.setScheduleDateTime(mScheduleRide)
                viewModel.schduleTime.value = ""
                viewModel.schduleDate.value = ""
                fragmentPackageEstimationBinding.rlSchdule.visibility = View.GONE
                // mConfirmPageFragmentBinding.btnSchedule.visibility = View.VISIBLE
                dialog.dismiss()
            }

            override fun onNegativeButtonClick(dialog: DialogInterface) {
                dialog.dismiss()
            }
        })
    }

    fun openScheduleUI() {
        val mScheduleFragment = DeliverySchduleDialogFragment()
        mScheduleFragment.show(activity!!.supportFragmentManager, mScheduleFragment.tag)
    }

    override fun onMapReady(p0: GoogleMap?) {

    }


}

