package com.bee.deliverymodule.views.deliveryList

import android.content.Intent
import android.content.res.Resources
import android.location.Address
import android.location.Geocoder
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.AdapterView
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.bee.delivermodule.R
import com.bee.delivermodule.databinding.FragmentDeliveryListBinding
import com.bee.deliverymodule.adapter.ClickItemAdapterPostion
import com.bee.deliverymodule.adapter.MultiAddressAdapter
import com.bee.deliverymodule.views.createOrder.CreateOrderNavigator
import com.bee.deliverymodule.views.datamodel.ItemDetailModel
import com.bee.deliverymodule.views.datamodel.PackageTypeModel
import com.bee.deliverymodule.views.deliveryHome.DeliveryMainViewModel
import com.bee.deliverymodule.views.estimatePackage.EstimatePackageFragment
import com.bee.deliverymodule.views.itemDetail.ItemDetailActivity
import com.bumptech.glide.Glide
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.*
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MapStyleOptions
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.gox.basemodule.adapter.PlaceListAdapter
import com.gox.basemodule.base.BaseFragment
import com.gox.basemodule.data.Constant
import com.gox.basemodule.data.PlacesModel
import com.gox.basemodule.utils.PlacesAutocompleteUtil
import com.gox.basemodule.utils.ViewUtils
import com.gox.basemodule.utils.createMultipartBody
import com.theartofdev.edmodo.cropper.CropImage
import kotlinx.android.synthetic.main.activity_package_detail.*
import java.io.File
import java.io.IOException

class DeliveryListFragment : BaseFragment<FragmentDeliveryListBinding>(), DeliveryListNavigator, AdapterView.OnItemClickListener, OnMapReadyCallback  {

    private lateinit var mfragmentViewBinding: FragmentDeliveryListBinding
    private lateinit var deliveryListViewModel: DeliveryListViewModel
    private lateinit var deliveryMainViewModel: DeliveryMainViewModel
    private lateinit var linearLayoutManager: LinearLayoutManager
    private lateinit var placesAutocompleteUtil: PlacesAutocompleteUtil
    private var prediction: ArrayList<PlacesModel> = ArrayList()
    private lateinit var textWatcher: PlaceTextLisnter
    private lateinit var multiAddressAdpater: MultiAddressAdapter
    private lateinit var fragmentDeliveryAddressFragment: SupportMapFragment
    private var googleApiClient: GoogleApiClient? = null
    private lateinit var mGoogleMap: GoogleMap
    private  var packageData: ArrayList<PackageTypeModel.ResponseData> = ArrayList()


    companion object {
        private var orderNavigator: CreateOrderNavigator? = null
        fun getDeliveryListFragment(createOrderNavigator: CreateOrderNavigator): Fragment {
            orderNavigator = createOrderNavigator
            return DeliveryListFragment()
        }
    }

    override fun getLayoutId(): Int {
        return R.layout.fragment_delivery_list
    }


    override fun initView(mRootView: View?, mViewDataBinding: ViewDataBinding?) {
        mfragmentViewBinding = mViewDataBinding as FragmentDeliveryListBinding
        deliveryListViewModel = ViewModelProviders.of(this).get(DeliveryListViewModel::class.java)
        deliveryMainViewModel = ViewModelProviders.of(activity!!).get(DeliveryMainViewModel::class.java)
        deliveryListViewModel.navigator = this
        mfragmentViewBinding.deliveryListViewModel = deliveryListViewModel
        mfragmentViewBinding.deliveryListFragment = this
        placesAutocompleteUtil = PlacesAutocompleteUtil(activity!!)
        textWatcher = PlaceTextLisnter()
        deliveryListViewModel.getPackageType()
        mfragmentViewBinding.atvPlaces.addTextChangedListener(textWatcher)
        deliveryListViewModel.isSingleDelivery.value = deliveryMainViewModel.isSingleDelivery.value
        orderNavigator!!.changeStepProgress(DeliveryListFragment::class.java.simpleName)

        updateView()

        //initalizeMap
        initialiseMap()


        //setUpclient
        setUpClient()

        //InitRecyclerView
        initRecylerView()
        mfragmentViewBinding.isFragileYes.setOnClickListener {
            deliveryListViewModel.isFragile.value = true
        }
        mfragmentViewBinding.isFragileNo.setOnClickListener {
            deliveryListViewModel.isFragile.value = false
        }

//        mfragmentViewBinding.isFragile.setOnCheckedChangeListener(RadioGroup.OnCheckedChangeListener {
//            radioGroup, i ->
//            deliveryListViewModel.isFragile.value = radioGroup.checkedRadioButtonId.equals(R.id.isFragileYes)
//        })

    }

    private fun initialiseMap() {
        MapsInitializer.initialize(activity)
        fragmentDeliveryAddressFragment = childFragmentManager.findFragmentById(R.id.addressMap) as SupportMapFragment
        fragmentDeliveryAddressFragment.getMapAsync(this)
    }

    fun setUpClient() {
        googleApiClient = GoogleApiClient.Builder(activity!!)
                .addApi(LocationServices.API)
                .build()
        googleApiClient!!.connect()
    }


    fun updateView() {
        mfragmentViewBinding.tvPickupAddress.setText(deliveryMainViewModel.strAddress.value)
        if (deliveryMainViewModel.isSingleDelivery.value!!) {
            mfragmentViewBinding.llSingleItem.visibility = View.VISIBLE
            mfragmentViewBinding.clDeliveryList.visibility = View.GONE
            orderNavigator!!.hideStepProgress(false)
        } else {
            mfragmentViewBinding.llSingleItem.visibility = View.GONE
            mfragmentViewBinding.clDeliveryList.visibility = View.VISIBLE
            orderNavigator!!.hideStepProgress(true)
        }

        mfragmentViewBinding.txtCategorySelection.setOnClickListener {
            spinnerpackageTypeCategory.expand()
        }

        deliveryListViewModel.packageTypeDetails.observe(this, Observer {
            if (!it.responseData.isNullOrEmpty()) {
                packageData.clear()
                packageData.addAll(it.responseData)
                val packageName:ArrayList<String> = ArrayList()
                for(pack in packageData){
                    packageName.add(pack.package_name!!)
                }
                spinnerpackageTypeCategory.setItems(packageName)
                if (deliveryListViewModel.packageTypeID.value != 0) {
                    val vehiclePosition = packageData!!.indexOfFirst { data -> data?.id == deliveryListViewModel.packageTypeID.value }
                    spinnerpackageTypeCategory.selectedIndex = vehiclePosition
                    mfragmentViewBinding.txtCategorySelection.setText(packageData.get(vehiclePosition)!!.package_name)
                } else {
                    spinnerpackageTypeCategory.selectedIndex = 0
                    mfragmentViewBinding.txtCategorySelection.setText(packageData!!.get(0)!!.package_name)
                    deliveryListViewModel.packageTypeID.value = packageData!!.get(0)!!.id
                }
            }
        })

        spinnerpackageTypeCategory.setOnItemSelectedListener { view, position, id, item ->
            run {
                mfragmentViewBinding.txtCategorySelection.setText(item.toString())
                deliveryListViewModel.packageTypeID.value = packageData[position].id
            }
        }
    }

    fun initRecylerView() {
        linearLayoutManager = LinearLayoutManager(activity)
        mfragmentViewBinding.rvDeliveryAddress.layoutManager = linearLayoutManager
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        //super.onActivityResult(requestCode, resultCode, data)
when(requestCode){
    CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE -> {
        if(data != null){
            val result = CropImage.getActivityResult(data)
            mfragmentViewBinding.ivCamera.visibility = View.GONE
            if(!result.uri.path.isNullOrEmpty()){
                deliveryListViewModel.imagePath.postValue(result.uri.path)
                Glide.with(this).load(File(result.uri.path))
                        .placeholder(R.drawable.default_image_placeholder)
                        .into(mfragmentViewBinding.ivPackageImage)}
        }
    }
}
        when (resultCode) {
            CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE -> {
                if(data != null){
                    val result = CropImage.getActivityResult(data)
                    mfragmentViewBinding.ivCamera.visibility = View.GONE
                    if(!result.uri.path.isNullOrEmpty()){
                        deliveryListViewModel.imagePath.postValue(result.uri.path)
                        Glide.with(this).load(File(result.uri.path))
                                .placeholder(R.drawable.default_image_placeholder)
                                .into(mfragmentViewBinding.ivPackageImage)}
                }
            }

            100 -> {
                if (data != null && data.hasExtra("pkgDetail")) {
                    val deliveryTypeToken = object : TypeToken<ItemDetailModel>() {}.type
                    val itemDetailModel = Gson().fromJson<ItemDetailModel>(data!!.getStringExtra("pkgDetail"), deliveryTypeToken)
                    if (itemDetailModel != null) {
                        deliveryMainViewModel.deliveryDetailList.value!!.add(itemDetailModel)
                        if (!deliveryMainViewModel.deliveryDetailList.value.isNullOrEmpty()) {
                            if (deliveryMainViewModel.deliveryDetailList.value!!.size < 2) {
                                multiadpater()
                            } else {
                                multiAddressAdpater.notifyItemRangeChanged(0, deliveryMainViewModel.deliveryDetailList.value!!.size - 1)
                            }
                        }
                    }
                }
            }
        }
    }

    override fun gotoEstimatepage() {
        orderNavigator!!.changeOrderCrateFrag(EstimatePackageFragment::class.java.simpleName)
    }

    override fun gotoItemDetailPage() {
        val intent = Intent(activity, ItemDetailActivity::class.java)
        intent.putExtra("countryName", deliveryMainViewModel.strCountryname.value)
        intent.putExtra("countryCode", deliveryMainViewModel.strCountryCode.value)
//        if(deliveryMainViewModel.deliveryDetailList.value!!.size != 0){
//        val deliveryDetailListdata  = deliveryMainViewModel.deliveryDetailList.value!!.get(deliveryMainViewModel.deliveryDetailList.value!!.size-1)
//        intent.putExtra("deliveryDetailList",deliveryDetailListdata)
//        }
        startActivityForResult(intent, 101)
    }

    override fun reset() {
        mfragmentViewBinding.atvPlaces.setText("")
        mfragmentViewBinding.edtDeliveryInstruction.setText("")
        mfragmentViewBinding.edtMobileNumber.setText("")
        mfragmentViewBinding.edtPackageDetail.setText("")
        mfragmentViewBinding.edtPackageType.setText("")
        mfragmentViewBinding.edtReciverName.setText("")
        mfragmentViewBinding.edtWeight.setText("")
        mfragmentViewBinding.edtLength.setText("")
        mfragmentViewBinding.edtBreadth.setText("")
        mfragmentViewBinding.edtHeight.setText("")
        mfragmentViewBinding.isFragileYes.setChecked(false)
        mfragmentViewBinding.isFragileNo.setChecked(true)
        deliveryListViewModel.isFragile.value = false
        deliveryListViewModel.getPackageType()

    }

    override fun onResume() {
        super.onResume()
        multiadpater()
    }

    override fun submit() {
        orderNavigator!!.changeOrderCrateFrag(EstimatePackageFragment::class.java.simpleName)
    }

    override fun validate(): Boolean {
        var isValid: Boolean = false
        if (mfragmentViewBinding.atvPlaces.getText().isNullOrEmpty())
            com.gox.basemodule.utils.ViewUtils.showToast(activity!!, resources.getString(R.string.empty_delivery_address), false)
        else if (deliveryListViewModel.mDeliveryInstruction.value.isNullOrEmpty())
            com.gox.basemodule.utils.ViewUtils.showToast(activity!!, resources.getString(R.string.empty_instruction), false)
        else if (deliveryListViewModel.mReciverName.value.isNullOrEmpty())
            com.gox.basemodule.utils.ViewUtils.showToast(activity!!, resources.getString(R.string.empty_reciver_name), false)
        else if (deliveryListViewModel.mReciverPhone.value.isNullOrEmpty())
            com.gox.basemodule.utils.ViewUtils.showToast(activity!!, resources.getString(R.string.empty_phone_number), false)
        else if (deliveryListViewModel.packageType.value.isNullOrEmpty())
            com.gox.basemodule.utils.ViewUtils.showToast(activity!!, resources.getString(R.string.empty_package_type), false)
        else if (deliveryListViewModel.packageDetail.value.isNullOrEmpty())
            com.gox.basemodule.utils.ViewUtils.showToast(activity!!, resources.getString(R.string.empty_package_detail), false)
//        else if (deliveryListViewModel.length.value.isNullOrEmpty())
//            com.gox.basemodule.utils.ViewUtils.showToast(activity!!, resources.getString(R.string.empty_length), false)
//        else if (deliveryListViewModel.breadth.value.isNullOrEmpty())
//            com.gox.basemodule.utils.ViewUtils.showToast(activity!!, resources.getString(R.string.empty_breadth), false)
//        else if (deliveryListViewModel.height.value.isNullOrEmpty())
//            com.gox.basemodule.utils.ViewUtils.showToast(activity!!, resources.getString(R.string.empty_height), false)
        else if (deliveryListViewModel.weight.value.isNullOrEmpty())
            com.gox.basemodule.utils.ViewUtils.showToast(activity!!, resources.getString(R.string.empty_weight), false)
        else if (deliveryListViewModel.imagePath.value.isNullOrEmpty())
            ViewUtils.showToast(activity!!, resources.getString(R.string.empty_image), false)
        else {

            val itemDetailModel = ItemDetailModel(deliveryListViewModel.mReciverName.value!!,
                    deliveryListViewModel.mReciverPhone.value!!,
                    deliveryListViewModel.mDeliveryInstruction.value!!,
                    deliveryListViewModel.packageTypeID.value!!,
                    deliveryListViewModel.packageDetail.value!!,
                    mfragmentViewBinding.atvPlaces.getText().toString(),
                    deliveryListViewModel.mLatitude.value!!,
                    deliveryListViewModel.mLongitude.value!!,
                    deliveryListViewModel.length.value!!,
                    deliveryListViewModel.breadth.value!!,
                    deliveryListViewModel.height.value!!,
                    deliveryListViewModel.weight.value!!,
                    if(deliveryListViewModel.isFragile.value!!){"1"}else{"0"},
                    deliveryListViewModel.imagePath.value!!)

            if (deliveryMainViewModel.deliveryDetailList.value.isNullOrEmpty()) {
                deliveryMainViewModel.deliveryDetailList.value = arrayListOf()
                deliveryMainViewModel.deliveryDetailList.value!!.add(itemDetailModel)
            }else{
                deliveryMainViewModel.deliveryDetailList.value = arrayListOf()
                deliveryMainViewModel.deliveryDetailList.value!!.add(itemDetailModel)
            }
            isValid = true
        }
        return isValid
    }

    override fun validateMultipleDelivery(): Boolean {
        if (deliveryMainViewModel.deliveryDetailList.value!!.size > 0)
            return true
        else
            ViewUtils.showToast(activity!!, R.string.empty_delivery, false)
        return false
    }

    inner class PlaceTextLisnter : TextWatcher {
        override fun afterTextChanged(p0: Editable?) {
        }

        override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
        }

        override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
            placesAutocompleteUtil.findAutocompletePredictions(s.toString(), deliveryMainViewModel.strCountryCode.value!!,
                    object : PlacesAutocompleteUtil.PlaceSuggestion {
                        override fun onPlaceReceived(mPlacesList: ArrayList<PlacesModel>?) {
                            prediction = mPlacesList!!
                            var adapter = PlaceListAdapter(activity!!, prediction)
                            mfragmentViewBinding.atvPlaces.setAdapter(adapter)
                            adapter.notifyDataSetChanged()
                            mfragmentViewBinding.atvPlaces.setOnItemClickListener(this@DeliveryListFragment)
                        }
                    })
        }
    }

    fun clearAddress() {
        mfragmentViewBinding.atvPlaces.removeTextChangedListener(textWatcher)
        mfragmentViewBinding.atvPlaces.setText("")
        mfragmentViewBinding.atvPlaces.addTextChangedListener(textWatcher)
    }

    override fun onItemClick(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        if (prediction.size > 0) try {
            val mPlace = prediction[position]
            val mLatLng = placesAutocompleteUtil.getPlaceName(mPlace.mPlaceId, object : PlacesAutocompleteUtil.onLoccationListner {
                override fun onLatLngRecived(placeLatLng: LatLng) {
                    if (placeLatLng.latitude != 0.0 && placeLatLng.longitude != 0.0) {
                        deliveryListViewModel.mDeliveryAddress.value = prediction[position].mFullAddress
                        mfragmentViewBinding.atvPlaces.removeTextChangedListener(textWatcher)
                        mfragmentViewBinding.atvPlaces.setText(prediction[position].mFullAddress.toString())
                        deliveryListViewModel.mLatitude.value = placeLatLng.latitude
                        deliveryListViewModel.mLongitude.value = placeLatLng.longitude
                        hideKeyboard()
                    }
                }
            })
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }


    override fun onMapReady(map: GoogleMap) {
        try {
            map!!.setMapStyle(MapStyleOptions.loadRawResourceStyle(activity, R.raw.style_json))
        } catch (e: Resources.NotFoundException) {
            e.printStackTrace()
        }
        this.mGoogleMap = map
        updateMapLocation(deliveryMainViewModel.pickupLatLong.value!!, true)
//        if (!deliveryMainViewModel.strCountryname.value.isNullOrEmpty())
//            getLatLng(deliveryMainViewModel.strCountryname.value!!)
    }

    private fun getLatLng(countryName: String) {
        val geoCoder = Geocoder(activity!!)
        var addressList: List<Address>? = listOf()
        try {
            addressList = geoCoder.getFromLocationName(countryName, 1)
        } catch (e: IOException) {
            e.printStackTrace()
        }
        if (!addressList.isNullOrEmpty()) {
            val address = addressList!![0]
            val latLng = LatLng(address.latitude, address.longitude)
            updateMapLocation(LatLng(latLng.latitude, latLng.longitude), true)
            //countryCode = LocationUtils.getCountryCode(activity!!, LatLng(latLng.latitude, latLng.longitude))
        }
    }

    fun updateMapLocation(location: LatLng, isAnimateMap: Boolean) {
        try {
            if (!isAnimateMap) {
                if (mGoogleMap != null) {
                    mGoogleMap?.moveCamera(CameraUpdateFactory
                            .newLatLngZoom(location, Constant.MapConfig.DEFAULT_ZOOM))
                }
            } else {
                if (mGoogleMap != null) {
                    mGoogleMap?.animateCamera(CameraUpdateFactory
                            .newLatLngZoom(location, Constant.MapConfig.DEFAULT_ZOOM))
                }
            }
        } catch (e: Exception) {

        }
    }

    private val mOnAdapterClickListener = object : ClickItemAdapterPostion {
        override fun clickPostion(postion: Int) {
            deliveryMainViewModel.deliveryDetailList.value!!.removeAt(postion)
            multiAddressAdpater.notifyDataSetChanged()
        }
    }

fun multiadpater(){
    multiAddressAdpater = MultiAddressAdapter(context!!, deliveryMainViewModel.deliveryDetailList.value!!)
    multiAddressAdpater.setOnClickListener(mOnAdapterClickListener)
    mfragmentViewBinding.rvDeliveryAddress.adapter = multiAddressAdpater
}

    fun captureImage() {
        val intent = CropImage.activity()
                .getIntent(activity!!)
        startActivityForResult(intent, CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE)
    }

}