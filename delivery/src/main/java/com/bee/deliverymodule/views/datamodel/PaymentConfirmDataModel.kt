package com.bee.deliverymodule.views.datamodel

data class PaymentConfirmDataModel(val error: List<Any?>? = listOf(),
                                   val message: String? = "",
                                   val statusCode: String? = "",
                                   val title: String? = ""
) {
    data class ResponseData(
            val message: String? = "",
            val url: String? = ""
    )

}