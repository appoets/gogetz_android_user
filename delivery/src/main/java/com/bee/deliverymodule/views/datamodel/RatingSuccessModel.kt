package com.bee.deliverymodule.views.datamodel


data class  RatingSuccessModel(
val error: List<Any?>? = listOf(),
val message: String? = "",
val responseData: List<Any?>? = listOf(),
val statusCode: String? = "",
val title: String? = "")