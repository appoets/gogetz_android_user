package com.bee.deliverymodule.views.deliveryHome

import androidx.fragment.app.Fragment
import androidx.lifecycle.MutableLiveData
import com.gox.basemodule.BaseApplication
import com.gox.basemodule.base.BaseViewModel
import com.gox.basemodule.data.PreferenceHelper
import com.gox.basemodule.data.PreferenceKey
import com.gox.basemodule.data.getValue
import com.gox.basemodule.repositary.ApiListener
import com.gox.basemodule.repositary.BaseModuleRepository
import com.bee.deliverymodule.views.datamodel.DeliveryCheckRequestModel
import com.bee.deliverymodule.views.datamodel.ItemDetailModel
import com.bee.deliverymodule.views.datamodel.RequestSchduleModel
import com.bee.deliverymodule.views.networkConfig.DeliverRespository
import com.google.android.gms.maps.model.LatLng

class DeliveryMainViewModel : BaseViewModel<DeliveryMainNavigator>() {
    var deliveryCheckRequestModel = MutableLiveData<DeliveryCheckRequestModel>()
    var errorResponse = MutableLiveData<String>()
    val appRepository = DeliverRespository.instance()
    val preferenceHelper = PreferenceHelper(BaseApplication.baseApplication)
    var showLoading = MutableLiveData<Boolean>()
    var strCountryname = MutableLiveData<String>()
    var strCountryCode = MutableLiveData<String>()
    var strAddress=MutableLiveData<String>()
    var pickupLatLong = MutableLiveData<LatLng>()
    var boxType=MutableLiveData<String>()
    var selectedVechileID=MutableLiveData<String>()
    var isSingleDelivery=MutableLiveData<Boolean>(true)
    var deliveryDetailList= MutableLiveData<ArrayList<ItemDetailModel>>(arrayListOf())
    var selectedBoxType=MutableLiveData<String>()
    var paymentMode=MutableLiveData<String>()
    var paymentHandleSide=MutableLiveData<Boolean>()
    var requestID=MutableLiveData<Int>()
    var scheduleModel = MutableLiveData<RequestSchduleModel>()
    var driverSpeed = MutableLiveData<Double>()
    var mainTitle = MutableLiveData<String>()



    fun setScheduleDateTime(reqScheduleRide: RequestSchduleModel) {
        scheduleModel.value = reqScheduleRide
    }

    fun getScheduleDateTimeData(): MutableLiveData<RequestSchduleModel> {
        return scheduleModel
    }


    fun changeFragment(fragment: Fragment) {
        navigator.loadFragment(fragment)
    }

    fun setToolbarTitle(title: String) {
        navigator.tootlBarTitle(title)
    }

    fun callCheckRequest() {
        getCompositeDisposable().addAll(appRepository.callCheckRequest(this, preferenceHelper.getValue(PreferenceKey.ACCESS_TOKEN, "").toString(), object : ApiListener {
            override fun onSuccess(successData: Any) {
                showLoading.postValue(false)
                deliveryCheckRequestModel.postValue(successData as DeliveryCheckRequestModel)
            }

            override fun onError(error: Throwable) {
                errorResponse.postValue(error.message)
                showLoading.postValue(false)
            }

        }))

    }
}