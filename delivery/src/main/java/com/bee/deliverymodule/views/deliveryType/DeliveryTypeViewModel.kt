package com.bee.deliverymodule.views.deliveryType

import com.gox.basemodule.base.BaseViewModel

class DeliveryTypeViewModel:BaseViewModel<DeliveryTypeNavigator>() {
    fun selectedDeliverytype(isSingle:Boolean){
        navigator.selectedDeliveryType(isSingle)
    }
}