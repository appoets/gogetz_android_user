package com.bee.deliverymodule.views.deliveryList

import androidx.lifecycle.MutableLiveData
import com.bee.deliverymodule.views.datamodel.PackageTypeModel
import com.bee.deliverymodule.views.networkConfig.DeliverRespository
import com.gox.basemodule.BaseApplication
import com.gox.basemodule.base.BaseViewModel
import com.gox.basemodule.data.PreferenceHelper
import com.gox.basemodule.data.PreferenceKey
import com.gox.basemodule.data.getValue
import com.gox.basemodule.repositary.ApiListener

class DeliveryListViewModel : BaseViewModel<DeliveryListNavigator>() {

    var mReciverName = MutableLiveData<String>()
    var mReciverPhone = MutableLiveData<String>()
    var mDeliveryInstruction = MutableLiveData<String>()
    var packageType = MutableLiveData<String>("1")
    var packageDetail = MutableLiveData<String>()
    var mLatitude = MutableLiveData<Double>()
    var mLongitude = MutableLiveData<Double>()
    var mDeliveryAddress = MutableLiveData<String>()
    var weight = MutableLiveData<String>()
    var height = MutableLiveData<String>().apply{setValue("")}
    var length = MutableLiveData<String>().apply{setValue("")}
    var breadth = MutableLiveData<String>().apply{setValue("")}
    var imagePath=MutableLiveData<String>()
    var isFragile = MutableLiveData<Boolean>().apply { setValue(false) }
    var isSingleDelivery = MutableLiveData<Boolean>(true)
    var packageTypeDetails = MutableLiveData<PackageTypeModel>()
    var packageTypeID = MutableLiveData<Int>().apply { setValue(0) }
    val preferenceHelper = PreferenceHelper(BaseApplication.baseApplication)
    val appRepository = DeliverRespository.instance()

    fun gotoItemDetailPage() {
        navigator.gotoItemDetailPage()
    }

    fun resetDetail() {
        navigator.reset()
    }

    fun submitDetail() {
        if(isSingleDelivery.value!!) {
            if (navigator.validate()) {
                navigator.submit()
            }
        }else{
            if(navigator.validateMultipleDelivery())
              navigator.submit()
        }
    }

    fun getPackageType() {
        getCompositeDisposable().addAll(appRepository.getPackageType(this, preferenceHelper.getValue(PreferenceKey.ACCESS_TOKEN, "").toString(), object : ApiListener {
            override fun onSuccess(successData: Any) {
                packageTypeDetails.postValue(successData as PackageTypeModel)
            }
            override fun onError(error: Throwable) {
//                    loadingProgress.postValue(false)
            }

        }))

    }
}
