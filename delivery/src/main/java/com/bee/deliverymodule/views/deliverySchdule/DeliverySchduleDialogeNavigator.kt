package com.bee.deliverymodule.views.deliverySchdule

interface  DeliverySchduleDialogeNavigator {
    fun pickDate()
    fun pickTime()
    fun scheduleRequest()

}