package com.bee.deliverymodule.views.datamodel

data class CancelRequestModel(
    var error: List<Any?>? = listOf(),
    var message: String? = "",
    var responseData: List<Any?>? = listOf(),
    var statusCode: String? = "",
    var title: String? = ""
)