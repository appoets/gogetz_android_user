package com.bee.deliverymodule.views.datamodel

data class PackageTypeModel(
    var error: List<Any?>? = listOf(),
    var message: String? = "",
    var responseData: List<ResponseData> = listOf(),
    var statusCode: String? = "",
    var title: String? = ""
) {
    data class ResponseData(
        var id: Int? = 0,
        var package_name: String? = ""
    )
}