package com.bee.deliverymodule.views.datamodel

import okhttp3.MultipartBody
import java.io.Serializable

data class  ItemDetailModel(var reciverName:String="",
                            var reciverPhoneNumber:String="",
                            var deliveryInstruction:String="",
                            var packageType:Int= 1,
                            var packageDetail:String="",
                            var deliveryAddress:String="",
                            var deliveryLatitude:Double=0.0,
                            var deliveryLongitude:Double=0.0,
                            var length:String="",
                            var breadth:String="",
                            var height:String="",
                            var weight:String="0",
                            var is_fragile:String="0",
                            var picture:String=""):Serializable