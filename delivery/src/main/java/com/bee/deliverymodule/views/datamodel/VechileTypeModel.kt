package com.bee.deliverymodule.views.datamodel

data class VechileTypeModel(
        var error: List<Any?>? = listOf(),
        var message: String? = "",
        var responseData: ResponseData? = ResponseData(),
        var statusCode: String? = "",
        var title: String? = ""
) {
    data class ResponseData(
            var promocodes: List<Any?>? = ArrayList(),
            var providers: List<Provider?>? = ArrayList(),
            var services: List<Service> = ArrayList()
    ) {
        data class Provider(
                var country_code: String? = "",
                var current_ride_vehicle: Any? = Any(),
                var current_store: Any? = Any(),
                var distance: Double? = 0.0,
                var email: String? = "",
                var first_name: String? = "",
                var gender: String? = "",
                var id: Int? = 0,
                var last_name: String? = "",
                var latitude: Double? = 0.0,
                var longitude: Double? = 0.0,
                var mobile: String? = "",
                var service: Service? = Service(),
                var service_id: Int? = 0
        ) {
            data class Service(
                    var admin_service: String? = "",
                    var base_fare: Double? = 0.0,
                    var category_id: Int? = 0,
                    var company_id: Int? = 0,
                    var delivery_vehicle: DeliveryVehicle? = DeliveryVehicle(),
                    var delivery_vehicle_id: Int? = 0,
                    var id: Int? = 0,
                    var per_miles: Double? = 0.0,
                    var per_mins: Double? = 0.0,
                    var provider_id: Int? = 0,
                    var provider_vehicle_id: Int? = 0,
                    var ride_delivery_id: Any? = Any(),
                    var service_id: Any? = Any(),
                    var status: String? = "",
                    var sub_category_id: Any? = Any()
            ) {
                data class DeliveryVehicle(
                        var capacity: Int? = 0,
                        var delivery_types_id: Int? = 0,
                        var id: Int? = 0,
                        var status: Int? = 0,
                        var vehicle_image: String? = "",
                        var vehicle_marker: String? = "",
                        var vehicle_name: String? = "",
                        var vehicle_type: String? = ""
                )
            }
        }

        data class Service(
                var capacity: Int? = 0,
                var delivery_types_id: Int? = 0,
                var estimated_time: String? = "",
                var id: Int? = 0,
                var price_details: PriceDetails? = PriceDetails(),
                var status: Int? = 0,
                var vehicle_image: String? = "",
                var vehicle_marker: String? = "",
                var vehicle_name: String? = "",
                var vehicle_type: String? = ""
        ) {
            data class PriceDetails(
                    var calculator: String? = "",
                    var delivery_vehicle_id: Int? = 0,
                    var distance: Int? = 0,
                    var fixed: Double? = 0.0,
                    var price: Double? = 0.0,
                    var weight: Double? = 0.0
            )
        }
    }
}