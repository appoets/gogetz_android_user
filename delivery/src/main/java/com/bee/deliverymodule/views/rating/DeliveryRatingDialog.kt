package com.bee.deliverymodule.views.rating

import android.content.Intent
import android.view.View
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.gox.basemodule.base.BaseDialogFragment
import com.bee.delivermodule.R
import com.bee.delivermodule.databinding.DialogRatingBinding
import com.bee.deliverymodule.views.deliveryHome.DeliveryMainViewModel
import com.google.maps.android.clustering.algo.NonHierarchicalDistanceBasedAlgorithm
import com.gox.basemodule.utils.ViewUtils

class DeliveryRatingDialog : BaseDialogFragment<DialogRatingBinding>() {
    private lateinit var dialogRatingBinding: DialogRatingBinding
    private lateinit var delvieryRatingViewModel: DeliveryRatingViewModel
    private lateinit var deliveryMainViewModel: DeliveryMainViewModel
    private  var requestID:Int?=0
    public var isShown:Boolean = false

    override fun getLayoutId(): Int {
        return R.layout.dialog_rating
    }

    override fun initView(mRootView: View?, mViewDataBinding: ViewDataBinding?) {
        dialogRatingBinding = mViewDataBinding as DialogRatingBinding
        delvieryRatingViewModel = ViewModelProviders.of(this).get(DeliveryRatingViewModel::class.java)
        dialogRatingBinding.ratingViewModel = delvieryRatingViewModel

        //GetArguments
        getArgumentValues()

        //GetObserVerValues
        getObserVerValues()

        dialogRatingBinding.rvDelivery.setOnRatingBarChangeListener { ratingBar, rating, fromUser ->  delvieryRatingViewModel.rating.value = dialogRatingBinding.rvDelivery.rating.toInt()}
    }

    fun getArgumentValues(){
        requestID=if(arguments!=null && arguments!!.containsKey("requestID"))  arguments!!.getInt("requestID") else 0
        if(requestID!=0){
            delvieryRatingViewModel.reqID.value=requestID!!.toInt()
        }
    }

    fun getObserVerValues() {
        delvieryRatingViewModel.loaderPrgress.observe(this, Observer {
            if (it) {
                showLoading()
            } else {
                hideLoading()
            }
        })

        delvieryRatingViewModel.ratingSuccess.observe(this, Observer {
            ViewUtils.showToast(activity!!,it.message,true)
            val intent = Intent(activity,Class.forName("com.gox.app.ui.dashboard.UserDashboardActivity"))
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            intent.putExtra("EXIT", true)
            startActivity(intent)
            dialog!!.dismiss()
//            activity!!.finish()
        })
    }

    override fun show(manager: FragmentManager, tag: String?) {
        try {
            if (isShown == false) {
                this.isShown = true
                super.show(manager, tag)
            }
        } catch (e: IllegalStateException) {
            e.printStackTrace()
        }
    }

    override fun show(transaction: FragmentTransaction, tag: String?): Int {
        if (isShown == false) {
            this.isShown = true
            return super.show(transaction, tag)
        }
        return -1
    }

//    override fun show(manager: FragmentManager?, tag: String?) {
//        if(!isShown){
//            isShown = true
//        }
//        super.show(manager, tag)
//    }
}