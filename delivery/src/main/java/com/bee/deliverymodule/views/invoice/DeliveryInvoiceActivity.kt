package com.bee.deliverymodule.views.invoice

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.gox.basemodule.base.BaseActivity
import com.gox.basemodule.common.payment.utils.CommonMethods
import com.gox.basemodule.utils.AppUtils
import com.gox.basemodule.utils.ViewUtils
import com.bee.delivermodule.R
import com.bee.delivermodule.databinding.ActivityInvoiceBinding
import com.bee.deliverymodule.adapter.DropLocationAdpater
import com.bee.deliverymodule.views.datamodel.DeliveryCheckRequestModel
import com.bee.deliverymodule.views.rating.DeliveryRatingDialog
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.gox.basemodule.BaseApplication
import com.gox.basemodule.common.payment.paystack_activity.PaystackActivityForWallet
import com.gox.basemodule.data.Constant
import com.gox.basemodule.data.PreferenceHelper
import com.gox.basemodule.data.PreferenceKey
import com.gox.basemodule.data.getValue
import java.util.*
import kotlin.collections.ArrayList


class DeliveryInvoiceActivity : BaseActivity<ActivityInvoiceBinding>(), DeliveryInvoiceNavigator {
    private lateinit var activityInvoiceBinding: ActivityInvoiceBinding
    private lateinit var invoiceViewModel: DeliveryInvoiceViewModel
    private var strDelivery: String? = ""
    private var delivery: DeliveryCheckRequestModel.ResponseData.Data.Delivery? = null
    private lateinit var dropLocationAdpater: DropLocationAdpater
    private var checkRequestTimer: Timer? = null
    private  var status:String?=""
    val deliveryRatingDialog = DeliveryRatingDialog()
    private val PICK_PAYSTACK = 1021
    val preferenceHelper = PreferenceHelper(BaseApplication.baseApplication)
    private var amount : Double? = 0.0
    private var paymentRef : String? =null
    private var requestId : Int? =null
    override fun getLayoutId(): Int {
        return R.layout.activity_invoice
    }

    override fun initView(mViewDataBinding: ViewDataBinding?) {

        activityInvoiceBinding = mViewDataBinding as ActivityInvoiceBinding
        invoiceViewModel = ViewModelProviders.of(this).get(DeliveryInvoiceViewModel::class.java)
        invoiceViewModel.navigator = this
        activityInvoiceBinding.invoiceViewModel = invoiceViewModel
        activityInvoiceBinding.invoiceActivity = this
        checkRequestTimer = Timer()
        //activityInvoiceBinding.invoiceActivity=this
        //GetIntentValues
        getIntentValues()
        //GetObserverValues
        getObservervalues()

        invoiceViewModel.loadingProgress.value=true
        //callCheckRequest
        checkRequestTimer!!.schedule(object : TimerTask() {
            override fun run() {
                invoiceViewModel.callCheckRequest()
            }
        }, 0, 5000)

    }

    fun getIntentValues() {
    }

    fun getObservervalues() {
        invoiceViewModel.loadingProgress.observe(this, Observer {
            baseLiveDataLoading.value = it
        })

        invoiceViewModel.confirmPaymentResponse.observe(this, Observer {
            if (it != null && it.statusCode.equals("200")) {
                activityInvoiceBinding.tvProceed.setText("DONE")
//               if(!deliveryRatingDialog.isShown){
//                val bundle = Bundle()
//                bundle.putInt("requestID", invoiceViewModel.deliveryCheckRequestModel.value!!.responseData!!.data!!.get(0)!!.id!!)
//                deliveryRatingDialog.arguments = bundle
//                   deliveryRatingDialog.isCancelable = false
//                deliveryRatingDialog.show(supportFragmentManager, "deliveryRating")
//               }
            }
        })

        invoiceViewModel.deliveryCheckRequestModel.observe(this, Observer {
            if (it != null && it.statusCode.equals("200")) {
                if(it.responseData!!.data!!.get(0)!!.paid!!.equals(0)){
//                if (status.equals(it.responseData!!.data!!.get(0)!!.status!!.toUpperCase())) {
//                    status=it.responseData!!.data!!.get(0)!!.status!!.toUpperCase()
                    if (!it.responseData!!.data!!.get(0)!!.s_address.isNullOrEmpty())
                        activityInvoiceBinding.tvPickupLocation.setText(it.responseData!!.data!!.get(0)!!.s_address)
                    if (it.responseData!!.data!!.get(0)!!.deliveries!!.size > 0) {
                        val deliveryData: ArrayList<DeliveryCheckRequestModel.ResponseData.Data.Delivery> = ArrayList()
                        deliveryData.addAll(it.responseData!!.data!!.get(0)!!.deliveries!!)
                        dropLocationAdpater = DropLocationAdpater(invoiceViewModel, this, deliveryData!!)
                        activityInvoiceBinding.rvDropLocation.adapter = dropLocationAdpater
                        dropLocationAdpater.notifyDataSetChanged()
                    }

//                if (!it.responseData!!.data!!.get(0)!!.delivery!!.get(0)!!.d_address.isNullOrEmpty())
//                    activityInvoiceBinding.tvDropLocation.setText(it.responseData!!.data!!.get(0)!!.d_address)
                    try {
                        activityInvoiceBinding.tvBaseFare.setText(AppUtils.getNumberFormat()!!.format(it.responseData!!.data!!.get(0)!!.deliveries.get(0)!!.payment!!.fixed))
                        activityInvoiceBinding.tvTitleTaxFare.setText("Tax @ " + it.responseData!!.data!!.get(0)!!.deliveries.get(0)!!.payment!!.tax.toString() + "%")
                        activityInvoiceBinding.tvTaxFare.setText(AppUtils.getNumberFormat()!!.format(it.responseData!!.data!!.get(0)!!.deliveries.get(0)!!.payment!!.tax))
                        activityInvoiceBinding.tvTotalFare.setText(AppUtils.getNumberFormat()!!.format(it.responseData!!.data!!.get(0)!!.deliveries.get(0)!!.payment!!.total))
                        activityInvoiceBinding.tvDiscountFare.setText(AppUtils.getNumberFormat()!!.format(it.responseData!!.data!!.get(0)!!.deliveries.get(0)!!.payment!!.discount))
                        activityInvoiceBinding.tvDistanceFare.setText(it.responseData!!.data!!.get(0)!!.deliveries.get(0)!!.payment!!.distance.toString() + " kms")
//                    activityInvoiceBinding.tvDurationFare.setText(it.responseData!!.data!!.get(0)!!.payment!!.tim.toString()+" kms")
                        activityInvoiceBinding.tvSubTotalFare.setText(AppUtils.getNumberFormat()!!.format(it.responseData!!.data!!.get(0)!!.deliveries.get(0)!!.payment!!.total))
//                    val deliveryTime = CommonMethods.getLocalTime(it.responseData!!.data!!.get(0)!!.finished_at!!, "Req_Date_Month_FullYear")
//                    activityInvoiceBinding.tvDeliveryDate.setText(deliveryTime)
                        if (it.responseData!!.data!!.get(0)!!.deliveries.get(0)!!.payment!!.promocode_id == null) {
                            activityInvoiceBinding.tvDiscountApplied.visibility = View.GONE
                        } else {
                            activityInvoiceBinding.tvDiscountApplied.visibility = View.VISIBLE
                            activityInvoiceBinding.tvDiscountApplied.setText(it.responseData!!.data!!.get(0)!!.deliveries.get(0)!!.payment!!.promocode_id.toString())
                        }

                        val paymentMode = it.responseData!!.data!!.get(0)!!.deliveries.get(0)!!.payment!!.payment_mode!!.toUpperCase()

                        if (!paymentMode.isNullOrEmpty() && paymentMode.equals("CASH")) {
                            activityInvoiceBinding.tvProceed.setText("DONE")
                        } else if (!paymentMode.equals("CASH") && it.responseData!!.data!!.get(0)!!.status == "ARRIVED") {
                            activityInvoiceBinding.tvProceed.setText("PAY")
                        }
                    } catch (ce: Exception){
                        ce.printStackTrace()
                    }
                }else {
                    if(invoiceViewModel.deliveryCheckRequestModel.value!!.responseData!!.data!!.get(0)!!.payment_by.equals("SENDER",true)){
                        checkRequestTimer!!.cancel()
                        this.finish()
                    }else {if(!deliveryRatingDialog.isShown){
                        val bundle = Bundle()
                        bundle.putInt("requestID", invoiceViewModel.deliveryCheckRequestModel.value!!.responseData!!.data!!.get(0)!!.id!!)
                        deliveryRatingDialog.arguments = bundle
                        deliveryRatingDialog.isCancelable = false
                        deliveryRatingDialog.show(supportFragmentManager, "deliveryRating")
                    } }
                }
            }
        })
    }

    override fun confirmPayment() {
        if (invoiceViewModel.deliveryCheckRequestModel.value!!.responseData != null && !invoiceViewModel.deliveryCheckRequestModel.value!!.responseData!!.data.isNullOrEmpty()) {
            val paymentMode = invoiceViewModel.deliveryCheckRequestModel.value!!.responseData!!.data!!.get(0)!!.deliveries.get(0)!!.payment!!.payment_mode!!.toUpperCase()
            invoiceViewModel.isPaid.value = invoiceViewModel.deliveryCheckRequestModel.value!!.responseData!!.data!!.get(0)!!.paid
            when (paymentMode) {
                "CASH" -> {
                    if (invoiceViewModel.isPaid.value == 1) {
                        val deliveryRatingDialog = DeliveryRatingDialog()
                        val bundle = Bundle()
                        bundle.putInt("requestID", invoiceViewModel.deliveryCheckRequestModel.value!!.responseData!!.data!!.get(0)!!.id!!)
                        deliveryRatingDialog.arguments = bundle
                        deliveryRatingDialog.isCancelable = false
                        deliveryRatingDialog.show(supportFragmentManager, "deliveryRating")
                    } else {
                        ViewUtils.showToast(this, "Wating for Payment confirmation", false)
                    }
                }
                else -> {
                    if(activityInvoiceBinding.tvProceed.text == "PAY") {
                        requestId = invoiceViewModel.deliveryCheckRequestModel.value!!.responseData!!.data!!.get(0)!!.id
                        amount = invoiceViewModel.deliveryCheckRequestModel.value!!.responseData!!.data!!.get(0)!!.deliveries.get(0)!!.payment!!.total
                        val intent = Intent(this, PaystackActivityForWallet::class.java)
                        intent.putExtra("from", "DELIVERY")
                        intent.putExtra("amount", amount!!)
                        intent.putExtra("email", preferenceHelper.getValue(PreferenceKey.EMAIL, "").toString())
                        startActivityForResult(intent, PICK_PAYSTACK)
                    } else {
                        val deliveryRatingDialog = DeliveryRatingDialog()
                        val bundle = Bundle()
                        bundle.putInt("requestID", invoiceViewModel.deliveryCheckRequestModel.value!!.responseData!!.data!!.get(0)!!.id!!)
                        deliveryRatingDialog.arguments = bundle
                        deliveryRatingDialog.isCancelable = false
                        deliveryRatingDialog.show(supportFragmentManager, "deliveryRating")
                    }
                }
            }
        }


    }

    fun showSchdeuleDialog() {
        val mScheduleFragment = DeliveryRatingDialog()
        mScheduleFragment.show(supportFragmentManager, mScheduleFragment.tag)

    }

    override fun onResume() {
        super.onResume()
    }

    override fun onDestroy() {
        super.onDestroy()
        checkRequestTimer!!.cancel()
    }

    @SuppressLint("MissingSuperCall")
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (resultCode != Activity.RESULT_CANCELED) {
            when (requestCode) {
                PICK_PAYSTACK -> {
                    if(data !=null && data?.extras?.get("sucess").toString().equals("1")) {
                        val reqTips = ReqTips()
                        reqTips.requestId = requestId
                        reqTips.cardId = ""
                        reqTips.tipsAmount = data?.extras?.get("amount").toString()
                        reqTips.reference = data?.extras?.get("reference").toString()
                        invoiceViewModel.callPayment(reqTips)
                    }
                }
            }
        }
    }
}

