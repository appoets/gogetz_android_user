package com.bee.deliverymodule.views.deliveryType

interface DeliveryTypeNavigator {
    fun selectedDeliveryType(isSignle:Boolean)
}