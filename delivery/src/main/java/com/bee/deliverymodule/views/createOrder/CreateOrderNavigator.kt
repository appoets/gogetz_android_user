package com.bee.deliverymodule.views.createOrder

interface  CreateOrderNavigator{
  fun changeOrderCrateFrag(tagName:String)
  fun hideStepProgress(isNeedToHide:Boolean)
  fun changeToolbarBackground(isTransparent:Boolean)
  fun changeStepProgress(pageName:String)
}