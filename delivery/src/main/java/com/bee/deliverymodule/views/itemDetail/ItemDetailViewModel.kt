package com.bee.deliverymodule.views.itemDetail

import androidx.lifecycle.MutableLiveData
import com.bee.deliverymodule.views.datamodel.DeliveryCheckRequestModel
import com.bee.deliverymodule.views.datamodel.PackageTypeModel
import com.bee.deliverymodule.views.networkConfig.DeliverRespository
import com.gox.basemodule.BaseApplication
import com.gox.basemodule.base.BaseViewModel
import com.gox.basemodule.data.PreferenceHelper
import com.gox.basemodule.data.PreferenceKey
import com.gox.basemodule.data.getValue
import com.gox.basemodule.repositary.ApiListener

class ItemDetailViewModel : BaseViewModel<ItemDetailNavigator>() {
    var mlDeliveryAddress = MutableLiveData<String>()
    var mLatitude=MutableLiveData<Double>()
    var mLongitude=MutableLiveData<Double>()
    var mlReciverName = MutableLiveData<String>()
    var mlReciverPhone = MutableLiveData<String>()
    var mlPackageType = MutableLiveData<String>("1")
    var mlPackDetail = MutableLiveData<String>()
    var mlDeliveryInstruction=MutableLiveData<String>()
    var mWeight=MutableLiveData<String>()
    var mBreadth=MutableLiveData<String>().apply{setValue("")}
    var mHeight=MutableLiveData<String>().apply{setValue("")}
    var mLength=MutableLiveData<String>().apply{setValue("")}
    var mIsFragile = MutableLiveData<Boolean>().apply { setValue(false) }
    var packageTypeDetails = MutableLiveData<PackageTypeModel>()
    var packageTypeID = MutableLiveData<Int>().apply { setValue(0) }
    val preferenceHelper = PreferenceHelper(BaseApplication.baseApplication)
    val appRepository = DeliverRespository.instance()
    var imagePath=MutableLiveData<String>()


    fun getPackageType() {
            getCompositeDisposable().addAll(appRepository.getPackageType(this, preferenceHelper.getValue(PreferenceKey.ACCESS_TOKEN, "").toString(), object : ApiListener {
                override fun onSuccess(successData: Any) {
                    packageTypeDetails.postValue(successData as PackageTypeModel)
                }
                override fun onError(error: Throwable) {
//                    loadingProgress.postValue(false)
                }

            }))

        }
}