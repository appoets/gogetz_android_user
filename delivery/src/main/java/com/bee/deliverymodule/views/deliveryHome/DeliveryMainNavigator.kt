package com.bee.deliverymodule.views.deliveryHome

import androidx.fragment.app.Fragment

interface DeliveryMainNavigator {
    fun loadFragment(fragment:Fragment)
    fun tootlBarTitle(title:String)
}