package com.bee.deliverymodule.views.itemDetail

interface  ItemDetailNavigator{
    fun onReset()
    fun onSubmit()
    fun onRepeat()
}