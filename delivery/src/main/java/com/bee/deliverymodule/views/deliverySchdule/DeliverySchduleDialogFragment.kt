package com.bee.deliverymodule.views.deliverySchdule

import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.content.DialogInterface
import android.util.Log
import android.view.View
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.bee.delivermodule.R
import com.bee.delivermodule.databinding.DeliveryScheduleFragmentBinding
import com.bee.deliverymodule.views.datamodel.RequestSchduleModel
import com.bee.deliverymodule.views.deliveryHome.DeliveryMainViewModel
import com.gox.basemodule.base.BaseDialogFragment
import com.gox.basemodule.utils.ViewUtils
import java.lang.Exception
import java.util.*


class DeliverySchduleDialogFragment : BaseDialogFragment<DeliveryScheduleFragmentBinding>(), DeliverySchduleDialogeNavigator {

    private lateinit var deliveryScheduleFragmentBinding: DeliveryScheduleFragmentBinding
    private lateinit var deliverySchduleDialogeViewModel: DeliverySchduleDialogeViewModel
    private lateinit var deliveryMainViewModel: DeliveryMainViewModel

    private var mSceduleDate: String? = null
    private var mSceduletime: String? = null

    override fun getLayoutId(): Int {
        return R.layout.delivery_schedule_fragment
    }

    override fun initView(mRootView: View?, mViewDataBinding: ViewDataBinding?) {
        deliveryScheduleFragmentBinding = mViewDataBinding as DeliveryScheduleFragmentBinding
        deliverySchduleDialogeViewModel = ViewModelProviders.of(activity!!).get(DeliverySchduleDialogeViewModel::class.java)
        deliveryMainViewModel = ViewModelProviders.of(activity!!).get(DeliveryMainViewModel::class.java)
        deliverySchduleDialogeViewModel.navigator = this
        deliveryScheduleFragmentBinding.viewModel = deliverySchduleDialogeViewModel
    }

    override fun pickDate() {
        val c = Calendar.getInstance()
        val mYear = c.get(Calendar.YEAR)
        val mMonth = c.get(Calendar.MONTH)
        val mDay = c.get(Calendar.DAY_OF_MONTH)
        val now = System.currentTimeMillis() - 1000
        val maxDate = System.currentTimeMillis() + (1000 * 60 * 60 * 24 * 3)
        val datePickerDialog = DatePickerDialog(activity!!, R.style.TransportCalenderThemeDialog,
                DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                    view.minDate = System.currentTimeMillis() - 1000
                    view.maxDate = maxDate - 1000
                    mSceduleDate = dayOfMonth.toString() + "-" + (monthOfYear + 1) + "-" + year
                    deliveryScheduleFragmentBinding.selectedDate.text = mSceduleDate
                }, mYear, mMonth, mDay)
        datePickerDialog.datePicker.minDate = now
        datePickerDialog.datePicker.maxDate = maxDate
        datePickerDialog.show()
    }

    override fun pickTime() {
        val c = Calendar.getInstance()
        val mHour = c.get(Calendar.HOUR_OF_DAY)
        val mMinute = c.get(Calendar.MINUTE)
        val mSeconds = c.get(Calendar.SECOND)

        // Launch Time Picker Dialog
        val timePickerDialog = TimePickerDialog(activity!!, R.style.TransportCalenderThemeDialog,
                TimePickerDialog.OnTimeSetListener { view, hourOfDay, minute ->
                    val c = Calendar.getInstance()
                    c.set(Calendar.HOUR_OF_DAY, hourOfDay)
                    c.set(Calendar.MINUTE, minute)
                    c.add(Calendar.MINUTE, -20)
                    val hr = c.get(Calendar.HOUR_OF_DAY)
                    val min = c.get(Calendar.MINUTE)
                    mSceduletime = "$hr:$min"
                    val mOriginalteim = "$hourOfDay:$minute"
                    Log.e("SchduledTime", "------" + mSceduletime)
                    deliveryScheduleFragmentBinding.selectedTime.text = mOriginalteim

                }, mHour, mMinute, true)
        timePickerDialog.show()
    }


    override fun scheduleRequest() {

        val mScheduleRide = RequestSchduleModel()
        if(!mSceduletime.isNullOrEmpty() && !mSceduleDate.isNullOrEmpty() ){
            mScheduleRide.scheduleDate = mSceduleDate
            mScheduleRide.scheduleTime = mSceduletime
            deliveryMainViewModel.setScheduleDateTime(mScheduleRide)
            dismiss()
        }else{
            ViewUtils.showToast(activity!!,"Please select date and time",false)
        }

    }


}