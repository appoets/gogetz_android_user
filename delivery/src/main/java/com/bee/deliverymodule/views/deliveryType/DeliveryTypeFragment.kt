package com.bee.deliverymodule.views.deliveryType

import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.gox.basemodule.base.BaseActivity
import com.gox.basemodule.base.BaseFragment
import com.bee.delivermodule.R
import com.bee.delivermodule.databinding.FragmentDeliverTypeBinding
import com.bee.deliverymodule.boxTypes.BoxTypeFragment
import com.bee.deliverymodule.views.createOrder.CreateOrderMainFragment
import com.bee.deliverymodule.views.deliveryHome.DeliveryMainActivity
import com.bee.deliverymodule.views.deliveryHome.DeliveryMainViewModel
import com.gox.basemodule.BaseApplication
import com.gox.basemodule.data.PreferenceHelper
import com.gox.basemodule.data.PreferenceKey
import com.gox.basemodule.data.getValue
import com.gox.basemodule.utils.ViewCallBack
import com.gox.basemodule.utils.ViewUtils


class DeliveryTypeFragment : BaseFragment<FragmentDeliverTypeBinding>(), DeliveryTypeNavigator {
    private lateinit var fragmentDeliverTypeBinding: FragmentDeliverTypeBinding
    private lateinit var deliveryTypeViewModel: DeliveryTypeViewModel
    private lateinit var deliveryMainViewModel: DeliveryMainViewModel
    private val preference = PreferenceHelper(BaseApplication.baseApplication)

    override fun getLayoutId(): Int {
        return R.layout.fragment_deliver_type
    }

    override fun initView(mRootView: View?, mViewDataBinding: ViewDataBinding?) {
        fragmentDeliverTypeBinding = mViewDataBinding as FragmentDeliverTypeBinding
        deliveryTypeViewModel = ViewModelProviders.of(this).get(DeliveryTypeViewModel::class.java)
        deliveryMainViewModel = ViewModelProviders.of(activity!!).get(DeliveryMainViewModel::class.java)
        deliveryTypeViewModel.navigator=this
        deliveryMainViewModel.mainTitle.postValue("Choose your delivery type")
        fragmentDeliverTypeBinding.deliveryFragment=this
    }

    override fun selectedDeliveryType(isSignle: Boolean) {
        if(isSignle)
            fragmentDeliverTypeBinding.rltype.setBackground(resources.getDrawable(R.drawable.bg_circle_appcolor))
        else
            fragmentDeliverTypeBinding.rlmultitype.setBackground(resources.getDrawable(R.drawable.bg_circle_appcolor))
        if(preference.getValue(PreferenceKey.ACCESS_TOKEN, "").toString().isEmpty()) {
            ViewUtils.showAlert(activity!!, R.string.gust_messages,object : ViewCallBack.Alert{
                override fun onPositiveButtonClick(dialog: DialogInterface) {
                    val intent = Intent(activity,Class.forName("com.gox.app.ui.signup.SignupActivity"))
                    intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                    intent.putExtra("EXIT", true)
                    startActivity(intent)
                    dialog.dismiss()
                }
                override fun onNegativeButtonClick(dialog: DialogInterface) {

                }
            })
        } else {
                val bundle = Bundle()
                bundle.putBoolean("isSignle", isSignle)
                Log.e("isSignle","----"+isSignle)
                deliveryMainViewModel.isSingleDelivery.value=isSignle
                val boxTypeFragment = BoxTypeFragment()
                deliveryMainViewModel.changeFragment(boxTypeFragment)
        }
    }

}