package com.bee.deliverymodule.views.estimatePackage

interface EstimatePackageNavigator {
   fun submitDetail()
   fun editSchdule()
   fun deleteSchdule()
}