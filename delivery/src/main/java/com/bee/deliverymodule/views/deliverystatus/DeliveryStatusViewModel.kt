package com.bee.deliverymodule.views.deliverystatus

import androidx.lifecycle.MutableLiveData
import com.gox.basemodule.BaseApplication
import com.gox.basemodule.base.BaseViewModel
import com.gox.basemodule.data.PreferenceHelper
import com.gox.basemodule.data.PreferenceKey
import com.gox.basemodule.data.getValue
import com.gox.basemodule.repositary.ApiListener
import com.bee.deliverymodule.views.datamodel.CancelRequest
import com.bee.deliverymodule.views.datamodel.CancelRequestModel
import com.bee.deliverymodule.views.datamodel.DeliveryCheckRequestModel
import com.bee.deliverymodule.views.networkConfig.DeliverRespository
import com.google.android.gms.maps.model.LatLng

class DeliveryStatusViewModel : BaseViewModel<DeliveryStatusNavigator>() {

    val requestID = MutableLiveData<String>()
    val cancelRequestDataModel = MutableLiveData<CancelRequestModel>()
    val appRepository = DeliverRespository.instance()
    val preferenceHelper = PreferenceHelper(BaseApplication.baseApplication)
    val loadinProgress = MutableLiveData<Boolean>()
    var driverSpeed = MutableLiveData<Double>()
    var tempSrc = MutableLiveData<LatLng>()
    var tempDest = MutableLiveData<LatLng>()
    var polyLineSrc = MutableLiveData<LatLng>()
    var polyLineDest = MutableLiveData<LatLng>()
    var latitude = MutableLiveData<Double>()
    var longitude = MutableLiveData<Double>()
    var currentStatus = MutableLiveData<String>()


    fun call() {

    }

    fun message() {

    }

    fun cancelRequest() {
        loadinProgress.value = true
        val params = HashMap<String, Any>()
        params.put("id", requestID.value.toString())
        getCompositeDisposable().addAll(appRepository.cancelRequest(this, preferenceHelper.getValue(PreferenceKey.ACCESS_TOKEN, "").toString(), params, object : ApiListener {
            override fun onSuccess(successData: Any) {
                loadinProgress.postValue(false)
                cancelRequestDataModel.postValue(successData as CancelRequestModel)
            }

            override fun onError(error: Throwable) {
                loadinProgress.postValue(false)
            }

        }))
    }
}