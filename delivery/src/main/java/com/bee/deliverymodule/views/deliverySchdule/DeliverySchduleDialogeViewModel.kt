package com.bee.deliverymodule.views.deliverySchdule

import com.gox.basemodule.base.BaseViewModel

class DeliverySchduleDialogeViewModel : BaseViewModel<DeliverySchduleDialogeNavigator>() {
    fun pickDate() = navigator.pickDate()

    fun pickTime() = navigator.pickTime()

    fun scheduleRequest() = navigator.scheduleRequest()


}