package com.bee.deliverymodule.views.createOrder

import android.text.TextUtils
import android.view.View
import androidx.core.content.ContextCompat
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.gox.basemodule.base.BaseFragment
import com.bee.delivermodule.R
import com.bee.delivermodule.databinding.FragmentOrderMainpageBinding
import com.bee.delivermodule.databinding.FragmentOrderStatusBinding
import com.bee.deliverymodule.views.deliveryHome.DeliveryMainViewModel
import com.bee.deliverymodule.views.deliveryList.DeliveryListFragment
import com.bee.deliverymodule.views.estimatePackage.EstimatePackageFragment
import com.bee.deliverymodule.views.vechiletype.FragmentAddVechile
import kotlinx.android.synthetic.main.fragment_order_mainpage.*
import kotlinx.android.synthetic.main.row_add_vechile.*
import kotlinx.android.synthetic.main.row_add_vechile.ivVechile

class CreateOrderMainFragment : BaseFragment<FragmentOrderMainpageBinding>(), CreateOrderNavigator {
    private lateinit var fragmentOrderMainpageBinding: FragmentOrderMainpageBinding
    private lateinit var createOrderViewModel: CreateOrderViewModel
    private lateinit var deliverymainViewModel: DeliveryMainViewModel
    private var isFragmentInBackstack: Boolean = false
    private var currentFragment: String? = ""


    override fun getLayoutId(): Int {
        return R.layout.fragment_order_mainpage
    }

    override fun initView(mRootView: View?, mViewDataBinding: ViewDataBinding?) {
        fragmentOrderMainpageBinding = mViewDataBinding as FragmentOrderMainpageBinding
        createOrderViewModel = ViewModelProviders.of(this).get(CreateOrderViewModel::class.java)
        deliverymainViewModel = ViewModelProviders.of(activity!!).get(DeliveryMainViewModel::class.java)
        createOrderViewModel.navigator = this
        fragmentOrderMainpageBinding.createOrderViewModel = createOrderViewModel
        changeFragment(FragmentAddVechile.getFragmentAddVechile(this))
    }


    fun changeFragment(fragment: Fragment) {
        val ft = childFragmentManager.beginTransaction()
        val backStackName = fragment::class.java.simpleName
        addBackStack(true, ft, backStackName, fragment)
    }

    private fun addBackStack(
            isReplaceAll: Boolean,
            ft: FragmentTransaction,
            backStackName: String,
            callingFragment: Fragment
    ) {
        if (currentFragment.equals(backStackName)) return
        if (isReplaceAll) {
            if (replaceFragment(backStackName) == false) {
                ft.replace(R.id.createOrderContainer, callingFragment, backStackName)
                ft.addToBackStack(callingFragment::class.java.simpleName)
                ft.commit()
                return
            }
        } else {
            ft.replace(R.id.createOrderContainer, callingFragment, backStackName)
            ft.addToBackStack(callingFragment::class.java.simpleName)
            ft.commit()
        }
    }

    private fun replaceFragment(backStackName: String): Boolean {
        val manager: FragmentManager = childFragmentManager
        isFragmentInBackstack = false
        val count: Int = manager.getBackStackEntryCount()
        if (count > 0) {
            for (i in 0 until count) {
                if (manager.getBackStackEntryAt(i) != null &&
                        manager.getBackStackEntryAt(i).getName().isNullOrEmpty()
                        && !TextUtils.isEmpty(backStackName)
                ) {
                    if (manager.getBackStackEntryAt(i).getName().equals(backStackName)) {
                        manager.popBackStackImmediate(backStackName, 0)
                        isFragmentInBackstack = true
                        break
                    }
                }
            }
        }
        return isFragmentInBackstack
    }

    override fun changeOrderCrateFrag(tagName: String) {
        // createOrderViewModel.fragName.postValue(tagName)
        when (tagName) {

            DeliveryListFragment::class.java.simpleName -> {
                changeFragment(DeliveryListFragment.getDeliveryListFragment(this))
            }

            EstimatePackageFragment::class.java.simpleName -> {
                changeFragment(EstimatePackageFragment.getEstimagePageFragment(this))
            }
        }
    }

    override fun hideStepProgress(isNeedToHide: Boolean) {
        if (isNeedToHide)
            fragmentOrderMainpageBinding.stepProgress.visibility = View.VISIBLE
        else
            fragmentOrderMainpageBinding.stepProgress.visibility = View.GONE

    }

    override fun changeToolbarBackground(isTransparent: Boolean) {

    }

    override fun changeStepProgress(pageName: String) {
        when (pageName) {
            FragmentAddVechile::class.java.simpleName -> {
                ivVechile.setImageDrawable(ContextCompat.getDrawable(activity!!, R.drawable.ic_radio_btn))
                vLVechile.setBackgroundColor(ContextCompat.getColor(activity!!, R.color.grey))
                ivRoute.setImageDrawable(ContextCompat.getDrawable(activity!!, R.drawable.ic_inactive_radio_btn))
                vlRoute.setBackgroundColor(ContextCompat.getColor(activity!!, R.color.grey))
                ivPrice.setImageDrawable(ContextCompat.getDrawable(activity!!, R.drawable.ic_inactive_radio_btn))
            }

            DeliveryListFragment::class.java.simpleName -> {
                ivVechile.setImageDrawable(ContextCompat.getDrawable(activity!!, R.drawable.ic_radio_btn))
                vLVechile.setBackgroundColor(ContextCompat.getColor(activity!!, R.color.app_color))
                ivRoute.setImageDrawable(ContextCompat.getDrawable(activity!!, R.drawable.ic_radio_btn))
                vlRoute.setBackgroundColor(ContextCompat.getColor(activity!!, R.color.grey))
                ivPrice.setImageDrawable(ContextCompat.getDrawable(activity!!, R.drawable.ic_inactive_radio_btn))
            }

            EstimatePackageFragment::class.java.simpleName -> {
                ivVechile.setImageDrawable(ContextCompat.getDrawable(activity!!, R.drawable.ic_radio_btn))
                vLVechile.setBackgroundColor(ContextCompat.getColor(activity!!, R.color.app_color))
                ivRoute.setImageDrawable(ContextCompat.getDrawable(activity!!, R.drawable.ic_radio_btn))
                vlRoute.setBackgroundColor(ContextCompat.getColor(activity!!, R.color.app_color))
                ivPrice.setImageDrawable(ContextCompat.getDrawable(activity!!, R.drawable.ic_radio_btn))
            }
        }
    }

}