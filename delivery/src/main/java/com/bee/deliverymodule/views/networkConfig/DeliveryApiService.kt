package com.bee.deliverymodule.views.networkConfig

import com.bee.deliverymodule.views.datamodel.*
import io.reactivex.Observable
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.http.*
import java.util.HashMap

interface DeliveryApiService {

    @GET("user/delivery/check/request")
    fun callCheckRequst(@Header("Authorization") token: String): Observable<DeliveryCheckRequestModel>

    @GET("user/delivery/services")
    fun getVechileTpes(@Header("Authorization") token: String, @Query("type") type: String, @Query("latitude") latitude: String, @Query("longitude") longitude: String): Observable<VechileTypeModel>

    @GET("user/delivery/types/{menuType}")
    fun getBoxType(@Header("Authorization") token: String, @Path("menuType") menuType: String): Observable<BoxTypesDataModel>

    @FormUrlEncoded
    @POST("user/delivery/estimate")
    fun getEstimate(@Header("Authorization") token: String, @FieldMap param:HashMap<String,Any>,
                    @Field("d_latitude[]") dLatitudeList:ArrayList<Double>,
                    @Field("d_longitude[]") dLongitude:ArrayList<Double>,
                    @Field("receiver_name[]") reciverNameList:ArrayList<String>,
                    @Field("receiver_mobile[]") reciverMobileNubList:ArrayList<String>,
                    @Field("d_address[]" ) deliverAddresList:ArrayList<String>,
                    @Field("package_type_id[]") packageTypeIDList:ArrayList<Int>,
                    @Field("receiver_instruction[]") reciverInstruction:ArrayList<String>,
                    @Field("weight[]") weightList:ArrayList<String>,
                    @Field("length[]") lengthList:ArrayList<String>,
                    @Field("breadth[]") breadthList:ArrayList<String>,
                    @Field("height[]") heightList:ArrayList<String>,
                    @Field("is_fragile[]") isFragile:ArrayList<String>,
                    @Field("distance[]") distacneList:ArrayList<Int>): Observable<EstimatedFareDataModel>

//    @FormUrlEncoded
//    @POST("user/delivery/send/request")
//    fun senRequest(@Header("Authorization") token: String,
//                   @FieldMap params:HashMap<String,Any>,
//                   @Field("d_latitude[]") dLatitudeList:ArrayList<Double>,
//                   @Field("d_longitude[]") dLongitude:ArrayList<Double>,
//                   @Field("receiver_name[]") reciverNameList:ArrayList<String>,
//                   @Field("receiver_mobile[]") reciverMobileNubList:ArrayList<String>,
//                   @Field("d_address[]" ) deliverAddresList:ArrayList<String>,
//                   @Field("package_type_id[]") packageTypeIDList:ArrayList<Int>,
//                   @Field("receiver_instruction[]") reciverInstruction:ArrayList<String>,
//                   @Field("weight[]") weightList:ArrayList<String>,
//                   @Field("length[]") lengthList:ArrayList<String>,
//                   @Field("breadth[]") breadthList:ArrayList<String>,
//                   @Field("height[]") heightList:ArrayList<String>,
//                   @Field("is_fragile[]") isFragile:ArrayList<String>,
//                   @Field("distance[]") distacneList:ArrayList<Int>,
//                   @Part imageList: java.util.ArrayList<MultipartBody.Part>
//    ): Observable<RequestCreateModel>

    @Multipart
    @POST("user/delivery/send/request")
    fun senRequest(@Header("Authorization") token: String,
                   @QueryMap params:HashMap<String,Any>,
                   @Query("d_latitude[]") dLatitudeList:ArrayList<Double>,
                   @Query("d_longitude[]") dLongitude:ArrayList<Double>,
                   @Query("receiver_name[]") reciverNameList:ArrayList<String>,
                   @Query("receiver_mobile[]") reciverMobileNubList:ArrayList<String>,
                   @Query("d_address[]" ) deliverAddresList:ArrayList<String>,
                   @Query("package_type_id[]") packageTypeIDList:ArrayList<Int>,
                   @Query("receiver_instruction[]") reciverInstruction:ArrayList<String>,
                   @Query("weight[]") weightList:ArrayList<String>,
                   @Query("length[]") lengthList:ArrayList<String>,
                   @Query("breadth[]") breadthList:ArrayList<String>,
                   @Query("height[]") heightList:ArrayList<String>,
                   @Query("is_fragile[]") isFragile:ArrayList<String>,
                   @Query("distance[]") distacneList:ArrayList<Int>,
                   @Part imageList: ArrayList<MultipartBody.Part>
    ): Observable<RequestCreateModel>

    @FormUrlEncoded
    @POST("user/delivery/cancel/request")
    fun cancel(@Header("Authorization") token: String, @FieldMap param: HashMap<String, Any>): Observable<CancelRequest>

    @FormUrlEncoded
    @POST("user/delivery/payment")
    fun confirmPayment(@Header("Authorization") token: String, @FieldMap param: HashMap<String, Any>): Observable<PaymentConfirmDataModel>

    @FormUrlEncoded
    @POST("user/delivery/rate")
    fun setRating(@Header("Authorization") token: String,
                  @FieldMap params: HashMap <String, Any>): Observable<RatingSuccessModel>

    @FormUrlEncoded
    @POST("user/delivery/cancel/request")
    fun cancelRide(@Header("Authorization") token: String,
                   @FieldMap params: HashMap<String, Any>): Observable<CancelRequestModel>

    @GET("user/delivery/package/types")
    fun getPackageType(@Header("Authorization") token: String): Observable<PackageTypeModel>


}