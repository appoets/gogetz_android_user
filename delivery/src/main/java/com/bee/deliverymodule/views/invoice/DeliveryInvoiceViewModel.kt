package com.bee.deliverymodule.views.invoice

import androidx.lifecycle.MutableLiveData
import com.gox.basemodule.BaseApplication
import com.gox.basemodule.base.BaseViewModel
import com.gox.basemodule.data.PreferenceHelper
import com.gox.basemodule.data.PreferenceKey
import com.gox.basemodule.data.getValue
import com.gox.basemodule.repositary.ApiListener
import com.bee.deliverymodule.views.datamodel.DeliveryCheckRequestModel
import com.bee.deliverymodule.views.datamodel.PaymentConfirmDataModel
import com.bee.deliverymodule.views.networkConfig.DeliverRespository
import com.gox.basemodule.common.payment.model.ResCommonSuccessModel

class DeliveryInvoiceViewModel : BaseViewModel<DeliveryInvoiceNavigator>() {
    var deliveryCheckRequestModel = MutableLiveData<DeliveryCheckRequestModel>()
    val confirmPaymentResponse = MutableLiveData<PaymentConfirmDataModel>()
    var paymentResponse = MutableLiveData<ResCommonSuccessModel>()
    var errorResposne = MutableLiveData<String>()
    var loadingProgress = MutableLiveData<Boolean>()
    val preferenceHelper = PreferenceHelper(BaseApplication.baseApplication)
    val isPaid=MutableLiveData<Int>().apply { setValue(0) }
    val appRepository = DeliverRespository.instance()

    fun confirmPayment(){
        navigator.confirmPayment()
    }

    fun callCheckRequest() {
        getCompositeDisposable().addAll(appRepository.callCheckRequest(this, preferenceHelper.getValue(PreferenceKey.ACCESS_TOKEN, "").toString(), object : ApiListener {
            override fun onSuccess(successData: Any) {
                loadingProgress.postValue(false)
                deliveryCheckRequestModel.postValue(successData as DeliveryCheckRequestModel)
            }

            override fun onError(error: Throwable) {
                loadingProgress.postValue(false)
            }

        }))
    }

    fun callPayment(reqTips: ReqTips){
        val hashMap: HashMap<String, Any> = HashMap()
        hashMap["id"] = reqTips.requestId.toString()
        hashMap["card_id"] = reqTips.cardId.toString()
        hashMap["reference"] = reqTips.reference.toString()
        if(reqTips.tipsAmount!=null){
            val dou: Double = reqTips.tipsAmount!!.toDouble()
            val amount: Int =  Math.round(dou).toInt()
            hashMap["tips"] = amount
        }
        getCompositeDisposable().addAll(appRepository.confirmPayment(this, preferenceHelper.getValue(PreferenceKey.ACCESS_TOKEN, "").toString(), hashMap,object : ApiListener {
            override fun onSuccess(successData: Any) {
                loadingProgress.postValue(false)
                confirmPaymentResponse.postValue(successData as PaymentConfirmDataModel)
            }

            override fun onError(error: Throwable) {
                loadingProgress.postValue(false)
            }

        }))
    }
}