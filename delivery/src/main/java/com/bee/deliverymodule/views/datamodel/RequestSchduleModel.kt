package com.bee.deliverymodule.views.datamodel

import com.google.gson.annotations.SerializedName

class RequestSchduleModel {

    @SerializedName("ScheduleDate")
    var scheduleDate: String? = null
    @SerializedName("scheduleTime")
    var scheduleTime: String? = null
}