package com.bee.deliverymodule.views.invoice

interface DeliveryInvoiceNavigator {
    fun confirmPayment()
}