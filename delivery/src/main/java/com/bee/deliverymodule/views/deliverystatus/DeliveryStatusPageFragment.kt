package com.bee.deliverymodule.views.deliverystatus

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.res.Resources
import android.graphics.Bitmap
import android.graphics.Canvas
import android.location.Address
import android.location.Geocoder
import android.location.Location
import android.net.Uri
import android.os.Bundle
import android.util.DisplayMetrics
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.widget.AdapterView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.gox.basemodule.BaseApplication
import com.gox.basemodule.base.BaseFragment
import com.gox.basemodule.common.chatmessage.ChatActivity
import com.gox.basemodule.data.*
import com.gox.basemodule.socket.SocketListener
import com.gox.basemodule.socket.SocketManager
import com.gox.basemodule.utils.CarMarkerAnimUtil
import com.gox.basemodule.utils.LocationUtils
import com.gox.basemodule.utils.PolyUtil
import com.gox.basemodule.utils.polyline.DirectionUtils
import com.gox.basemodule.utils.polyline.PolyLineListener
import com.gox.basemodule.utils.polyline.PolylineUtil
import com.bee.delivermodule.R
import com.bee.delivermodule.databinding.FragmentOrderStatusBinding
import com.bee.deliverymodule.views.datamodel.DeliveryCheckRequestModel
import com.bee.deliverymodule.views.deliveryHome.DeliveryMainViewModel
import com.bee.deliverymodule.views.deliveryType.DeliveryTypeFragment
import com.bee.deliverymodule.views.invoice.DeliveryInvoiceActivity
import com.bee.deliverymodule.views.rating.DeliveryRatingDialog
import com.bumptech.glide.Glide
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.*
import com.google.android.gms.maps.model.*
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import io.socket.emitter.Emitter
import org.json.JSONObject
import java.io.IOException
import java.lang.Exception
import java.text.DecimalFormat

class DeliveryStatusPageFragment : BaseFragment<FragmentOrderStatusBinding>(),
        OnMapReadyCallback,
        GoogleMap.OnCameraIdleListener,
        GoogleMap.OnCameraMoveListener,
        AdapterView.OnItemClickListener,
        DeliveryStatusNavigator,
        PolyLineListener {
    private lateinit var fragmentOrderStatusBinding: FragmentOrderStatusBinding
    private lateinit var deliveryStatusViewModel: DeliveryStatusViewModel
    private lateinit var deliverymainViewmodel: DeliveryMainViewModel
    private lateinit var fragmentHomeTransActionMap: SupportMapFragment
    private var googleApiClient: GoogleApiClient? = null
    private lateinit var mGoogleMap: GoogleMap
    private lateinit var countryCode: String
    private var countryName: String? = ""
    private var currentStatus: String? = ""
    private var isSearch: Boolean? = false
    private val preference = PreferenceHelper(BaseApplication.baseApplication)
    private var mPolyline: Polyline? = null
    private lateinit var polyUtil: PolyUtil
    private var polyLine = ArrayList<LatLng>()
    private var canDrawPolyLine: Boolean = true
    private var polylineKey = ""
    private var startLatLng = LatLng(0.0, 0.0)
    private var endLatLng = LatLng(0.0, 0.0)
    private var srcMarker: Marker? = null
    private var mRoomConnected: Boolean? = false


    override fun getLayoutId(): Int {
        return R.layout.fragment_order_status
    }

    override fun initView(mRootView: View?, mViewDataBinding: ViewDataBinding?) {
        fragmentOrderStatusBinding = mViewDataBinding as FragmentOrderStatusBinding
        deliveryStatusViewModel = ViewModelProviders.of(this).get(DeliveryStatusViewModel::class.java)
        deliverymainViewmodel = ViewModelProviders.of(activity!!).get(DeliveryMainViewModel::class.java)
        deliveryStatusViewModel.navigator = this
        fragmentOrderStatusBinding.deliveryStatusModel = deliveryStatusViewModel
        fragmentOrderStatusBinding.deliverySatusFragmet = this
        getBundleValues()
        initialiseMap()
        var polyUtil = PolyUtil()

        //SetUpClient
        setUpClient()
        //getOBserVerValues

        SocketManager.onEvent(Constant.ROOM_NAME.STATUS, Emitter.Listener {
            if (it[0].toString().contains(Constant.ModuleTypes.DELIVERY)) {
                mRoomConnected = true
                Log.e("SOCKET", "SOCKET_SK transport request " + it[0])
            }
        })

        SocketManager.onEvent(Constant.ROOM_NAME.DELIVERY_REQ, Emitter.Listener {
            // checkRequestTimer!!.cancel()
            Log.e("SOCKET", "SOCKET_SK transport request " + it[0])
            deliverymainViewmodel.callCheckRequest()
        })


        SocketManager.onEvent(Constant.ROOM_NAME.UPDATELOCATION, Emitter.Listener {
            Log.e("SOCKET", "SOCKET_SK location received" + it[0])
            updateRouteViaSocket(it)
        })


        SocketManager.setOnSocketRefreshListener(object : SocketListener.connectionRefreshCallBack {
            override fun onRefresh() {

                if (!deliveryStatusViewModel.requestID.value.isNullOrEmpty())
                    SocketManager.emit(Constant.ROOM_NAME.DELIVERY_ROOM_NAME, Constant.ROOM_ID.getDeliveryRoom(deliverymainViewmodel.requestID.value!!))

                SocketManager.onEvent(Constant.ROOM_NAME.DELIVERY_REQ, Emitter.Listener {
                    // checkRequestTimer!!.cancel()
                    Log.e("SOCKET", "SOCKET_SK transport request " + it[0])
                    deliverymainViewmodel.callCheckRequest()
                })


                SocketManager.onEvent(Constant.ROOM_NAME.UPDATELOCATION, Emitter.Listener {
                    Log.e("SOCKET", "SOCKET_SK location received" + it[0])
                    updateRouteViaSocket(it)

                })
            }
        })


    }

    override fun onMapReady(map: GoogleMap) {
        try {
            map!!.setMapStyle(MapStyleOptions.loadRawResourceStyle(activity, R.raw.style_json))
        } catch (e: Resources.NotFoundException) {
            e.printStackTrace()
        }
        this.mGoogleMap = map
        this.mGoogleMap?.setOnCameraMoveListener(this)
        this.mGoogleMap?.setOnCameraIdleListener(this)
        getObservalues()
    }

    private fun getLatLng(countryName: String) {
        val geoCoder = Geocoder(activity!!)
        var addressList: List<Address>? = listOf()
        try {
            addressList = geoCoder.getFromLocationName(countryName, 1)
        } catch (e: IOException) {
            e.printStackTrace()
        }
        if (!addressList.isNullOrEmpty()) {
            val address = addressList!![0]
            val latLng = LatLng(address.latitude, address.longitude)
            updateMapLocation(LatLng(latLng.latitude, latLng.longitude), true)
            countryCode = LocationUtils.getCountryCode(activity!!, LatLng(latLng.latitude, latLng.longitude))
        }
    }

    override fun onCameraIdle() {

    }

    override fun onCameraMove() {

    }

    override fun onItemClick(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {

    }

    private fun initialiseMap() {
        MapsInitializer.initialize(activity)
        fragmentHomeTransActionMap = childFragmentManager.findFragmentById(R.id.deliveryStatusMap) as SupportMapFragment
        fragmentHomeTransActionMap.getMapAsync(this)
    }

    fun setUpClient() {
        googleApiClient = GoogleApiClient.Builder(activity!!)
                .addApi(LocationServices.API)
                .build()
        googleApiClient!!.connect()
    }

    fun updateMapLocation(location: LatLng, isAnimateMap: Boolean) {
        try {
            if (!isAnimateMap) {
                if (mGoogleMap != null) {
                    mGoogleMap?.moveCamera(CameraUpdateFactory
                            .newLatLngZoom(location, Constant.MapConfig.DEFAULT_ZOOM))
                }
            } else {
                if (mGoogleMap != null) {
                    mGoogleMap?.animateCamera(CameraUpdateFactory
                            .newLatLngZoom(location, Constant.MapConfig.DEFAULT_ZOOM))
                }
            }
        } catch (e: Exception) {

        }
    }

    fun getBundleValues() {
        countryName = if (arguments != null && arguments!!.containsKey("countryName")) arguments!!.getString("countryName") else ""
        isSearch = if (arguments != null && arguments!!.containsKey("isSearch")) arguments!!.getBoolean("isSearch", false) else false

    }

    fun showOngoingServiceStatus(isNeedtoShow: Boolean) {
        updateView()
    }

    fun updateView() {
        if (deliverymainViewmodel.deliveryCheckRequestModel.value!!.responseData != null) {
            if (!deliverymainViewmodel.deliveryCheckRequestModel.value!!.responseData!!.data.isNullOrEmpty()) {
                isSearch = false
                deliveryStatusViewModel.requestID.value = deliverymainViewmodel.deliveryCheckRequestModel.value!!.responseData!!.data!!.get(0)!!.id.toString()
                val deliveryRequet = deliverymainViewmodel.deliveryCheckRequestModel.value!!.responseData!!.data!!.get(0)
                deliverymainViewmodel.requestID.value = deliveryRequet!!.id
                fragmentOrderStatusBinding.tvStatus.setText(deliveryRequet.status.toString())

                if(!deliveryRequet.deliveries.isNullOrEmpty()){
                    var srcLatLng:LatLng = LatLng(0.0,0.0)
                    if(deliveryRequet.provider_id != null)
                     srcLatLng = LatLng(deliveryRequet.provider!!.latitude?:0.0, deliveryRequet.provider!!.longitude?:0.0)
                    var intval = 0
                for(delivery in deliveryRequet.deliveries){
                    intval = intval + 1;
                    if(!delivery.status.equals("COMPLETED",true)){
                        fragmentOrderStatusBinding.tvStatusDescription.setText("Recepient "+intval.toString()+"( "+delivery.status.toString()+" )"+":\n"+delivery!!.d_address.toString())
                        val destLatLng = LatLng(delivery!!.d_latitude!!.toDouble(), delivery!!.d_longitude!!.toDouble())
                        drawRoute(srcLatLng, destLatLng)
                        break
                    }
                }
                }
                if (!currentStatus.equals(deliverymainViewmodel.deliveryCheckRequestModel.value!!.responseData!!.data!!.get(0)!!.status!!.toUpperCase())) {

                    if (!mRoomConnected!!) {
                        PreferenceHelper(BaseApplication.baseApplication).setValue(PreferenceKey.DELIVERY_REQ_ID, deliverymainViewmodel.requestID.value!!)
                        if (deliverymainViewmodel!!.requestID.value != 0) {
                            SocketManager.emit(Constant.ROOM_NAME.DELIVERY_ROOM_NAME, Constant.ROOM_ID.getDeliveryRoom(deliverymainViewmodel.requestID.value!!))
                        }
                    }
                    if (!deliverymainViewmodel.deliveryCheckRequestModel.value!!.responseData!!.data!!.get(0)!!.status!!.equals("SEARCHING")) {
                        preference.setValue(Constant.Chat.USER_ID, deliveryRequet.user_id)
                        preference.setValue(Constant.Chat.REQUEST_ID, deliveryRequet.id)
                        preference.setValue(Constant.Chat.PROVIDER_ID, deliveryRequet.provider!!.id)
                        preference.setValue(Constant.Chat.USER_NAME, deliveryRequet.user!!.first_name)
                        preference.setValue(Constant.Chat.PROVIDER_NAME, deliveryRequet.provider!!.first_name)
                        preference.setValue(Constant.Chat.ADMIN_SERVICE, deliveryRequet.admin_service)
                    }


                    currentStatus = deliverymainViewmodel.deliveryCheckRequestModel.value!!.responseData!!.data!!.get(0)!!.status!!.toUpperCase()
                    val pickLocation = LatLng(deliverymainViewmodel.deliveryCheckRequestModel.value!!.responseData!!.data!!.get(0)!!.s_latitude!!, deliverymainViewmodel.deliveryCheckRequestModel.value!!.responseData!!.data!!.get(0)!!.s_longitude!!)
                    updateMapLocation(pickLocation, true)
                    when (deliverymainViewmodel.deliveryCheckRequestModel.value!!.responseData!!.data!!.get(0)!!.status!!.toUpperCase()) {

                        "SEARCHING" -> {
                            fragmentOrderStatusBinding.cvDeliveryStatus.visibility = View.GONE
                            fragmentOrderStatusBinding.llBottomCourierStatus.visibility = View.GONE
                            fragmentOrderStatusBinding.deliverySearchView.visibility = View.VISIBLE
                            fragmentOrderStatusBinding.deliveryCancelRequest.visibility = View.VISIBLE
                        }

                        "ACCEPTED" -> {
                            fragmentOrderStatusBinding.tvStatusDescription.setText(deliveryRequet!!.status.toString())
                        }

                        "STARTED" -> {
                            fragmentOrderStatusBinding.cvDeliveryStatus.visibility = View.VISIBLE
                            fragmentOrderStatusBinding.llBottomCourierStatus.visibility = View.VISIBLE
                            fragmentOrderStatusBinding.deliverySearchView.visibility = View.GONE
                            fragmentOrderStatusBinding.deliveryCancelRequest.visibility = View.GONE
                            updateStatusBottomView(deliveryRequet)
//                            val srcLatLng = LatLng(deliveryRequet.provider!!.latitude!!.toDouble(), deliveryRequet.provider!!.longitude!!.toDouble())
//                            for(delivery in deliveryRequet.delivery){
//                                if(!delivery.status.equals("COMPLETED")){
//                                    val destLatLng = LatLng(delivery!!.d_latitude!!.toDouble(), delivery!!.d_longitude!!.toDouble())
//                                    drawRoute(srcLatLng, destLatLng)
//                                    break
//                                }
//                            }
                        }

                        "ARRIVED" -> {
                            fragmentOrderStatusBinding.cvDeliveryStatus.visibility = View.VISIBLE
                            fragmentOrderStatusBinding.llBottomCourierStatus.visibility = View.VISIBLE
                            fragmentOrderStatusBinding.deliverySearchView.visibility = View.GONE
                            fragmentOrderStatusBinding.deliveryCancelRequest.visibility = View.GONE
                            updateStatusBottomView(deliveryRequet)
                            if (deliveryRequet.paid == 0 && deliveryRequet.payment_by!!.equals("SENDER",true)) {
                                val intent = Intent(activity!!, DeliveryInvoiceActivity::class.java)
                                intent.putExtra("Payment","0")
                                //intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
                                startActivity(intent)
//                                activity!!.finish()
                            }
                        }

                        "PICKEDUP" -> {
                            fragmentOrderStatusBinding.cvDeliveryStatus.visibility = View.VISIBLE
                            fragmentOrderStatusBinding.llBottomCourierStatus.visibility = View.VISIBLE
                            fragmentOrderStatusBinding.deliverySearchView.visibility = View.GONE
                            fragmentOrderStatusBinding.deliveryCancelRequest.visibility = View.GONE
//                            val destLatLng = LatLng(deliveryRequet.d_latitude!!.toDouble(), deliveryRequet.d_longitude!!.toDouble())
                            updateStatusBottomView(deliveryRequet)
                        }

                        "DROPPED" -> {
                            fragmentOrderStatusBinding.cvDeliveryStatus.visibility = View.VISIBLE
                            fragmentOrderStatusBinding.llBottomCourierStatus.visibility = View.VISIBLE
                            fragmentOrderStatusBinding.deliverySearchView.visibility = View.GONE
                            fragmentOrderStatusBinding.deliveryCancelRequest.visibility = View.GONE
                            updateStatusBottomView(deliveryRequet)
                        }

                        "COMPLETED" -> {
                            fragmentOrderStatusBinding.cvDeliveryStatus.visibility = View.VISIBLE
                            fragmentOrderStatusBinding.llBottomCourierStatus.visibility = View.VISIBLE
                            fragmentOrderStatusBinding.deliverySearchView.visibility = View.GONE
                            fragmentOrderStatusBinding.deliveryCancelRequest.visibility = View.GONE
                            if (deliveryRequet.paid == 0) {
                                val intent = Intent(activity!!, DeliveryInvoiceActivity::class.java)
                                intent.putExtra("Payment","1")
                                //intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
                                startActivity(intent)
//                                activity!!.finish()
                            } else {
                                try {
                                    val ratingDialog = DeliveryRatingDialog()
                                    val bundle = Bundle()
                                    bundle.putInt("requestID", deliverymainViewmodel.requestID.value!!)
                                    ratingDialog.arguments = bundle
                                    ratingDialog.isCancelable = false
                                    ratingDialog.show(activity!!.supportFragmentManager, "deliverry_rating")
                                } catch (ce: Exception){
                                    ce.printStackTrace()
                                }
                            }
                        }

                        else -> {
                            fragmentOrderStatusBinding.cvDeliveryStatus.visibility = View.VISIBLE
                            fragmentOrderStatusBinding.llBottomCourierStatus.visibility = View.VISIBLE
                            fragmentOrderStatusBinding.deliverySearchView.visibility = View.GONE
                        }

                    }
                }
            } else {
                if (isSearch!!) {
                    fragmentOrderStatusBinding.cvDeliveryStatus.visibility = View.GONE
                    fragmentOrderStatusBinding.llBottomCourierStatus.visibility = View.GONE
                    fragmentOrderStatusBinding.deliverySearchView.visibility = View.VISIBLE
                    fragmentOrderStatusBinding.deliveryCancelRequest.visibility = View.GONE

                }
                // deliverymainViewmodel.changeFragment(DeliveryTypeFragment())
            }
        }

    }

    fun updateStatusBottomView(request: DeliveryCheckRequestModel.ResponseData.Data) {
        if (!request.provider!!.picture.isNullOrEmpty())
            Glide.with(this).load(request.provider!!.picture).into(fragmentOrderStatusBinding.ivProviderImg)
        if (!request.provider!!.first_name.isNullOrEmpty())
            fragmentOrderStatusBinding.tvDeliveryBoyName.setText(request.provider!!.first_name)
        fragmentOrderStatusBinding.tvServiceID.setText(request.booking_id.toString())
    }

    fun getObservalues() {
        deliveryStatusViewModel.cancelRequestDataModel.observe(this, Observer {
            if (it.statusCode.equals("200")) {
                deliverymainViewmodel.deliveryDetailList.value!!.clear()
                deliverymainViewmodel.changeFragment(DeliveryTypeFragment())
            }
        })

        deliveryStatusViewModel.loadinProgress.observe(this, Observer {
            baseLiveDataLoading.value = it
        })

        deliverymainViewmodel.deliveryCheckRequestModel.observe(activity!!, Observer {
            if (it != null && it.statusCode!!.equals("200")) {
                if (it.responseData!!.data.isNullOrEmpty()) {
                    showOngoingServiceStatus(false)
                } else {
                    showOngoingServiceStatus(true)
                }
            }
        })
    }

    override fun makeCall() {
        try {
            val phoneNumber = deliverymainViewmodel.deliveryCheckRequestModel.value!!.responseData!!!!.data!!.get(0)!!.provider!!.mobile
            val intent = Intent(Intent.ACTION_DIAL)
            intent.data = Uri.parse("tel:$phoneNumber")
            startActivity(intent)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun message() {
        val intent = Intent(activity!!, ChatActivity::class.java)
        startActivity(intent)
    }

    override fun whenDone(output: PolylineOptions) {
        mGoogleMap!!.clear()

        mPolyline = mGoogleMap!!.addPolyline(output.width(5f)
                .color(ContextCompat.getColor(activity!!, R.color.colorTaxiBlack)))

        polyLine = output.points as ArrayList<LatLng>

        val builder = LatLngBounds.Builder()

        for (latLng in polyLine) builder.include(latLng)

        mGoogleMap!!.animateCamera(CameraUpdateFactory.newLatLngBounds(builder.build(), 100))

        try {
            val src = R.drawable.ic_marker_car
            val dest = R.drawable.ic_marker_dest

            activity?.run {
                srcMarker = mGoogleMap.addMarker(MarkerOptions().position(polyLine[0])
                        .icon(BitmapDescriptorFactory.fromBitmap(bitmapFromVector(this, src))))
            }



            if (deliveryStatusViewModel.currentStatus.value?.length?:0 > 2) {
                val size = polyLine.size - 1
                val results = FloatArray(1)
                var sum = 0f

                for (i in 0 until size) {
                    Location.distanceBetween(polyLine[i].latitude, polyLine[i].longitude,
                            polyLine[i + 1].latitude, polyLine[i + 1].longitude, results)
                    sum += results[0]
                }

                println("RRR mPolyline.size = " + polyLine.size)
                println("RRR mPolyline::driverSpeed(m/s) = " + deliveryStatusViewModel.driverSpeed.value)
                println("RRR mPolyline::totalDistance(M) = $sum")

                val seconds = sum / deliveryStatusViewModel.driverSpeed.value!!
                println("RRR mPolyline::totalTime(S) = $seconds")

                val hours = seconds.toInt() / 3600
                var remainder = seconds.toInt() - hours * 3600
                val mins = remainder / 60
                remainder -= mins * 60
                val secs = remainder


//            destMarker = mGoogleMap!!.addMarker(MarkerOptions().position(polyLine[polyLine.size - 1])
//                    .icon(BitmapDescriptorFactory.fromBitmap(etaMarker(etaText))))

                val etaMarker = (activity!!.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater)
                        .inflate(R.layout.marker_eta, null)
                val tvEta = etaMarker.findViewById(R.id.tvEta) as TextView
                tvEta.text = getETAFormat(hours, mins)

                mGoogleMap!!.addMarker(MarkerOptions().position(polyLine[polyLine.size - 1])
                        .icon(BitmapDescriptorFactory.fromBitmap(bitmapFromVector(activity!!, dest))))
            } else mGoogleMap!!.addMarker(MarkerOptions().position(polyLine[polyLine.size - 1])
                    .icon(BitmapDescriptorFactory.fromBitmap(bitmapFromVector(activity!!, dest))))

        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun getDistanceTime(meters: Double, seconds: Double) {
        activity?.runOnUiThread {
            deliveryStatusViewModel.driverSpeed.value = meters / seconds
            println("RRR :::: speed = ${deliveryStatusViewModel.driverSpeed.value}")
        }
    }

    override fun whenFail(statusCode: String) {
        if (statusCode == "OVER_DAILY_LIMIT" || statusCode.contains("You have exceeded your daily request quota for this API")) {
            polylineKey = BaseApplication.getCustomPreference!!.getString(PreferenceKey.GOOGLE_API_KEY, "") as String
            PolylineUtil(this).execute(DirectionUtils().getDirectionsUrl(deliveryStatusViewModel.tempSrc.value,
                    deliveryStatusViewModel.tempDest.value, polylineKey))
        }
        println("RRR whenFail = $statusCode")
        when (statusCode) {
            //   "NOT_FOUND" -> Toast.makeText(this, getString(R.string.NoRoadMapAvailable), Toast.LENGTH_SHORT).show()
            //  "ZERO_RESULTS" -> Toast.makeText(this, getString(R.string.NoRoadMapAvailable), Toast.LENGTH_SHORT).show()
            "MAX_WAYPOINTS_EXCEEDED" -> Log.d("", getString(R.string.WayPointLlimitExceeded))
            "MAX_ROUTE_LENGTH_EXCEEDED" -> Log.d("", getString(R.string.RoadMapLimitExceeded))
            "INVALID_REQUEST" -> Log.d("", getString(R.string.InvalidInputs))
            "OVER_DAILY_LIMIT" -> Log.d("", getString(R.string.MayBeInvalidAPIBillingPendingMethodDeprecated))
            "OVER_QUERY_LIMIT" -> Log.d("", getString(R.string.TooManyRequestlimitExceeded))
            "REQUEST_DENIED" -> Log.d("", getString(R.string.DirectionsServiceNotEnabled))
            "UNKNOWN_ERROR" -> Log.d("", getString(R.string.ServerError))
            else -> Log.d("", statusCode)
        }
    }

    private fun bitmapFromVector(context: Context, drawableId: Int): Bitmap {
        val drawable = ContextCompat.getDrawable(context, drawableId)
        val bitmap = Bitmap.createBitmap(drawable!!.intrinsicWidth,
                drawable.intrinsicHeight, Bitmap.Config.ARGB_8888)
        val canvas = Canvas(bitmap)
        drawable.setBounds(0, 0, canvas.width, canvas.height)
        drawable.draw(canvas)
        return bitmap
    }

    private fun fireBaseLocationListener(id: Int) {
        val myRef = FirebaseDatabase.getInstance().getReference("providerId$id")
        myRef.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                println("RRR :: dataSnapshot = $dataSnapshot")
                val value = dataSnapshot.getValue(LocationPointsEntity::class.java)
                if (value != null) {
                    /* deliverymainViewmodel.pickupLatLong.value.latitude= value.lat
                     mTaxiMainViewModel.longitude.value = value.lng*/

                    if (startLatLng.latitude != 0.0) endLatLng = startLatLng
                    startLatLng = LatLng(value.lat, value.lng)

                    if (endLatLng.latitude != 0.0 && polyLine.size > 0) {
                        try {
                            CarMarkerAnimUtil().carAnimWithBearing(srcMarker!!, endLatLng, startLatLng)
                            polyLineRerouting(endLatLng, polyLine)
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                    } else if (polyLine == null || polyLine.size < 1)
                        if (deliveryStatusViewModel.tempSrc.value != null)
                            drawRoute(deliveryStatusViewModel.tempSrc.value!!, deliveryStatusViewModel.tempDest.value!!)

                }
            }

            override fun onCancelled(error: DatabaseError) {
                println("RRR :: error = $error")
            }
        })
    }


    private fun updateRouteViaSocket(it: Array<Any>) {
        try {
        activity!!.runOnUiThread() {
            val data1 = it[0] as JSONObject
            deliveryStatusViewModel.latitude.value = data1.getString("lat").toDouble()
            deliveryStatusViewModel.longitude.value = data1.getString("lng").toDouble()
            if (startLatLng.latitude != 0.0) endLatLng = startLatLng
            startLatLng = LatLng(data1.getString("lat").toDouble(), data1.getString("lng").toDouble())
            if (endLatLng.latitude != 0.0 && polyLine.size > 0) {
                try {
                    CarMarkerAnimUtil().carAnimWithBearing(srcMarker!!, endLatLng, startLatLng)
                    polyLineRerouting(endLatLng, polyLine)
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            } else if (polyLine == null || polyLine.size < 1)
                if (deliveryStatusViewModel.tempSrc.value != null)
                    drawRoute(deliveryStatusViewModel.tempSrc.value!!, deliveryStatusViewModel.tempDest.value!!)

        }
        } catch (ce: Exception) {
            ce.printStackTrace()
        }
    }


    private fun polyLineRerouting(point: LatLng, polyLine: ArrayList<LatLng>) {
        println("----->     RRR TaxiDashBoardActivity.polyLineRerouting     <-----")
        println("RRR containsLocation = " + polyUtil.containsLocation(point, polyLine, true))
        println("RRR isLocationOnEdge = " + polyUtil.isLocationOnEdge(point, polyLine, true, 50.0))
        println("RRR locationIndexOnPath = " + polyUtil.locationIndexOnPath(point, polyLine, true, 50.0))
        println("RRR locationIndexOnEdgeOrPath = " + polyUtil.locationIndexOnEdgeOrPath(point, polyLine, false, true, 50.0))
        val index = polyUtil.locationIndexOnEdgeOrPath(point, polyLine, false, true, 50.0)
        if (index >= 0) {
            polyLine.subList(0, index + 1).clear()
            mPolyline!!.remove()
            val options = PolylineOptions()
            options.addAll(polyLine)
            mPolyline = mGoogleMap!!.addPolyline(options.width(5f).color
            (ContextCompat.getColor(activity!!, R.color.colorTaxiBlack)))
            println("RRR mPolyline = " + polyLine.size)
            val size = polyLine.size - 1
            val results = FloatArray(1)
            var sum = 0f
            for (i in 0 until size) {
                Location.distanceBetween(polyLine[i].latitude, polyLine[i].longitude,
                        polyLine[i + 1].latitude, polyLine[i + 1].longitude, results)
                sum += results[0]
            }
            println("RRR mPolyline.size = " + polyLine.size)
            println("RRR mPolyline::driverSpeed(m/s) = " + deliveryStatusViewModel.driverSpeed.value)
            println("RRR mPolyline::totalDistance(M) = $sum")
            val seconds = sum / deliveryStatusViewModel.driverSpeed.value!!
            println("RRR mPolyline::totalTime(S) = $seconds")

            val hours = seconds.toInt() / 3600
            var remainder = seconds.toInt() - hours * 3600
            val mins = remainder / 60
            remainder -= mins * 60
            val secs = remainder

            val etaMarker = (activity!!.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater)
                    .inflate(R.layout.marker_eta, null)
            val tvEta = etaMarker.findViewById(R.id.tvEta) as TextView
            tvEta.text = getETAFormat(hours, mins)


            mGoogleMap!!.addMarker(MarkerOptions().position(polyLine[polyLine.size - 1])
                    .icon(BitmapDescriptorFactory.fromBitmap(createDrawableFromView(activity!!, etaMarker))))

        } else {
            canDrawPolyLine = true
            drawRoute(LatLng(deliveryStatusViewModel.latitude.value!!, deliveryStatusViewModel.longitude.value!!),
                    deliveryStatusViewModel.polyLineDest.value!!)
        }
    }

    private fun drawRoute(src: LatLng, dest: LatLng) {
        try {
            deliveryStatusViewModel.tempSrc.value = src
            deliveryStatusViewModel.tempDest.value = dest
            if (canDrawPolyLine) {
                canDrawPolyLine = false
                android.os.Handler().postDelayed({ canDrawPolyLine = true }, 5000)
                PolylineUtil(this).execute(DirectionUtils().getDirectionsUrl(src, dest, getText(R.string.google_map_key).toString()))
            }
            deliveryStatusViewModel.polyLineSrc.value = src
            deliveryStatusViewModel.polyLineDest.value = dest
        } catch (e: Exception){
            e.printStackTrace()
        }
    }


    private fun createDrawableFromView(context: Context, view: View): Bitmap {
        val displayMetrics = DisplayMetrics()
        (context as Activity).windowManager.defaultDisplay.getMetrics(displayMetrics)
        view.layoutParams = RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,
                RelativeLayout.LayoutParams.WRAP_CONTENT)
        view.measure(displayMetrics.widthPixels, displayMetrics.heightPixels)
        view.layout(0, 0, displayMetrics.widthPixels, displayMetrics.heightPixels)
        view.buildDrawingCache()
        val bitmap = Bitmap.createBitmap(view.measuredWidth, view.measuredHeight, Bitmap.Config.ARGB_8888)
        val canvas = Canvas(bitmap)
        view.draw(canvas)
        return bitmap
    }

    private fun getETAFormat(hours: Int, mins: Int): String {
        var etaFormat = "ETA\n"
        if (hours > 0) {
            if (hours == 1)
                etaFormat = "${DecimalFormat("00").format(hours)} hr"
            else
                etaFormat = "${DecimalFormat("00").format(hours)} hrs"
        }

        if (mins >= 0) {
            if (mins <= 1)
                etaFormat = etaFormat + " $mins min"
            else
                etaFormat = etaFormat + " ${DecimalFormat("00").format(mins)} mins"
        }
        return etaFormat
    }


}