package com.bee.deliverymodule.views.deliveryList

interface DeliveryListNavigator {
    fun gotoEstimatepage()
    fun gotoItemDetailPage()
    fun reset()
    fun submit()
    fun validate():Boolean
    fun validateMultipleDelivery():Boolean
}