package com.bee.deliverymodule.views.estimatePackage

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.gox.basemodule.BaseApplication
import com.gox.basemodule.base.BaseViewModel
import com.gox.basemodule.data.Constant
import com.gox.basemodule.data.PreferenceHelper
import com.gox.basemodule.data.PreferenceKey
import com.gox.basemodule.data.getValue
import com.gox.basemodule.model.ReqPaymentUpdateModel
import com.gox.basemodule.repositary.ApiListener
import com.bee.deliverymodule.views.datamodel.EstimatedFareDataModel
import com.bee.deliverymodule.views.datamodel.ReqTips
import com.bee.deliverymodule.views.datamodel.RequestCreateModel
import com.bee.deliverymodule.views.networkConfig.DeliverRespository
import okhttp3.MultipartBody


class EstimagePackageViewModel : BaseViewModel<EstimatePackageNavigator>() {
    val mlEstimagedFarDataModel = MutableLiveData<EstimatedFareDataModel>()
    val mCreateDeliveryResponseModel=MutableLiveData<RequestCreateModel>()
    val deliveryLatitude = MutableLiveData<ArrayList<Double>>(arrayListOf())
    val deliveryLongitude = MutableLiveData<ArrayList<Double>>(arrayListOf())
    val deliveryAddress=MutableLiveData<ArrayList<String>>(arrayListOf())
    val reciverName = MutableLiveData<ArrayList<String>>(arrayListOf())
    val reciverPhone = MutableLiveData<ArrayList<String>>(arrayListOf())
    val reciverInstruction = MutableLiveData<ArrayList<String>>(arrayListOf())
    val packateTypesList=MutableLiveData<ArrayList<Int>>(arrayListOf())
    val weightList=MutableLiveData<ArrayList<String>>(arrayListOf())
    val LengthList=MutableLiveData<ArrayList<String>>(arrayListOf())
    val HeightList=MutableLiveData<ArrayList<String>>(arrayListOf())
    val IsFragileList=MutableLiveData<ArrayList<String>>(arrayListOf())
    val BreadthList=MutableLiveData<ArrayList<String>>(arrayListOf())
    val distanceList=MutableLiveData<ArrayList<Int>>(arrayListOf())
    var imageList=MutableLiveData<ArrayList<MultipartBody.Part>>(arrayListOf())
    var schduleTime=MutableLiveData<String>()
    var schduleDate=MutableLiveData<String>()
    val weight = MutableLiveData<String>()
    val distance = MutableLiveData<String>()
    val isFregile = MutableLiveData<MutableList<String>>(mutableListOf())
    val serviceType = MutableLiveData<String>()
    val deliveryType = MutableLiveData<String>()
    val paymentMode = MutableLiveData<String>().apply { setValue("CASH") }
    val packageTypeId =MutableLiveData<ArrayList<String>>(arrayListOf())
    val payBy = MutableLiveData<String>("SENDER")
    val appRepository = DeliverRespository.instance()
    val preferenceHelper = PreferenceHelper(BaseApplication.baseApplication)
    val loadingProgress = MutableLiveData<Boolean>()
    var errorResponse = MutableLiveData<String>()


    fun callEstimateApi(requestParam: HashMap<String,Any>) {
       // loadingProgress.value=true
        getCompositeDisposable().addAll(appRepository.getEstimate(this, requestParam, deliveryLatitude.value!!,
                deliveryLongitude.value!!,
                reciverName.value!!,
                reciverPhone.value!!,
                deliveryAddress.value!!,
                packateTypesList.value!!,
                reciverInstruction.value!!,
                weightList.value!!,
                LengthList.value!!,
                BreadthList.value!!,
                HeightList.value!!,
                IsFragileList.value!!,
                distanceList.value!!,
                preferenceHelper.getValue(PreferenceKey.ACCESS_TOKEN, "").toString(), object : ApiListener {
            override fun onSuccess(successData: Any) {
                loadingProgress.postValue(false)
                mlEstimagedFarDataModel.postValue(successData as EstimatedFareDataModel)
            }

            override fun onError(error: Throwable) {
                loadingProgress.postValue(false)
                errorResponse.value = getErrorMessage(error) ?: ""

            }

        }))
    }

    fun sendDeliveryRequest(sendRequestParam:HashMap<String,Any>){
        //loadingProgress.value=true
        getCompositeDisposable().addAll(appRepository.sendDelivery(this, sendRequestParam,
                deliveryLatitude.value!!,
                deliveryLongitude.value!!,
                reciverName.value!!,
                reciverPhone.value!!,
                deliveryAddress.value!!,
                packateTypesList.value!!,
                reciverInstruction.value!!,
                weightList.value!!,
                LengthList.value!!,
                BreadthList.value!!,
                HeightList.value!!,
                IsFragileList.value!!,
                distanceList.value!!,
                imageList.value!!,
                preferenceHelper.getValue(PreferenceKey.ACCESS_TOKEN, "").toString(), object : ApiListener {
            override fun onSuccess(successData: Any) {
                mCreateDeliveryResponseModel.postValue(successData as RequestCreateModel)
                loadingProgress.postValue(false)
            }

            override fun onError(error: Throwable) {
                System.out.println("error--> "+ error.toString())
                loadingProgress.postValue(false)
                errorResponse.value = getErrorMessage(error) ?: ""
            }

        }))
    }





    fun getErrorResponse(): LiveData<String> {
        return errorResponse
    }
}