package com.bee.deliverymodule.views.networkConfig

import androidx.lifecycle.ViewModel
import com.gox.basemodule.data.Constant
import com.gox.basemodule.repositary.ApiListener
import com.gox.basemodule.repositary.BaseRepository
import com.bee.deliverymodule.boxTypes.BoxTypeViewModel
import com.bee.deliverymodule.views.deliveryHome.DeliveryMainViewModel
import com.bee.deliverymodule.views.deliverystatus.DeliveryStatusViewModel
import com.bee.deliverymodule.views.estimatePackage.EstimagePackageViewModel
import com.bee.deliverymodule.views.invoice.DeliveryInvoiceViewModel
import com.bee.deliverymodule.views.rating.DeliveryRatingDialog
import com.bee.deliverymodule.views.rating.DeliveryRatingViewModel
import com.bee.deliverymodule.views.vechiletype.AddVechileViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.util.ArrayList

class DeliverRespository : BaseRepository() {
    companion object {
        private var mTransactionRepository: DeliverRespository? = null
        fun instance(): DeliverRespository {
            if (mTransactionRepository == null) {
                mTransactionRepository = DeliverRespository()
            }
            return mTransactionRepository!!
        }
    }

    fun callCheckRequest(viewModel: Any, token: String, apiListener: ApiListener): Disposable {
        return BaseRepository().createApiClient(Constant.BaseUrl.APP_BASE_URL, DeliveryApiService::class.java)
                .callCheckRequst(token)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe({
                    try {
                        apiListener.onSuccess(it)
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }, {
                    apiListener.onError(it)
                })
    }


    fun callVechileTypes(viewModel: AddVechileViewModel, token: String,type:String, apiListener: ApiListener): Disposable {
        return BaseRepository().createApiClient(Constant.BaseUrl.APP_BASE_URL, DeliveryApiService::class.java)
                .getVechileTpes(token, type, viewModel.mLatitude.value!!, viewModel.mLongitude.value!!)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe({
                    apiListener.onSuccess(it)
                }, {
                    apiListener.onError(it)
                })
    }

    fun getBoxType(viewModel: BoxTypeViewModel, token: String, apiListener: ApiListener): Disposable {
        return BaseRepository().createApiClient(Constant.BaseUrl.APP_BASE_URL, DeliveryApiService::class.java)
                .getBoxType(token, "1")
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe({
                    apiListener.onSuccess(it)
                }, {
                    apiListener.onError(it)
                })
    }

    fun getEstimate(viewModel: EstimagePackageViewModel, requestParam: HashMap<String, Any>,
                    deliverLatitudeList: ArrayList<Double>,
                    deliverLongitudeList: ArrayList<Double>,
                    reciverNameist: ArrayList<String>,
                    recivePhoneNumber: ArrayList<String>,
                    deliverAddressList: ArrayList<String>,
                    packageType: ArrayList<Int>,
                    reciverInstruction: ArrayList<String>,
                    weightList: ArrayList<String>,
                    lengthList: ArrayList<String>,
                    breadthList: ArrayList<String>,
                    heightList: ArrayList<String>,
                    isFragile: ArrayList<String>,
                    distanceList: ArrayList<Int>,
                    token: String, apiListener: ApiListener): Disposable {
        return BaseRepository().createApiClient(Constant.BaseUrl.APP_BASE_URL, DeliveryApiService::class.java)
                .getEstimate(token, requestParam,deliverLatitudeList, deliverLongitudeList, reciverNameist, recivePhoneNumber, deliverAddressList, packageType,
                        reciverInstruction, weightList,lengthList,breadthList,heightList,isFragile, distanceList)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe({
                    apiListener.onSuccess(it)
                }, {
                    apiListener.onError(it)
                })
    }

    fun sendDelivery(viewModel: EstimagePackageViewModel, requestParam: HashMap<String, Any>,
                     deliverLatitudeList: ArrayList<Double>,
                     deliverLongitudeList: ArrayList<Double>,
                     reciverNameist: ArrayList<String>,
                     recivePhoneNumber: ArrayList<String>,
                     deliverAddressList: ArrayList<String>,
                     packageType: ArrayList<Int>,
                     reciverInstruction: ArrayList<String>,
                     weightList: ArrayList<String>,
                     lengthList: ArrayList<String>,
                     breadthList: ArrayList<String>,
                     heightList: ArrayList<String>,
                     isFragile: ArrayList<String>,
                     distanceList: ArrayList<Int>,
                     imageList: ArrayList<MultipartBody.Part>,
                     token: String,
                     apiListener: ApiListener): Disposable {
        return BaseRepository().createApiClient(Constant.BaseUrl.APP_BASE_URL, DeliveryApiService::class.java)
                .senRequest(token, requestParam, deliverLatitudeList, deliverLongitudeList, reciverNameist, recivePhoneNumber, deliverAddressList, packageType,
                        reciverInstruction, weightList,lengthList,breadthList,heightList,isFragile, distanceList,imageList)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe({
                    apiListener.onSuccess(it)
                }, {
                    apiListener.onError(it)
                })
    }

    fun confirmPayment(viewModel: DeliveryInvoiceViewModel, token: String, params: HashMap<String, Any>, apiListener: ApiListener): Disposable {
        return BaseRepository().createApiClient(Constant.BaseUrl.APP_BASE_URL, DeliveryApiService::class.java)
                .confirmPayment(token, params)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe({
                    apiListener.onSuccess(it)
                }, {
                    apiListener.onError(it)
                })
    }

    fun setRating(viewModel: DeliveryRatingViewModel, token: String, params: HashMap<String, Any>, apiListener: ApiListener): Disposable {
        return BaseRepository().createApiClient(Constant.BaseUrl.APP_BASE_URL, DeliveryApiService::class.java)
                .setRating(token, params)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe({
                    apiListener.onSuccess(it)
                }, {
                    apiListener.onError(it)
                })
    }


    fun cancelRequest(viewModel: DeliveryStatusViewModel, token: String, params: HashMap<String, Any>, apiListener: ApiListener): Disposable {
        return BaseRepository().createApiClient(Constant.BaseUrl.APP_BASE_URL, DeliveryApiService::class.java)
                .cancelRide(token, params)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe({
                    apiListener.onSuccess(it)
                }, {
                    apiListener.onError(it)
                })
    }

    fun getPackageType(viewModel: ViewModel, token: String,apiListener: ApiListener): Disposable {
        return BaseRepository().createApiClient(Constant.BaseUrl.APP_BASE_URL, DeliveryApiService::class.java)
                .getPackageType(token)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe({
                    apiListener.onSuccess(it)
                }, {
                    apiListener.onError(it)
                })
    }



}