package com.bee.deliverymodule.views.deliverystatus

interface DeliveryStatusNavigator {
    fun makeCall()
    fun message()
}