package com.bee.deliverymodule.views.vechiletype

import android.Manifest
import android.annotation.SuppressLint
import android.content.res.Resources
import android.location.Location
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.AdapterView
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.gox.basemodule.adapter.PlaceListAdapter
import com.gox.basemodule.base.BaseFragment
import com.gox.basemodule.data.Constant
import com.gox.basemodule.data.PlacesModel
import com.gox.basemodule.utils.PlacesAutocompleteUtil
import com.gox.basemodule.utils.ViewUtils
import com.bee.delivermodule.R
import com.bee.delivermodule.databinding.FragmentAddVechileBinding
import com.bee.deliverymodule.adapter.VechileAdpater
import com.bee.deliverymodule.views.createOrder.CreateOrderNavigator
import com.bee.deliverymodule.views.deliveryHome.DeliveryMainViewModel
import com.bee.deliverymodule.views.deliveryList.DeliveryListFragment
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.*
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MapStyleOptions
import com.gox.basemodule.data.PreferenceKey
import com.gox.basemodule.data.getValue
import com.gox.basemodule.utils.LocationCallBack
import com.gox.basemodule.utils.LocationUtils
import permissions.dispatcher.NeedsPermission

class FragmentAddVechile : BaseFragment<FragmentAddVechileBinding>(),
        AddVechileInterface,
        OnMapReadyCallback,
        AdapterView.OnItemClickListener {
    private lateinit var fragmentAddVechileBinding: FragmentAddVechileBinding
    private lateinit var addVechileViewModel: AddVechileViewModel
    private lateinit var deliveryMainViewModel: DeliveryMainViewModel
    private lateinit var gMap: GoogleMap
    private lateinit var fragmentHomeTransActionMap: SupportMapFragment
    private var mGoogleMap: GoogleMap? = null
    private var googleApiClient: GoogleApiClient? = null
    private lateinit var linearLayoutManager: LinearLayoutManager
    private lateinit var vechileAdapter: VechileAdpater
    private lateinit var placesAutocompleteUtil: PlacesAutocompleteUtil
    private var prediction: ArrayList<PlacesModel> = ArrayList()
    private lateinit var countryCode: String
    private lateinit var textWatcher: PlaceTextLisnter
    private var mLastKnownLocation: Location? = null

    companion object {
        private lateinit var createOrderNavigator: CreateOrderNavigator
        fun getFragmentAddVechile(orderNavigator: CreateOrderNavigator): Fragment {
            createOrderNavigator = orderNavigator
            return FragmentAddVechile()
        }
    }

    override fun getLayoutId(): Int {
        return R.layout.fragment_add_vechile
    }

    override fun initView(mRootView: View?, mViewDataBinding: ViewDataBinding?) {
        fragmentAddVechileBinding = mViewDataBinding as FragmentAddVechileBinding
        addVechileViewModel = ViewModelProvider(this).get(AddVechileViewModel::class.java)

        activity?.run {
            deliveryMainViewModel = ViewModelProvider(this).get(DeliveryMainViewModel::class.java)
            MapsInitializer.initialize(this)
            placesAutocompleteUtil = PlacesAutocompleteUtil(this)

        }

        addVechileViewModel.navigator = this
        fragmentAddVechileBinding.viewModel = addVechileViewModel
        fragmentAddVechileBinding.addVechileFragment = this
        textWatcher = PlaceTextLisnter()
        fragmentAddVechileBinding.atvPlaces.setText(deliveryMainViewModel.strAddress.value)
        fragmentAddVechileBinding.atvPlaces.addTextChangedListener(textWatcher)
        createOrderNavigator.hideStepProgress(true)
        createOrderNavigator.changeStepProgress(FragmentAddVechile::class.java.simpleName)
        deliveryMainViewModel.selectedVechileID.value = ""
//        initalizeRecycler()
        //SetupMap
        initialiseMap()

        //SetUpClient
        setUpClient()

        //ObserverValues
        getObserVerValues()

        //getVechile
        getVechileTypes()

    }

    @SuppressLint("MissingPermission")
    @NeedsPermission(Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION)
    fun enableLocation(){
        LocationUtils.enableGpsAlert(requireContext())
    }

    fun initalizeRecycler() {
        linearLayoutManager = LinearLayoutManager(activity)
        fragmentAddVechileBinding.rvVechile.layoutManager = linearLayoutManager
    }

    fun getVechileTypes() {
        //addVechileViewModel.get
    }

    private fun initialiseMap() {
        fragmentHomeTransActionMap = childFragmentManager.findFragmentById(R.id.vechileMap) as SupportMapFragment
        fragmentHomeTransActionMap.getMapAsync(this)
    }

    fun setUpClient() {
        googleApiClient = GoogleApiClient.Builder(activity!!)
                .addApi(LocationServices.API)
                .build()
        googleApiClient!!.connect()
    }

    fun getObserVerValues() {
        addVechileViewModel.loadingProgress.observe(this, Observer {
            baseLiveDataLoading.postValue(it)
        })

        addVechileViewModel.vechileTypesModel.observe(this, Observer {
            if (it != null && it.statusCode.equals("200")) {
                if (!it.responseData!!.services!!.isNullOrEmpty()) {
                    vechileAdapter = VechileAdpater(addVechileViewModel, activity!!, it.responseData!!.services!!)
                    fragmentAddVechileBinding.rvVechile.adapter = vechileAdapter
                    deliveryMainViewModel.selectedVechileID.postValue("")
                }
            }
        })

        addVechileViewModel.selectedPosition.observe(this, Observer {
            if (!it.isNullOrEmpty()) {
                val selectedVechileID = addVechileViewModel.vechileTypesModel.value!!.responseData!!.services.get(it.toInt()).id
                deliveryMainViewModel.selectedVechileID.postValue(selectedVechileID.toString())
            }
        })
    }

    override fun goToItemDetailPage() {
        if (!addVechileViewModel.selectedPosition.value.isNullOrEmpty() && !deliveryMainViewModel.selectedVechileID.value!!.isNullOrEmpty())
            createOrderNavigator.changeOrderCrateFrag(DeliveryListFragment::class.java.simpleName)
        else
            ViewUtils.showToast(activity!!, "Please select any vechile", false)
    }

    override fun onMapReady(map: GoogleMap) {
        gMap = map
        try {
            gMap.setMapStyle(MapStyleOptions.loadRawResourceStyle(activity, R.raw.style_json))
        } catch (e: Resources.NotFoundException) {
            e.printStackTrace()
        }
        updateLocationUI()
//        if (deliveryMainViewModel.pickupLatLong.value != null) {


//        }
    }

    override fun onResume() {
        super.onResume()
    }

    @SuppressLint("MissingPermission")
    @NeedsPermission(Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION)
    fun updateLocationUI() {

        gMap.uiSettings.isMyLocationButtonEnabled = false
        gMap.uiSettings.isCompassEnabled = false

        LocationUtils.getLastKnownLocation(context!!, object : LocationCallBack.LastKnownLocation {
            override fun onSuccess(location: Location) {
                mLastKnownLocation = location
                updateMapLocation(LatLng(location.latitude, location.longitude),true)

                val address = LocationUtils.getCurrentAddress(baseActivity, LatLng(location.latitude, location.longitude))

                address.firstOrNull()?.let {
                    fragmentAddVechileBinding.atvPlaces.setText(it.getAddressLine(0))
                    deliveryMainViewModel.strAddress.value = it.getAddressLine(0)
                    deliveryMainViewModel.pickupLatLong.postValue(LatLng(location.latitude, location.longitude))
                    deliveryMainViewModel.strCountryCode.value = LocationUtils.getCountryCode(context!!, LatLng(it.latitude, it.longitude))
                }

                mLastKnownLocation?.let {
                    addVechileViewModel.mLatitude.value =it.latitude.toString()
                    addVechileViewModel.mLongitude.value = it.longitude.toString()
                }


                if (!addVechileViewModel.mLatitude.value.isNullOrEmpty() && !addVechileViewModel.mLongitude.value.isNullOrEmpty())
                    addVechileViewModel.getVechileType(deliveryMainViewModel.selectedBoxType.value!!)

            }

            override fun onFailure(messsage: String?) {
                deliveryMainViewModel.pickupLatLong.value?.let {
                    addVechileViewModel.mLatitude.value = it.latitude.toString()
                    addVechileViewModel.mLongitude.value = it.longitude.toString()
                }

                if (!addVechileViewModel.mLatitude.value.isNullOrEmpty() && !addVechileViewModel.mLongitude.value.isNullOrEmpty())
                    addVechileViewModel.getVechileType(deliveryMainViewModel.selectedBoxType.value!!)
                updateMapLocation(Constant.MapConfig.DEFAULT_LOCATION, true)
//                updateMapLocation(Constant.MapConfig.DEFAULT_LOCATION)
            }
        })
    }



    fun updateMapLocation(location: LatLng, isAnimateMap: Boolean) {
        if (!isAnimateMap) {
            gMap.moveCamera(CameraUpdateFactory.newLatLngZoom(location, Constant.MapConfig.DEFAULT_ZOOM))
        } else {
            gMap.animateCamera(CameraUpdateFactory.newLatLngZoom(location, Constant.MapConfig.DEFAULT_ZOOM))
        }
    }


    inner class PlaceTextLisnter : TextWatcher {
        override fun afterTextChanged(p0: Editable?) {
        }

        override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
        }

        override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
            placesAutocompleteUtil.findAutocompletePredictions(s.toString().trim(), deliveryMainViewModel.strCountryCode.value!!,
                    object : PlacesAutocompleteUtil.PlaceSuggestion {
                        override fun onPlaceReceived(mPlacesList: ArrayList<PlacesModel>?) {
                            try {
                                prediction = mPlacesList!!
                                val adapter = PlaceListAdapter(activity!!, prediction)
                                fragmentAddVechileBinding.atvPlaces.setAdapter(adapter)
                                adapter.notifyDataSetChanged()
                                fragmentAddVechileBinding.atvPlaces.setOnItemClickListener(this@FragmentAddVechile)
                            } catch (e: Exception) {
                                e.printStackTrace()
                            }
                        }
                    })

        }
    }

    fun clearAddress() {
        fragmentAddVechileBinding.atvPlaces.removeTextChangedListener(textWatcher)
        fragmentAddVechileBinding.atvPlaces.setText("")
        fragmentAddVechileBinding.atvPlaces.addTextChangedListener(textWatcher)
    }

    override fun onItemClick(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        if (prediction.size > 0) try {
            val mPlace = prediction[position]
            val mLatLng = placesAutocompleteUtil.getPlaceName(mPlace.mPlaceId, object : PlacesAutocompleteUtil.onLoccationListner {
                override fun onLatLngRecived(placeLatLng: LatLng) {
                    if (placeLatLng.latitude != 0.0 && placeLatLng.longitude != 0.0) {
                        addVechileViewModel.addressLiveData.value = prediction[position].mFullAddress
                        fragmentAddVechileBinding.atvPlaces.removeTextChangedListener(textWatcher)
                        fragmentAddVechileBinding.atvPlaces.setText(prediction[position].mFullAddress.toString())
                        deliveryMainViewModel.strAddress.value = (prediction[position].mFullAddress.toString())
                        deliveryMainViewModel.pickupLatLong.postValue(LatLng(placeLatLng.latitude, placeLatLng.longitude))
                        deliveryMainViewModel.strCountryCode.value = LocationUtils.getCountryCode(context!!, LatLng(placeLatLng.latitude, placeLatLng.longitude))
                        mGoogleMap?.animateCamera(CameraUpdateFactory.newLatLngZoom(placeLatLng, Constant.MapConfig.DEFAULT_ZOOM))
                        addVechileViewModel.mLatitude.value = placeLatLng.latitude.toString()
                        addVechileViewModel.mLongitude.value = placeLatLng.longitude.toString()
                        deliveryMainViewModel.pickupLatLong.value=placeLatLng
                        hideKeyboard()
                        addVechileViewModel.getVechileType(deliveryMainViewModel.selectedBoxType.value!!)
                    }
                }
            })
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }


}