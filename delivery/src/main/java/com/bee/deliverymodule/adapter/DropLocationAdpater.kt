package com.bee.deliverymodule.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.gox.basemodule.utils.ViewUtils
import com.bee.delivermodule.R
import com.bee.delivermodule.databinding.ListDropLocationBinding
import com.bee.deliverymodule.views.datamodel.DeliveryCheckRequestModel
import com.bee.deliverymodule.views.invoice.DeliveryInvoiceViewModel
import com.bumptech.glide.Glide


class DropLocationAdpater(val deliveryInvoiceViewModel: DeliveryInvoiceViewModel , val context: Context, val dropLocationList: List<DeliveryCheckRequestModel.ResponseData.Data.Delivery>) : RecyclerView.Adapter<DropLocationAdpater.DropLocationViewHolder>() {

    private var selectedPosition: Int? = -1

    inner class DropLocationViewHolder(listDropLocationBinding: ListDropLocationBinding) : RecyclerView.ViewHolder(listDropLocationBinding.root) {
        val dropLocationBinding = listDropLocationBinding
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DropLocationViewHolder {
        val viewHolderBinding = DataBindingUtil.inflate<ListDropLocationBinding>(LayoutInflater.from(parent.context), R.layout.list_drop_location, parent, false)
        return DropLocationViewHolder(viewHolderBinding)
    }

    override fun getItemCount(): Int {
        return dropLocationList.size
    }

    override fun onBindViewHolder(holder: DropLocationViewHolder, position: Int) {
            holder.dropLocationBinding.tvDropLocation.text = dropLocationList.get(position).d_address
    }

}