package com.bee.deliverymodule.adapter

import android.content.Context
import android.inputmethodservice.Keyboard
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.bee.delivermodule.R
import com.bee.delivermodule.databinding.RowAddressListBinding
import com.bee.deliverymodule.views.datamodel.ItemDetailModel

class MultiAddressAdapter(var context: Context, var addressList: ArrayList<ItemDetailModel>) : RecyclerView.Adapter<MultiAddressAdapter.MultiAddressViewHolder>() {

    private var mOnAdapterClickListener: ClickItemAdapterPostion? = null

    fun setOnClickListener(onClickListener: ClickItemAdapterPostion) {
        this.mOnAdapterClickListener = onClickListener
    }
    inner class MultiAddressViewHolder(addressBinding: RowAddressListBinding) : RecyclerView.ViewHolder(addressBinding.root) {
        var rowAddressListBinding = addressBinding
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MultiAddressViewHolder {
        val viewHolder = DataBindingUtil.inflate<RowAddressListBinding>(LayoutInflater.from(parent.context)
                , R.layout.row_address_list, parent, false)
        return MultiAddressViewHolder(viewHolder)
    }

    override fun getItemCount(): Int {
        return addressList.size
    }

    override fun onBindViewHolder(holder: MultiAddressViewHolder, position: Int) {
        if (!addressList.isNullOrEmpty()) {
            holder.rowAddressListBinding.tvSno.setText((position + 1).toString())
            holder.rowAddressListBinding.tvAddress.setText(addressList.get(position).deliveryAddress)
            holder.rowAddressListBinding.removeAddressBtn.setOnClickListener {
                mOnAdapterClickListener!!.clickPostion(position)
            }
        }
    }

}