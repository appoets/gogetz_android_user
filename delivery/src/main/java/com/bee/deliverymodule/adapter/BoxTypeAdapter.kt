package com.bee.deliverymodule.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.bee.delivermodule.R
import com.bee.delivermodule.databinding.RowBoxTypesBinding
import com.bee.deliverymodule.boxTypes.BoxTypeViewModel
import com.bee.deliverymodule.views.datamodel.BoxTypesDataModel

class BoxTypeAdapter(val viewModel: BoxTypeViewModel, val context: Context, val boxTypes: List<BoxTypesDataModel.ResponseData>) : RecyclerView.Adapter<BoxTypeAdapter.BoxTypeViewHolder>() {

    inner class BoxTypeViewHolder(rowBoxTypesBinding: RowBoxTypesBinding) : RecyclerView.ViewHolder(rowBoxTypesBinding.root) {
        val rowBoxTypeBinding = rowBoxTypesBinding
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BoxTypeViewHolder {
        val rowBoxTypeBinding = DataBindingUtil.inflate<RowBoxTypesBinding>(LayoutInflater.from(parent.context), R.layout.row_box_types, parent, false)
        return BoxTypeViewHolder(rowBoxTypeBinding)
    }

    override fun getItemCount(): Int {
        return boxTypes.size
    }

    override fun onBindViewHolder(holder: BoxTypeViewHolder, position: Int) {

        if (!boxTypes.get(position).delivery_name.isNullOrEmpty()) {
            holder.rowBoxTypeBinding.tvboxTypes.setText(boxTypes.get(position).delivery_name)
        }

        holder.rowBoxTypeBinding.clBoxTypes.setOnClickListener(View.OnClickListener {
            holder.rowBoxTypeBinding.clBoxTypes.setBackgroundColor(context.resources.getColor(R.color.app_color))
            holder.rowBoxTypeBinding.tvboxTypes.setTextColor(context.resources.getColor(R.color.black))
            viewModel.selectedBoxtype.postValue(boxTypes.get(position).id.toString())
        })
    }
}