package com.bee.deliverymodule.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.core.content.ContextCompat.*
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.gox.basemodule.utils.ViewUtils
import com.bee.delivermodule.R
import com.bee.delivermodule.databinding.RowAddVechileBinding
import com.bee.deliverymodule.views.datamodel.VechileTypeModel
import com.bee.deliverymodule.views.vechiletype.AddVechileViewModel
import com.bumptech.glide.Glide


class VechileAdpater(val vechileViewModel: AddVechileViewModel, val context: Context, val vechileList: List<VechileTypeModel.ResponseData.Service>) : RecyclerView.Adapter<VechileAdpater.VechileViewHolder>() {

    private var selectedPosition: Int? = -1

    inner class VechileViewHolder(rowAddVechileBinding: RowAddVechileBinding) : RecyclerView.ViewHolder(rowAddVechileBinding.root) {
        val vechileBinding = rowAddVechileBinding
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VechileViewHolder {
        val viewHolderBinding = DataBindingUtil.inflate<RowAddVechileBinding>(LayoutInflater.from(parent.context), R.layout.row_add_vechile, parent, false)
        return VechileViewHolder(viewHolderBinding)
    }

    override fun getItemCount(): Int {
        return vechileList.size
    }

    override fun onBindViewHolder(holder: VechileViewHolder, position: Int) {
        if (!vechileList.get(0).vehicle_image.isNullOrEmpty()) {
            Glide.with(context)
                    .load(vechileList.get(position).vehicle_image.toString())
                    .circleCrop()
                    .placeholder(R.color.greay)
                    .into(holder.vechileBinding.ivVechile)
        }

        holder.vechileBinding.clVechile.setOnClickListener(View.OnClickListener {
            selectedPosition = position
            notifyDataSetChanged()
            vechileViewModel.selectedPosition.value = position.toString()
        })


        if (selectedPosition == position) {
            holder.vechileBinding.tvVechileName.setText(vechileList.get(position).vehicle_name)
            holder.vechileBinding.tvVechileName.background = getDrawable(context, R.drawable.bg_appcolor_rounded_corner)
        } else {
            holder.vechileBinding.tvVechileName.setText(vechileList.get(position).vehicle_name)
            holder.vechileBinding.tvVechileName.background = getDrawable(context, R.drawable.bg_white)
        }

    }
}